package com.msouza.coffeebreak;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.ItemPedidoDecorator;
import com.msouza.coffeebreak.entidades.Categoria;
import com.msouza.coffeebreak.entidades.model.ItemPedido;
import com.msouza.coffeebreak.entidades.model.Notificacao;
import com.msouza.coffeebreak.entidades.model.Produto;

public class DBHelper extends SQLiteOpenHelper{
	
	String DB_PATH;
    private final static String DB_NAME = "db_order.db";
	public final static int DB_VERSION = 1;
    public static SQLiteDatabase db; 
 
    private final Context context;
    
	private final String TABLE_NAME = "tbl_order";
	private final String ID = "id";
	private final String MENU_NAME = "Menu_name";
	private final String QUANTITY = "Quantity";
	private final String TOTAL_PRICE = "Total_price";
	
	
    public DBHelper(Context context) {
 
    	super(context, DB_NAME, null, DB_VERSION);
        this.context = context;
        
        DB_PATH = Constant.DBPath;
    }	
 
    public void createDataBase() throws IOException{
 
    	boolean dbExist = checkDataBase();
    	SQLiteDatabase db_Read = null;

 
    	if(dbExist){
    		// banco ja existe
    		
    	}else{
    		db_Read = this.getReadableDatabase();
    		db_Read.close();
 
        	try {
    			copyDataBase();
    		} catch (IOException e) {
        		throw new Error("Error copying database");
        	}
    	}
 
    }
 
   
   
    private boolean checkDataBase(){
 
    	File dbFile = new File(DB_PATH + DB_NAME);

    	return dbFile.exists();
    	
    }
 
    
    private void copyDataBase() throws IOException{
    	
    	InputStream myInput = context.getAssets().open(DB_NAME);
 
    	String outFileName = DB_PATH + DB_NAME;
 
    	OutputStream myOutput = new FileOutputStream(outFileName);
    	
    	byte[] buffer = new byte[1024];
    	int length;
    	while ((length = myInput.read(buffer))>0){
    		myOutput.write(buffer, 0, length);
    	}
 
    	myOutput.flush();
    	myOutput.close();
    	myInput.close();
 
    }
 
    public void openDataBase() throws SQLException{
    	String myPath = DB_PATH + DB_NAME;
    	db = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
    }
 
    @Override
	public void close() {
    	db.close();
	}
 
	@Override
	public void onCreate(SQLiteDatabase db) {
 
	}
 
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
 
	}
	
	/** busca tudo que tem no banco */
 	public ArrayList<ArrayList<Object>> getAllData(){
		ArrayList<ArrayList<Object>> dataArrays = new ArrayList<ArrayList<Object>>();
 
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						TABLE_NAME,
						new String[]{ID, MENU_NAME, QUANTITY, TOTAL_PRICE},
						null,null, null, null, null);
				cursor.moveToFirst();
	
				if (!cursor.isAfterLast()){
					do{
						ArrayList<Object> dataList = new ArrayList<Object>();
	 
						dataList.add(cursor.getLong(0));
						dataList.add(cursor.getString(1));
						dataList.add(cursor.getString(2));
						dataList.add(cursor.getString(3));
	 
						dataArrays.add(dataList);
					}
				
					while (cursor.moveToNext());
				}
				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return dataArrays;
	}
 	
 	/** verifica se o registro ja existe */
 	public boolean isDataExist(long id){
 		boolean exist = false;
		
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						TABLE_NAME,
						new String[]{ID},
						ID +"="+id,
						null, null, null, null);
				if(cursor.getCount() > 0){
					exist = true;
				}

				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return exist;
	}
 	
 	public boolean isPreviousDataExist(){
 		boolean exist = false;
		
		Cursor cursor = null;
 
			try{
				cursor = db.query(
						"itenspedido",
						new String[]{ID},
						null,null, null, null, null);
				if(cursor.getCount() > 0){
					exist = true;
				}

				cursor.close();
			}catch (SQLException e){
				Log.e("DB Error", e.toString());
				e.printStackTrace();
			}
		
		return exist;
	}

 	public void addData(long id, String menu_name, int quantity, double total_price){
		ContentValues values = new ContentValues();
		values.put(ID, id);
		values.put(MENU_NAME, menu_name);
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
 
		try{db.insert(TABLE_NAME, null, values);}
		catch(Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
 	
 	public void deleteData(long id){
		try {db.delete(TABLE_NAME, ID + "=" + id, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
 	
 	public void deleteAllData(){
		try {db.delete(TABLE_NAME, null, null);}
		catch (Exception e)
		{
			Log.e("DB ERROR", e.toString());
			e.printStackTrace();
		}
	}
 	
 	public void updateData(long id, int quantity, double total_price){
		ContentValues values = new ContentValues();
		values.put(QUANTITY, quantity);
		values.put(TOTAL_PRICE, total_price);
 
		try {db.update(TABLE_NAME, values, ID + "=" + id, null);}
		catch (Exception e)
		{
			Log.e("DB Error", e.toString());
			e.printStackTrace();
		}
	}


	public void limpaPedido(){
		try {db.delete("itenspedido", null, null);}
		catch (Exception e)
		{
			Log.e("ERRO LIMPAR PEDIDO", e.toString());
			e.printStackTrace();
		}
	}

	public void removeItemPedido(Long id){
		try {db.delete("itenspedido", "id = " + id, null);}
		catch (Exception e)
		{
			Log.e("ERRO REMOVER ITEM", e.toString());
			e.printStackTrace();
		}
	}



	//Verifica se o item do pedido já tem no banco, se sim, só incrementa a quantidade
	public boolean itemJaExiste(ItemPedido item){
		int numItens = 0;

		Cursor cursor = null;
		try{
			cursor = db.rawQuery("select count(id) from itenspedido where produtoid = " + item.getIdProduto(), null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					numItens = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO CONTAR CAT", e.toString());
			e.printStackTrace();
		}

		System.out.println("QUANTIDADE ITENS: " + numItens);

		return numItens > 0;
	}

	public void atualizaQuantidadeItem(ItemPedido item){
		try{
			db.execSQL("update itenspedido set quantidade = quantidade + ? where produtoid = ?", new String[]{item.getQuantidade()+"", item.getIdProduto()+""});
			System.out.println("EXECUTOU UPDATE");
		}catch (SQLException e){
			Log.e("ERRO AO INCREMENTAR", e.toString());
			e.printStackTrace();
		}
		System.out.println("ATUALIZOU");
	}


	public void adicionaItemPedido(ItemPedido item){
		if(this.itemJaExiste(item)){
			this.atualizaQuantidadeItem(item);
		}else {
			ContentValues campos = new ContentValues();
			campos.put("produtoid", item.getIdProduto());
			campos.put("quantidade", item.getQuantidade());

			try {
				db.insert("itenspedido", null, campos);
			} catch (Exception e) {
				Log.e("ERRO AO INSERIR ITEM", e.toString());
				e.printStackTrace();
			}
		}
	}

	public List<ItemPedido> buscaItensPedido(){
		List<ItemPedido> itens = new ArrayList<>();

		Cursor cursor = null;

		try{
			cursor = db.query("itenspedido",	new String[]{"id", "produtoid", "quantidade"}, null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					itens.add(new ItemPedido(cursor.getLong(0), cursor.getLong(1), cursor.getInt(2)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR ITENS", e.toString());
			e.printStackTrace();
		}

		return itens;
	}

	public enum TipoBuscaProduto{
        ID, CATEGORIA;
    }


	// PRODUTOS
	public void deletaProdutoExistente(Produto produto){
		try{
			db.execSQL("delete from produtos where id = ?", new String[]{produto.getId()+""});
		}catch (SQLException e){
			Log.e("ERRO AO DELETAR PROD", e.toString());
			e.printStackTrace();
		}
	}


	public void adicionaProduto(Produto produto){
		//deletaProdutoExistente(produto);

		ContentValues campos = new ContentValues();
		campos.put("id", produto.getId());
		campos.put("categoriaid", produto.getCategoriaid());
		campos.put("nome", produto.getNome());
		campos.put("descricao", produto.getDescricao());
		campos.put("foto", produto.getFoto());
		campos.put("tempopreparo", produto.getTempoPreparo());
		campos.put("ativo", produto.getAtivo());
		campos.put("valorunitario", produto.getValorUnitario());
		campos.put("valorantigo", produto.getValorAntigo());
		campos.put("promocao", produto.getValorAntigo());

		try{
			db.insert("produtos", null, campos);
		} catch(Exception e){
			Log.e("ERRO AO INSERIR PRODUTO", e.toString());
			e.printStackTrace();
		}
	}


	public List<Produto> buscaProdutos(Long valor, TipoBuscaProduto tipo){
		List<Produto> produtos = new ArrayList<>();

        String campo = tipo.equals(TipoBuscaProduto.CATEGORIA) ? "categoriaid" : "id";

		Cursor cursor = null;

		try{
			cursor = db.query("produtos", new String[]{"id", "categoriaid", "nome", "descricao", "foto", "tempopreparo", "ativo", "valorunitario", "valorantigo", "promocao"},
					campo + "=?",new String[]{valor.toString()}, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					produtos.add(new Produto(cursor.getLong(0), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getLong(1), cursor.getInt(5),
							cursor.getInt(6), cursor.getDouble(7), cursor.getDouble(8), cursor.getInt(9)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR PRODUTOS", e.toString());
			e.printStackTrace();
		}

		return produtos;
	}


	public void salvaPromocoesDB(List<Produto> produtos){
		ContentValues campos;
		for(Produto produto : produtos){
			campos = new ContentValues();
			campos.put("id", produto.getId());
			campos.put("categoriaid", produto.getCategoriaid());
			campos.put("nome", produto.getNome());
			campos.put("descricao", produto.getDescricao());
			campos.put("foto", produto.getFoto());
			campos.put("tempopreparo", produto.getTempoPreparo());
			campos.put("ativo", produto.getAtivo());
			campos.put("valorunitario", produto.getValorUnitario());
			campos.put("valorantigo", produto.getValorAntigo());
			campos.put("promocao", produto.getValorAntigo());

			try{
				db.insert("promocoes", null, campos);
			} catch(Exception e){
				Log.e("ERRO AO INSERIR PRODUTO", e.toString());
				e.printStackTrace();
			}
		}

	}

	public void limpaPromocoes(){
		try {db.delete("promocoes", null, null);}
		catch (Exception e)	{
			Log.e("ERRO AO DELETAR PROD", e.toString());
			e.printStackTrace();
		}
	}

	public List<Produto> buscaProdutosEmPromocao(){
		List<Produto> produtos = new ArrayList<>();

		Cursor cursor = null;

		try{
			cursor = db.query("promocoes", new String[]{"id", "categoriaid", "nome", "descricao", "foto", "tempopreparo", "ativo", "valorunitario", "valorantigo", "promocao"},
					null,null, null, null, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					produtos.add(new Produto(cursor.getLong(0), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getLong(1), cursor.getInt(5),
							cursor.getInt(6), cursor.getDouble(7), cursor.getDouble(8), cursor.getInt(9)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR PROMOS", e.toString());
			e.printStackTrace();
		}

		return produtos;
	}

	public void deletaTodosProtudos(){
		try {db.delete("produtos", null, null);}
		catch (Exception e)	{
			Log.e("ERRO AO DELETAR PROD", e.toString());
			e.printStackTrace();
		}
	}


	// CATEGORIAS

    public void adicionaCategoria(Categoria categoria){
        ContentValues campos = new ContentValues();
        campos.put("id", categoria.getId());
        campos.put("nome", categoria.getNome());
        campos.put("foto", categoria.getFoto());

        try{
            db.insert("categorias", null, campos);
        } catch(Exception e){
            Log.e("ERRO AO INSERIR CAT", e.toString());
            e.printStackTrace();
        }
    }


    public List<Categoria> buscaCategorias(){
        List<Categoria> categorias = new ArrayList<>();
        Cursor cursor = null;

        try{
            cursor = db.query("categorias", new String[]{"id", "nome", "foto"}, null,null, null, null, null);
            cursor.moveToFirst();

            if (!cursor.isAfterLast()){
                do{
                    categorias.add(new Categoria(cursor.getLong(0), cursor.getString(1), cursor.getString(2)));
                } while (cursor.moveToNext());
            }
            cursor.close();
        }catch (SQLException e){
            Log.e("ERRO AO BUSCAR CAT", e.toString());
            e.printStackTrace();
        }

        return categorias;
    }

    public void deletaTodasCategorias(){
        try {db.delete("categorias", null, null);}
        catch (Exception e)	{
            Log.e("ERRO AO DELETAR CAT", e.toString());
            e.printStackTrace();
        }
    }

    public boolean temCategoriasNoBanco(){
		int numCategorias = 0;

		Cursor cursor = null;
		try{
			cursor = db.rawQuery("select count(id) from categorias", null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					numCategorias = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO CONTAR CAT", e.toString());
			e.printStackTrace();
		}

		return numCategorias > 0;
	}

    /*
    * select p.id, p.nome, p.valorunitario, ip.quantidade from produtos p inner join itenspedido ip on ip.produtoid = p.id
    * */

	public List<ItemPedidoDecorator> buscaItensDecorator(){
		List<ItemPedidoDecorator> itens = new ArrayList<>();
		Cursor cursor = null;

		try{
			cursor = db.rawQuery("select p.id, p.nome, p.valorunitario, ip.quantidade, ip.id as iditem, p.tempopreparo from produtos p inner join itenspedido ip on ip.produtoid = p.id", null);

			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					itens.add(new ItemPedidoDecorator(cursor.getLong(4), cursor.getLong(0), cursor.getString(1), cursor.getDouble(2), cursor.getInt(3), cursor.getInt(5)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR ITENS", e.toString());
			e.printStackTrace();
		}

		return itens;
	}


	// Salva notificações no banco
	public void adicionaNotificacao(Notificacao notificacao){
		ContentValues campos = new ContentValues();
		campos.put("pedidoid", notificacao.getIdPedido());
		campos.put("status", notificacao.getStatus());
		campos.put("hora", new SimpleDateFormat("HH:mm").format(new Date()));
		try{
			campos.put("mensagem", URLDecoder.decode(notificacao.getMensagem(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		try{
			db.insert("notificacoes", null, campos);
		} catch(Exception e){
			Log.e("ERRO AO INSERIR PRODUTO", e.toString());
			e.printStackTrace();
		}
	}


	public List<Notificacao> listaNotificacoes(){
		List<Notificacao> itens = new ArrayList<>();
		Cursor cursor = null;

		try{
			cursor = db.rawQuery("select n.id, n.pedidoid, n.hora, n.mensagem, n.status from notificacoes n order by n.id ASC", null);

			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					itens.add(new Notificacao(cursor.getInt(0), cursor.getLong(1), cursor.getString(2), cursor.getString(3), cursor.getInt(4)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR ITENS", e.toString());
			e.printStackTrace();
		}

		return itens;
	}

	public void limpaNotificacoes(){
		try {db.delete("notificacoes", null, null);}
		catch (Exception e)	{
			Log.e("ERRO AO DELETAR CAT", e.toString());
			e.printStackTrace();
		}
	}

	public boolean verificaProdutoFavoritado(Long produto, Long usuario){
		int numFav = 0;

		Cursor cursor = null;
		try{

			cursor = db.rawQuery("select count(id) from favoritos where produtoid = " + produto + " and usuarioid = " + usuario, null);
			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					numFav = cursor.getInt(0);
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO CONTAR FAV", e.toString());
			e.printStackTrace();
		}

		return numFav > 0;
	}

	public void removeFavorito(Long produtoid, Long usuarioid){
		try {
			db.delete("favoritos","produtoid = ? and usuarioid = ?",new String[]{produtoid.toString(), usuarioid.toString()});
		}
		catch (Exception e)	{
			Log.e("ERRO AO DELETAR FAV", e.toString());
			e.printStackTrace();
		}
	}

	public void adicionaFavoritos(Long produtoid, Long usuarioid){
		ContentValues campos = new ContentValues();
		campos.put("usuarioid", usuarioid);
		campos.put("produtoid", produtoid);

		try{
			db.insert("favoritos", null, campos);
		} catch(Exception e){
			Log.e("ERRO AO INSERIR FAV", e.toString());
			e.printStackTrace();
		}
	}


	public List<Favorito> listaFavoritos(Long id){
		List<Favorito> favoritos = new ArrayList<>();
		Cursor cursor = null;

		try{
			cursor = db.rawQuery("select f.id, f.produtoid, p.nome, p.valorunitario from favoritos f inner join produtos p on p.id = f.produtoid where f.usuarioid = " + id + " group by 2 order by p.nome ASC", null);

			cursor.moveToFirst();

			if (!cursor.isAfterLast()){
				do{
					favoritos.add(new Favorito(cursor.getLong(0), cursor.getLong(1), cursor.getDouble(3), cursor.getString(2)));
				} while (cursor.moveToNext());
			}
			cursor.close();
		}catch (SQLException e){
			Log.e("ERRO AO BUSCAR FAV", e.toString());
			e.printStackTrace();
		}

		return favoritos;
	}

	public void limpaFavoritos(){
		try {db.delete("favoritos", null, null);}
		catch (Exception e)	{
			Log.e("ERRO AO LIMPAR FAV", e.toString());
			e.printStackTrace();
		}
	}

}