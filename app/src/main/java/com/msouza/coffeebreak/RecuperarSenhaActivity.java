package com.msouza.coffeebreak;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.Usuario;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class RecuperarSenhaActivity extends Activity implements View.OnClickListener {

    private EditText txtEmail;
    private Button btnRecuperarSenha, btnVoltar;
    private ProgressDialog carregando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperar_senha);

        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnRecuperarSenha = (Button) findViewById(R.id.btnRecuperarSenha);
        btnVoltar = (Button) findViewById(R.id.btnVoltar);

        btnRecuperarSenha.setOnClickListener(this);
        btnVoltar.setOnClickListener(this);

        carregando = new ProgressDialog(this);
        carregando.setMessage("Processando..");
        carregando.setCanceledOnTouchOutside(false);
        carregando.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private boolean validarCampos(){
        if(Utilitarios.isNull(txtEmail)){
            Toast.makeText(RecuperarSenhaActivity.this, "Preencha o email.", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(txtEmail.getText().toString()).matches()){
            Toast.makeText(RecuperarSenhaActivity.this, "Informe um endereço de email válido.", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnVoltar:
                finish();
                break;

            case R.id.btnRecuperarSenha:
                if(validarCampos()){
                    recuperarSenhaWS(new RequestParams("email", txtEmail.getText().toString().trim()));
                }
                break;
        }
    }

    private void recuperarSenhaWS(RequestParams params){
        carregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.RECUPERAR_SENHA, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                carregando.dismiss();

                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);

                Toast.makeText(RecuperarSenhaActivity.this, retorno.getMensagem(), Toast.LENGTH_LONG).show();
            }

        });
    }

}
