package com.msouza.coffeebreak;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.Usuario;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Matheus on 20/08/2017.
 */

public class TabMinhaConta extends Fragment {

    public enum TipoImagem{
        LOCAL, FACEBOOK;
    }

    private EditText nome, email;
   // private EditText senha1, senha2, senhaAtual;
    private Button btnSalvar;
    private CircleImageView imagemPerfil;
    private boolean alteraSenha = false;
    Long codUsuario = 0l;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab_minha_conta, container, false);


        // txt = (TextView) rootView.findViewById(R.id.txtCredito);
        nome = (EditText) rootView.findViewById( R.id.txtNome);
        email = (EditText) rootView.findViewById( R.id.txtEmail);
      //  senha1 = (EditText) rootView.findViewById( R.id.txtSenha);
     //   senha2 = (EditText) rootView.findViewById( R.id.txtSenha2);
      //  senhaAtual = (EditText) rootView.findViewById(R.id.txtSenhaAtual);
        btnSalvar = (Button) rootView.findViewById(R.id.btnSalvar);
        imagemPerfil = (CircleImageView) rootView.findViewById(R.id.imagemPerfil);


        Sessao sessao = new Sessao(getActivity().getApplicationContext());
        HashMap<String, String> user = sessao.getDetalhesUsuario();
        nome.setText(user.get(Sessao.KEY_NOME));
        email.setText(user.get(Sessao.KEY_EMAIL));
        codUsuario = Long.valueOf(user.get(Sessao.KEY_IDUSUARIO));

        // Carrega Imagem com o Picasso e salva em cache
        Picasso.with(getActivity().getApplicationContext()).load(Utilitarios.getURLFoto(user.get(Sessao.KEY_FOTO)))
                .placeholder(R.drawable.com_facebook_profile_picture_blank_square)
                .error(R.drawable.com_facebook_profile_picture_blank_square)
                .into(imagemPerfil);

        //Glide.with(getActivity().getApplicationContext()).load(Utilitarios.getURLFoto(args.getString("foto"))).error(android.R.drawable.stat_notify_error).into(foto);

//        Glide.with(TabMinhaConta.this)
//                .load(user.get(Sessao.KEY_FOTO))
//                .error(android.R.drawable.stat_notify_error)
//                .into(imagemPerfil);

//
//
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.get(Constant.PERFIL_USUARIO_BY_ID + idUsuario, new JsonHttpResponseHandler() {
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {
//
//                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
//                Usuario usuario = gson.fromJson(resposta.toString(), Usuario.class);
//
//
//            }
//        });


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos()){

                    RequestParams parametros = new RequestParams();
                    parametros.put("id", codUsuario);
                    parametros.put("nome", nome.getText().toString());
                   /* if(alteraSenha) {
                        parametros.put("senha", senha1.getText().toString().trim());
                        parametros.put("senhaatual", senhaAtual.getText().toString().trim());
                    }*/
                    invokeWS(parametros);
                }
            }
        });

        imagemPerfil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                exibeAlerta("Clicou na foto (AÇÃO ALTERAR)");
            }
        });

        return rootView;
    }


    private boolean validarCampos(){

        if(isNull(nome)){
            exibeAlerta("Informe o nome");
            return false;
        }

       /* if(!isNull(senhaAtual) && !isNull(senha1) && !isNull(senha2)){
            if(!senha1.getText().toString().trim().equals(senha2.getText().toString().trim())){
                exibeAlerta("As senhas não conferem");
                return false;
            }
            alteraSenha = true;
        }*/

        return true;
    }

    private void exibeAlerta(String mensagem){
        Toast.makeText(getActivity().getApplicationContext(),mensagem,Toast.LENGTH_LONG).show();
    }

    private boolean isNull(EditText edit){
        return edit.getText().toString().trim() == null || edit.getText().toString().trim().equals("");
    }



    private void invokeWS(RequestParams params){
//        AsyncHttpClient client = new AsyncHttpClient();
//        client.post(Constant.SALVA_ALTERACOES_PERFIL, params, new JsonHttpResponseHandler(){
//            @Override
//            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
//                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
//                Usuario usuario = gson.fromJson(respostaJson.toString(), Usuario.class);
//
//                Sessao sessao = new Sessao(getActivity().getApplicationContext());
//                sessao.limparCampos();
//                sessao.iniciarSessao(usuario.getNome(), usuario.getEmail(), usuario.getFoto(), usuario.getId());
//
//                exibeAlerta("As alterações foram salvas!");
//            }
//
//        });
    }


}
