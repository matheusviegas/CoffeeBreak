package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.AdapterDataSemHora;
import com.msouza.coffeebreak.entidades.Credito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoAtualizaFoto;
import com.msouza.coffeebreak.entidades.StatusUsuario;
import com.msouza.coffeebreak.entidades.Usuario;
import com.msouza.coffeebreak.entidades.ViaCEP;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.entities.Profile;
import com.sromku.simple.fb.listeners.OnLoginListener;
import com.sromku.simple.fb.listeners.OnProfileListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import cz.msebera.android.httpclient.Header;

public class ActivityLogin extends Activity {

    GridView listCategory;
    ProgressBar prgLoading;
    TextView txtAlert;



    Button login, botaoLogar;
    EditText txtEmail,txtSenha;
    SimpleFacebook fb;
    AlertDialog alt;
    TextView linkCadastro, esqueciMinhaSenha;

    private ProgressDialog progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getActionBar().hide();


        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);
        bar.setTitle("Login");

        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtAlert = (TextView) findViewById(R.id.txtAlert);


        // LOGIN
        login = (Button)findViewById(R.id.login);
        botaoLogar = (Button) findViewById(R.id.botaoLogar);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        linkCadastro = (TextView) findViewById(R.id.linkCadastro);
        esqueciMinhaSenha = (TextView) findViewById(R.id.esqueciMinhaSenha);

        progressBar = new ProgressDialog(login.getContext());
        progressBar.setCancelable(false);
        progressBar.setCanceledOnTouchOutside(false);
        progressBar.setMessage("Autenticando...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        botaoLogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarLogin()){


                    RequestParams parametros = new RequestParams();
                    parametros.put("email", txtEmail.getText().toString());
                    parametros.put("senha", txtSenha.getText().toString());
                    invokeWS(parametros);



/*

                    Intent painel = new Intent(MainActivity.this, Painel.class);

                    painel.putExtra("nome", "Matheus");
                    painel.putExtra("email", txtEmail.getText());
                    painel.putExtra("foto", "http://askspace.pl/images/10012014222324-f514286a390d6f08e70c076dd69cab5f6237.jpg");

                    startActivity(painel);*/
                }
            }
        });


        // ANTIGA PROGRESS BAR
        /*progressBar = new ProgressDialog(login.getContext());
        progressBar.setCancelable(false);
        progressBar.setMessage("Autenticando...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);*/


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


        // Clique no link de cadastro
        linkCadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





               /* AsyncHttpClient client = new AsyncHttpClient();
                client.get("https://viacep.com.br/ws/96010160/json/", new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {

                        Gson gson = new GsonBuilder().create();
                        ViaCEP consultaCEP = gson.fromJson(resposta.toString(), ViaCEP.class);

                        System.out.println(consultaCEP);
                    }
                });*/



                // COMENTEI PRA TESTAR REQUEST
                startActivity(new Intent(getApplicationContext(), ActivityCadastro.class));
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });

        esqueciMinhaSenha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityLogin.this, RecuperarSenhaActivity.class));
                overridePendingTransition(R.anim.open_next, R.anim.close_next);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       return true;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }


    @Override
    protected void onResume() {
        super.onResume();

        fb= SimpleFacebook.getInstance(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        fb.onActivityResult(requestCode,resultCode,data);
    }

    private boolean validarLogin(){
        if(txtEmail.getText().toString().trim() == null || txtEmail.getText().toString().trim().equals("")){
            exibeAlerta("Preencha o email");
            return false;
        }

        if(!this.validaEmail(txtEmail.getText().toString())){
            exibeAlerta("Email inválido");
            return false;
        }

        if(txtSenha.getText().toString().trim() == null || txtSenha.getText().toString().trim().equals("")){
            exibeAlerta("Preencha a senha");
            return false;
        }

        return true;
    }

    private boolean validaEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void exibeAlerta(String mensagem){
        Toast.makeText(getApplicationContext(),mensagem,Toast.LENGTH_LONG).show();
    }


    private void login(){
        OnLoginListener loginListener = new OnLoginListener() {
            @Override
            public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {



                OnProfileListener onProfileListener =new OnProfileListener() {
                    @Override
                    public void onComplete(Profile response) {
                        super.onComplete(response);

                        verificaContaFB(response);

                    }

                    @Override
                    public void onException(Throwable throwable) {
                        super.onException(throwable);
                    }

                    @Override
                    public void onFail(String reason) {
                        super.onFail(reason);
                    }

                };
                Profile.Properties properties = new Profile.Properties.Builder()
                        .add(Profile.Properties.EMAIL)
                        .add(Profile.Properties.PICTURE)
                        .add(Profile.Properties.NAME)
                        .build();
                fb.getProfile(properties,onProfileListener);

            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onException(Throwable throwable) {

            }

            @Override
            public void onFail(String reason) {

            }
        };
        fb.login(loginListener);

    }


    private void verificaContaFB(final Profile perfil){

        progressBar.show();


        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.LOGIN_API, new RequestParams("idcontasocial", Long.parseLong(perfil.getId())), new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                progressBar.hide();

                System.out.println("JSONTOSTRING" + respostaJson.toString());


                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new AdapterDataSemHora()).create();
                Usuario usuario = gson.fromJson(respostaJson.toString(), Usuario.class);
                System.out.println("TOSTRINGUSUARIO: ");
                System.out.println(usuario.toString());


                // SESSAO


                if(usuario.usuarioOK()){
                    StatusUsuario statusUsuario = StatusUsuario.values()[usuario.getStatus()];
                    if(statusUsuario.equals(StatusUsuario.ATIVO)) {
                        Sessao sessao = new Sessao(getApplicationContext());
                        sessao.iniciarSessao(usuario.getNome(), usuario.getEmail(), usuario.getFoto(), usuario.getId(), Long.parseLong(perfil.getId()), usuario.getCpf(), usuario.getTelefone());

                        Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        startActivity(intent);
                    } else if(statusUsuario.equals(StatusUsuario.CONTA_DESATIVADA)) {
                        Calendar desativacao = Calendar.getInstance();
                        desativacao.setTime(usuario.getDatadesativacao());
                        desativacao.add(Calendar.DAY_OF_MONTH, 30);
                        if(new Date().after(desativacao.getTime())){
                            cancelarContaDefinitivoWS(usuario.getId());
                            Toast.makeText(getApplicationContext(), "O prazo para reativar sua conta expirou.", Toast.LENGTH_LONG).show();
                        }else{
                            Intent reativar = new Intent(ActivityLogin.this, ReativarContaActivity.class);
                            reativar.putExtra("id", usuario.getId());
                            reativar.putExtra("nome", usuario.getNome());
                            reativar.putExtra("datalimite", new SimpleDateFormat("dd/MM/yyyy").format(desativacao.getTime()));
                            startActivity(reativar);
                            overridePendingTransition(R.anim.open_next, R.anim.close_next);
                        }

                    }else {
                        Toast.makeText(getApplicationContext(), statusUsuario.getMensagem(), Toast.LENGTH_LONG).show();
                    }

                }else{
                    System.out.println("Usuario nao cadastrado ainda");

                    Intent cadastro = new Intent(ActivityLogin.this, ActivityCadastro.class);
                    cadastro.putExtra("idcontasocial", Long.parseLong(perfil.getId()));
                    cadastro.putExtra("nomecompleto", perfil.getName());
                    cadastro.putExtra("email", perfil.getEmail());
                    cadastro.putExtra("foto", perfil.getPicture().toString());
                    cadastro.putExtra("cadastrofb", true);
                    startActivity(cadastro);
                }
                overridePendingTransition(R.anim.open_next, R.anim.close_next);

            }

        });




    }




    /**
     * Método que faz o login pelo webservice
     *
     * @param params
     */
    private void invokeWS(RequestParams params){
        progressBar.show();

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.LOGIN_API,params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressBar.hide();
               try {
                   String resposta = new String(responseBody);
                   resposta = resposta.replaceAll("\\[", "");
                   resposta = resposta.replaceAll("\\]", "");

                    JSONObject obj = new JSONObject(resposta);

                   Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new AdapterDataSemHora()).create();
                   Usuario usuario = gson.fromJson(obj.toString(), Usuario.class);

                   StatusUsuario statusUsuario = StatusUsuario.values()[obj.getInt("status")];
                   if(statusUsuario.equals(StatusUsuario.ATIVO)) {
                       // SESSAO
                       Sessao sessao = new Sessao(getApplicationContext());
                       sessao.iniciarSessao(obj.getString("nome"), obj.getString("email"), obj.getString("foto"), obj.getLong("id"), 1l, obj.getString("cpf"), obj.getString("telefone"));

                       System.out.println("CPF: " + obj.getString("cpf"));
                       System.out.println("TELEFONE: " + obj.getString("telefone"));

                       Intent intent = new Intent(ActivityLogin.this, MainActivity.class);
                       intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                       intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                       startActivity(intent);
                   }else if(statusUsuario.equals(StatusUsuario.CONTA_DESATIVADA)) {
                       Calendar desativacao = Calendar.getInstance();
                       desativacao.setTime(usuario.getDatadesativacao());
                       desativacao.add(Calendar.DAY_OF_MONTH, 30);
                       if(new Date().after(desativacao.getTime())){
                           cancelarContaDefinitivoWS(usuario.getId());
                           Toast.makeText(getApplicationContext(), "O prazo para reativar sua conta expirou.", Toast.LENGTH_LONG).show();
                       }else{
                           Intent reativar = new Intent(ActivityLogin.this, ReativarContaActivity.class);
                           reativar.putExtra("id", usuario.getId());
                           reativar.putExtra("nome", usuario.getNome());
                           reativar.putExtra("datalimite", new SimpleDateFormat("dd/MM/yyyy").format(desativacao.getTime()));
                           startActivity(reativar);
                           overridePendingTransition(R.anim.open_next, R.anim.close_next);
                       }
                   }else{
                       Toast.makeText(getApplicationContext(), statusUsuario.getMensagem(), Toast.LENGTH_LONG).show();
                   }

                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Email ou senha incorretos!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }catch (NumberFormatException nf){
                   nf.printStackTrace();
                   Toast.makeText(getApplicationContext(), "Email ou senha incorretos!", Toast.LENGTH_LONG).show();
               }catch (JsonSyntaxException js){
                   Toast.makeText(getApplicationContext(), "Email ou senha incorretos!", Toast.LENGTH_LONG).show();
               }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                progressBar.hide();
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Recurso não encontrado", Toast.LENGTH_LONG).show();
                }
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Servidor fora do ar", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Erro inesperado! [Causa Comum: O dispositivo não está conectado na internet ou o servidor está fora do ar.", Toast.LENGTH_LONG).show();
                }
            }


        });
    }



    private void cancelarContaDefinitivoWS(Long id){
        RequestParams params = new RequestParams();
        params.put("usuarioid", id);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.CANCELAR_CONTA_DEFINITIVO, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {

            }

        });

    }

}
