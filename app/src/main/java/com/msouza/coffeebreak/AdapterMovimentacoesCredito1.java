package com.msouza.coffeebreak;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.Usuario;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class AdapterMovimentacoesCredito1 extends ArrayAdapter<MovimentacaoCredito> {

    private List<MovimentacaoCredito> dataSet;
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm", new Locale("pt", "BR"));
    DBHelper dbHelper;
    Sessao sessao;

    private static class ViewHolder {
        TextView valor, data;
    }

    public AdapterMovimentacoesCredito1(List<MovimentacaoCredito> data, Context context) {
        super(context, R.layout.movimentacao_credito_item, data);
        this.dataSet = data;
        this.mContext=context;
        this.dbHelper = new DBHelper(context);
        this.sessao = new Sessao(context);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MovimentacaoCredito movimentacaoCredito = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.movimentacao_credito_item, parent, false);
            viewHolder.valor = (TextView) convertView.findViewById(R.id.valor);
            viewHolder.data = (TextView) convertView.findViewById(R.id.data);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.valor.setText(Utilitarios.formatarValor(movimentacaoCredito.getValormovimentado(), true));
        viewHolder.valor.setTextColor(Color.parseColor(getCor(movimentacaoCredito.getTipomovimentacao())));

        viewHolder.data.setText(movimentacaoCredito.getDatamovimentacao() != null ? sdf.format(movimentacaoCredito.getDatamovimentacao()).toUpperCase() : "-");

        return convertView;
    }


    private String getCor(int tipo){
        switch(tipo){
            case 1:
                return "#dd4b39";
            case 2:
                return "#00a65a";
            default:
                return "#424242";
        }
    }
}