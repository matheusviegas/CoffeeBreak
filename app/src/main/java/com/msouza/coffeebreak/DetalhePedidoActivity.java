package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.ItemPedidoWS;
import com.msouza.coffeebreak.entidades.PedidoWS;
import com.msouza.coffeebreak.entidades.model.ItemPedido;
import org.json.JSONArray;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class DetalhePedidoActivity extends Activity {

    List<ItemPedidoWS> itensPedidoWS;
    ProgressDialog janelaCarregando;

    TextView total, desconto, formaPagamento, totalPagar, statusPagamento, statusPedido, observacoes, dataPedido, retirada, motivo;

    LinearLayout mLinearListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_pedido);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        janelaCarregando = new ProgressDialog(this);
        janelaCarregando.setMessage("Abrindo pedido..");

        mLinearListView =(LinearLayout) findViewById(R.id.listaItens);

        total = (TextView) findViewById(R.id.total);
        desconto = (TextView) findViewById(R.id.desconto);
        formaPagamento = (TextView) findViewById(R.id.formaPagamento);
        totalPagar = (TextView) findViewById(R.id.totalPagar);
        statusPagamento = (TextView) findViewById(R.id.statusPagamento);
        statusPedido = (TextView) findViewById(R.id.statusPedido);
        observacoes = (TextView) findViewById(R.id.observacoes);
        dataPedido = (TextView) findViewById(R.id.dataPedido);
        retirada = (TextView) findViewById(R.id.retirada);
        motivo = (TextView) findViewById(R.id.motivo);

        itensPedidoWS = new ArrayList<>();


        // Pega dados da Intent
        Intent params = getIntent();
        Double valorTotal = params.getDoubleExtra("valortotal", 0d);
        int pago = params.getIntExtra("pago", 0);
        int status = params.getIntExtra("status", 0);
        int porcentagemDesconto = params.getIntExtra("desconto", 0);
        int tipopagamento = params.getIntExtra("tipopagamento", 1);
        Long idPedido = params.getLongExtra("id", 1l);
        String obs = params.getStringExtra("obs");
        String dataPed = params.getStringExtra("data");
        String ret = params.getStringExtra("retirada");
        String mot = params.getStringExtra("motivo");

        bar.setTitle("Detalhe do Pedido #" + idPedido);

        total.setText(Html.fromHtml("<b>Valor:</b> " + Utilitarios.formatarValor(valorTotal, true)));
        totalPagar.setText(Html.fromHtml("<b>Total a Pagar:</b> " + Utilitarios.formatarValor(valorTotal - Utilitarios.calculaValorDescontado(new PedidoWS(valorTotal, porcentagemDesconto)), true)));
        formaPagamento.setText(Html.fromHtml("<b>Forma Pagamento:</b> " + (tipopagamento == 1 ? "Crédito" : "Dinheiro")));
        statusPagamento.setText(Html.fromHtml("<b>Status Pagamento:</b> " + (pago == 1 ? "Pago" : "Não Pago")));
        statusPedido.setText(Html.fromHtml("<b>Status Pedido:</b> " + Utilitarios.getStatusTexto(status)));
        desconto.setText(Html.fromHtml("<b>Desconto:</b> " + Utilitarios.formatarValor(Utilitarios.calculaValorDescontado(new PedidoWS(valorTotal, porcentagemDesconto)), true) + " (" + porcentagemDesconto + "%)"));
        observacoes.setText(Html.fromHtml("<b>Observações:</b> " + (obs.equals("") ? "Nenhuma observação." : obs)));
        dataPedido.setText(Html.fromHtml("<b>Pedido feito em:</b> " + dataPed));
        retirada.setText(Html.fromHtml("<b>Retirado em:</b> " + ret));

        if(status == 4 && !mot.equals("")){
            motivo.setVisibility(View.VISIBLE);
            motivo.setText(Html.fromHtml("<b>Motivo do Cancelamento:</b> " + mot));
        }else{
            motivo.setVisibility(View.GONE);
        }


        buscaItensPedido(idPedido);
    }


    private void buscaItensPedido(Long idPedido){
        janelaCarregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.LISTA_ITENS_PEDIDO_FEITO + "/" + idPedido, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                Type listType = new TypeToken<ArrayList<ItemPedidoWS>>(){}.getType();
                List<ItemPedidoWS> pedidosWS = gson.fromJson(resposta.toString(), listType);
                itensPedidoWS.addAll(pedidosWS);

                for (ItemPedidoWS item : pedidosWS) {

                    LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                    View mLinearView = inflater.inflate(R.layout.detalhe_pedido_item_lista, null);

                    TextView nomeProduto = (TextView) mLinearView.findViewById(R.id.nomeProduto);
                    TextView quantidade = (TextView) mLinearView.findViewById(R.id.quantidade);
                    TextView valor = (TextView) mLinearView.findViewById(R.id.valor);

                    nomeProduto.setText(item.getNome());
                    quantidade.setText(item.getQuantidade() + "x");
                    valor.setText(Utilitarios.formatarValor(item.getValor(), true));

                    mLinearListView.addView(mLinearView);
                }

                janelaCarregando.dismiss();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalhe_pedido, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            case R.id.pedido_flash:
                DBHelper dbHelper = new DBHelper(DetalhePedidoActivity.this);
                dbHelper.openDataBase();
                dbHelper.limpaPedido();
                ItemPedido itemPedidoDB = new ItemPedido();
                for(ItemPedidoWS ip : itensPedidoWS){
                    itemPedidoDB.atualizaCamposWS(ip.getProdutoid(), ip.getQuantidade());
                    dbHelper.adicionaItemPedido(itemPedidoDB);
                }
                dbHelper.close();

                // Pergunta se o jovem que ir pro carrinho
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("Seu pedido foi clonado com sucesso! \n\nDeseja ser redirecionado para o carrinho?");
                builder.setCancelable(false);
                builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(DetalhePedidoActivity.this, ActivityCart.class));
                        overridePendingTransition(R.anim.open_main, R.anim.close_next);
                    }
                });

                builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert = builder.create();
                alert.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
