package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class ActivityAbout extends Activity {

	Button btnChat, btnVerChamados;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
		
		ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Suporte");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

		btnChat = (Button) findViewById(R.id.btnChat);
		btnVerChamados = (Button) findViewById(R.id.btnVerChamados);


		btnChat.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				abreJanelaIniciarChat();
			}
		});

		btnVerChamados.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent chat = new Intent(ActivityAbout.this, ListaChamadosActivity.class);
				startActivity(chat);
			}
		});

	}

	private void abreJanelaIniciarChat(){
		LayoutInflater li = LayoutInflater.from(ActivityAbout.this);
		final View viewJanelinha = li.inflate(R.layout.janelinha_iniciar_chat, null);
		final android.app.AlertDialog janela = new android.app.AlertDialog.Builder(ActivityAbout.this).create();
		janela.setView(viewJanelinha);
		janela.setCanceledOnTouchOutside(false);

		final EditText txtMotivo = (EditText) viewJanelinha.findViewById(R.id.txtMotivo);

		viewJanelinha.findViewById(R.id.btnOK).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(txtMotivo.getText().toString().length() >= 5){
					janela.dismiss();
					Intent chat = new Intent(ActivityAbout.this, ChatActivity.class);
					chat.putExtra("titulo", txtMotivo.getText().toString());
					startActivity(chat);
					overridePendingTransition(R.anim.open_main, R.anim.close_next);
				}else{
					Toast.makeText(ActivityAbout.this, "Digite um motivo válido. (mínimo 5 caracteres)", Toast.LENGTH_SHORT).show();
				}
			}
		});

		viewJanelinha.findViewById(R.id.btnCancelar).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				janela.dismiss();
			}
		});

		janela.show();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			this.finish();
			overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}
}