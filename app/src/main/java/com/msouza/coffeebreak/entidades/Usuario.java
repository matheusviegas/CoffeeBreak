package com.msouza.coffeebreak.entidades;

import java.util.Date;

/**
 * Created by Matheus on 16/07/2017.
 */

public class Usuario {
    private Long id;
    private String nome;
    private String sobrenome;
    private String email;
    private String foto;
    private Date datacadastro;
    private int status;
    private String mensagem;
    private Date datadesativacao;
    private String cpf;
    private String telefone;

    public Usuario(Long id, String nome, String sobrenome, String email, String foto, Date datacadastro, int status, String mensagem, String cpf, String telefone) {
        this.id = id;
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.foto = foto;
        this.datacadastro = datacadastro;
        this.status = status;
        this.mensagem = mensagem;
        this.cpf = cpf;
        this.telefone = telefone;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Date getDatacadastro() {
        return datacadastro;
    }

    public void setDatacadastro(Date datacadastro) {
        this.datacadastro = datacadastro;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean usuarioOK(){
        return (this.id != null && this.nome != null && this.email != null);
    }

    public Date getDatadesativacao() {
        return datadesativacao;
    }

    public void setDatadesativacao(Date datadesativacao) {
        this.datadesativacao = datadesativacao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "nome='" + nome + '\'' +
                ", sobrenome='" + sobrenome + '\'' +
                '}';
    }
}
