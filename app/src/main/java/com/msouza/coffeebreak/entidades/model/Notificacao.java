package com.msouza.coffeebreak.entidades.model;

/**
 * Created by Matheus on 19/09/2017.
 */

public class Notificacao {
    private int id;
    private Long idPedido;
    private String horaNotificacao;
    private String mensagem;
    private int status;
    private String titulo;
    private String foto;
    private Long idChamado;

    public Notificacao() {
    }

    public Notificacao(int id, Long idPedido, String horaNotificacao, String mensagem, int status) {
        this.id = id;
        this.idPedido = idPedido;
        this.horaNotificacao = horaNotificacao;
        this.mensagem = mensagem;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Long idPedido) {
        this.idPedido = idPedido;
    }

    public String getHoraNotificacao() {
        return horaNotificacao;
    }

    public void setHoraNotificacao(String horaNotificacao) {
        this.horaNotificacao = horaNotificacao;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getIdChamado() {
        return idChamado;
    }

    public void setIdChamado(Long idChamado) {
        this.idChamado = idChamado;
    }
}
