package com.msouza.coffeebreak.entidades;

import java.util.Date;

/**
 * Created by Matheus on 27/06/2017.
 */

public class MovimentacaoCredito {
    private Long id;
    private Date datamovimentacao;
    private String descricao;
    private int tipomovimentacao;
    private Double valormovimentado;

    public MovimentacaoCredito(Long id, Date datamovimentacao, String descricao, int tipomovimentacao, Double valormovimentado) {
        this.id = id;
        this.datamovimentacao = datamovimentacao;
        this.descricao = descricao;
        this.tipomovimentacao = tipomovimentacao;
        this.valormovimentado = valormovimentado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDatamovimentacao() {
        return datamovimentacao;
    }

    public void setDatamovimentacao(Date datamovimentacao) {
        this.datamovimentacao = datamovimentacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getTipomovimentacao() {
        return tipomovimentacao;
    }

    public void setTipomovimentacao(int tipomovimentacao) {
        this.tipomovimentacao = tipomovimentacao;
    }

    public Double getValormovimentado() {
        return valormovimentado;
    }

    public void setValormovimentado(Double valormovimentado) {
        this.valormovimentado = valormovimentado;
    }

    @Override
    public String toString() {
        return "MovimentacaoCredito{" +
                "id=" + id +
                ", datamovimentacao=" + datamovimentacao +
                ", descricao='" + descricao + '\'' +
                ", tipomovimentacao=" + tipomovimentacao +
                ", valormovimentado=" + valormovimentado +
                '}';
    }
}
