package com.msouza.coffeebreak.entidades;

public class RetornoCount {
    private int total;

    public RetornoCount(int total) {
        this.total = total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
