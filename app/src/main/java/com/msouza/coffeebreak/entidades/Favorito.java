package com.msouza.coffeebreak.entidades;

public class Favorito {
    private Long id;
    private Long produtoid;
    private Double valorUnitario;
    private String nome;

    public Favorito(Long id, Long produtoid, Double valorUnitario, String nome) {
        this.id = id;
        this.produtoid = produtoid;
        this.valorUnitario = valorUnitario;
        this.nome = nome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProdutoid() {
        return produtoid;
    }

    public void setProdutoid(Long produtoid) {
        this.produtoid = produtoid;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
