package com.msouza.coffeebreak.entidades;

import java.util.Date;

/**
 * Created by Matheus on 28/09/2017.
 */

public class PedidoWS {
    private Long id;
    private int status;
    private int pago;
    private Double valortotal;
    private int tipopagamento;
    private int desconto;
    private Date datapedido;
    private String observacao;
    private Date horaretirada;
    private String motivo;

    public PedidoWS(Long id, int status, int pago, Double valortotal, int tipopagamento, int desconto, Date datapedido, String observacao, Date horaretirada, String motivo) {
        this.id = id;
        this.status = status;
        this.pago = pago;
        this.valortotal = valortotal;
        this.tipopagamento = tipopagamento;
        this.desconto = desconto;
        this.datapedido = datapedido;
        this.observacao = observacao;
        this.horaretirada = horaretirada;
        this.motivo = motivo;
    }

    public PedidoWS(Double valortotal, int desconto) {
        this.valortotal = valortotal;
        this.desconto = desconto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPago() {
        return pago;
    }

    public void setPago(int pago) {
        this.pago = pago;
    }

    public Double getValortotal() {
        return valortotal;
    }

    public void setValortotal(Double valortotal) {
        this.valortotal = valortotal;
    }

    public int getTipopagamento() {
        return tipopagamento;
    }

    public void setTipopagamento(int tipopagamento) {
        this.tipopagamento = tipopagamento;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public Date getDatapedido() {
        return datapedido;
    }

    public void setDatapedido(Date datapedido) {
        this.datapedido = datapedido;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Date getHoraretirada() {
        return horaretirada;
    }

    public void setHoraretirada(Date horaretirada) {
        this.horaretirada = horaretirada;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }
}

/*
*
* */