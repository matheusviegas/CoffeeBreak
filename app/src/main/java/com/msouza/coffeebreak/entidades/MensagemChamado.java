package com.msouza.coffeebreak.entidades;

import java.util.Date;

public class MensagemChamado {

    private Long id;
    private Long chamadoid;
    private int adminenviou;
    private String mensagem;
    private Date dataenvio;

    public MensagemChamado(Long id, Long chamadoid, int adminenviou, String mensagem, Date dataenvio) {
        this.id = id;
        this.chamadoid = chamadoid;
        this.adminenviou = adminenviou;
        this.mensagem = mensagem;
        this.dataenvio = dataenvio;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChamadoid() {
        return chamadoid;
    }

    public void setChamadoid(Long chamadoid) {
        this.chamadoid = chamadoid;
    }

    public int getAdminenviou() {
        return adminenviou;
    }

    public void setAdminenviou(int adminenviou) {
        this.adminenviou = adminenviou;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public Date getDataenvio() {
        return dataenvio;
    }

    public void setDataenvio(Date dataenvio) {
        this.dataenvio = dataenvio;
    }
}
