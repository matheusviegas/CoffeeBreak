package com.msouza.coffeebreak.entidades;

/**
 * Created by msouza on 21/10/17.
 */

public enum StatusUsuario {
    ATIVO("Login efetuado com sucesso!"),
    CONTA_DESATIVADA("Sua conta foi desativada. É necessário reativa-la para voltar a usar nossos serviços."),
    CONTA_EXCLUIDA_PERMANENTEMENTE("Dados incorretos.");

    private final String mensagem;

    StatusUsuario(String m){
        this.mensagem = m;
    }

    public String getMensagem() {
        return mensagem;
    }
}
