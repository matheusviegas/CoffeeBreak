package com.msouza.coffeebreak.entidades.model;

import com.msouza.coffeebreak.entidades.ItemPedidoDecorator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Matheus on 16/09/2017.
 */

public class PedidoDecorator {
    private int tipopagamento;
    private int desconto;
    private Long cliente;
    private String observacao;
    private Double total;
    private Date retirada;
    private List<ItemPedidoDecorator> itens;

    public int getTipopagamento() {
        return tipopagamento;
    }

    public void setTipopagamento(int tipopagamento) {
        this.tipopagamento = tipopagamento;
    }

    public int getDesconto() {
        return desconto;
    }

    public void setDesconto(int desconto) {
        this.desconto = desconto;
    }

    public Long getCliente() {
        return cliente;
    }

    public void setCliente(Long cliente) {
        this.cliente = cliente;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Date getRetirada() {
        return retirada;
    }

    public void setRetirada(Date retirada) {
        this.retirada = retirada;
    }

    public List<ItemPedidoDecorator> getItens() {
        return itens;
    }

    public void setItens(List<ItemPedidoDecorator> itens) {
        this.itens = itens;
    }

    public String toJsonString(){
        String ret = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(this.retirada);
        String it = "";

        for(ItemPedidoDecorator i : this.itens){
            it += "\"" + i.getProdutoid() + "\":\"" + i.getQuantidade() + "\",";
        }
        it = it.substring(0, it.length()-1);

        String preco = String.format("%.2f", this.total);
        preco = preco.replaceAll(",", ".");

        String json = "{\"observacao\": \"%s\", " +
                "\"tipopagamento\": %d," +
                "\"desconto\": %d," +
                "\"total\" : %s," +
                "\"retirada\": \"%s\", " +
                "\"itens\": {%s}, " +
                "\"cliente\": %d}";

        return String.format(json, this.observacao, this.tipopagamento, this.desconto, preco, ret, it, this.cliente);
    }
}


/*
* {"observacao": "Nada a declarar",
"tipopagamento": 2,
"desconto": 0,
"total" : 125.5,
"retirada": "2017-01-01 22:53:00", "itens": { "14": "5","15": "1", "16": "2"}, "cliente": 12}
* */