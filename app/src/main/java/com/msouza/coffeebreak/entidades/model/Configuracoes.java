package com.msouza.coffeebreak.entidades.model;

/**
 * Created by msouza on 08/10/17.
 */

public class Configuracoes {
    private boolean notificacoes;
    private boolean promocoes;

    public Configuracoes(boolean notificacoes, boolean promocoes) {
        this.notificacoes = notificacoes;
        this.promocoes = promocoes;
    }

    public boolean isNotificacoes() {
        return notificacoes;
    }

    public void setNotificacoes(boolean notificacoes) {
        this.notificacoes = notificacoes;
    }

    public boolean isPromocoes() {
        return promocoes;
    }

    public void setPromocoes(boolean promocoes) {
        this.promocoes = promocoes;
    }
}
