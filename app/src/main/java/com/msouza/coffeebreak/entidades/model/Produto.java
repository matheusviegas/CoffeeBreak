package com.msouza.coffeebreak.entidades.model;

/**
 * Created by Matheus on 08/09/2017.
 */

public class Produto {
    private Long id;
    private String nome;
    private String descricao;
    private String foto;
    private Long categoriaid;
    private int tempoPreparo;
    private int ativo;
    private Double valorUnitario;
    private Double valorAntigo;
    private int promocao;

    public Produto(Long id, String nome, String descricao, String foto, Long categoriaid, int tempoPreparo, int ativo, Double valorUnitario, Double valorAntigo, int promocao) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.foto = foto;
        this.categoriaid = categoriaid;
        this.tempoPreparo = tempoPreparo;
        this.ativo = ativo;
        this.valorUnitario = valorUnitario;
        this.valorAntigo = valorAntigo;
        this.promocao = promocao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public Long getCategoriaid() {
        return categoriaid;
    }

    public void setCategoriaid(Long categoriaid) {
        this.categoriaid = categoriaid;
    }

    public int getTempoPreparo() {
        return tempoPreparo;
    }

    public void setTempoPreparo(int tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }

    public int getAtivo() {
        return ativo;
    }

    public void setAtivo(int ativo) {
        this.ativo = ativo;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public Double getValorAntigo() {
        return valorAntigo;
    }

    public void setValorAntigo(Double valorAntigo) {
        this.valorAntigo = valorAntigo;
    }

    public int getPromocao() {
        return promocao;
    }

    public void setPromocao(int promocao) {
        this.promocao = promocao;
    }

    @Override
    public String toString() {
        return "Produto{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", valorUnitario=" + valorUnitario +
                '}';
    }

    public boolean isDisponivel(){
        return this.ativo == 1;
    }
}
