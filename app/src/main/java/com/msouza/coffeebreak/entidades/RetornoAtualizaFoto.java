package com.msouza.coffeebreak.entidades;

public class RetornoAtualizaFoto {
    private String status;
    private String mensagem;
    private String urlNovaFoto;

    public RetornoAtualizaFoto(String status, String mensagem, String urlNovaFoto) {
        this.status = status;
        this.mensagem = mensagem;
        this.urlNovaFoto = urlNovaFoto;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getUrlNovaFoto() {
        return urlNovaFoto;
    }

    public void setUrlNovaFoto(String urlNovaFoto) {
        this.urlNovaFoto = urlNovaFoto;
    }

    @Override
    public String toString() {
        return "RetornoAtualizaFoto{" +
                "status='" + status + '\'' +
                ", mensagem='" + mensagem + '\'' +
                ", urlNovaFoto='" + urlNovaFoto + '\'' +
                '}';
    }
}
