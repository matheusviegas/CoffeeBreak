package com.msouza.coffeebreak.entidades;

import java.util.Date;

public class PedidoAberto {
    private Long numero;
    private int status;
    private Date dataPedido;

    public PedidoAberto(Long numero, int status, Date dataPedido) {
        this.numero = numero;
        this.status = status;
        this.dataPedido = dataPedido;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getDataPedido() {
        return dataPedido;
    }

    public void setDataPedido(Date dataPedido) {
        this.dataPedido = dataPedido;
    }
}
