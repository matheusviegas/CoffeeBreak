package com.msouza.coffeebreak.entidades.model;

/**
 * Created by Matheus on 16/09/2017.
 */

public class PedidoStatus {
    private Long id;
    private int status;

    public PedidoStatus(Long id, int status) {
        this.id = id;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusTexto(){
        switch (this.status){
            case 0:
                return "Em Rascunho";
            case 1:
                return "Aguardando Aprovação";
            case 2:
                return "Pedido em Produção";
            case 3:
                return "Pedido Pronto";
            case 4:
                return "Pedido Cancelado";
            default:
                return "Status Desconhecido";
        }
    }
}
