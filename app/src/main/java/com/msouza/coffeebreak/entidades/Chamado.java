package com.msouza.coffeebreak.entidades;

import java.util.Date;

/**
 * Created by Matheus on 26/09/2017.
 */

public class Chamado {
    private Long id;
    private String titulo;
    private Long usuarioid;
    private Date data_abertura;
    private Date data_fechamento;

    public Chamado(Long id, String titulo, Long usuarioid, Date data_abertura, Date data_fechamento) {
        this.id = id;
        this.titulo = titulo;
        this.usuarioid = usuarioid;
        this.data_abertura = data_abertura;
        this.data_fechamento = data_fechamento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Long getUsuarioid() {
        return usuarioid;
    }

    public void setUsuarioid(Long usuarioid) {
        this.usuarioid = usuarioid;
    }

    public Date getData_abertura() {
        return data_abertura;
    }

    public void setData_abertura(Date data_abertura) {
        this.data_abertura = data_abertura;
    }

    public Date getData_fechamento() {
        return data_fechamento;
    }

    public void setData_fechamento(Date data_fechamento) {
        this.data_fechamento = data_fechamento;
    }
}
