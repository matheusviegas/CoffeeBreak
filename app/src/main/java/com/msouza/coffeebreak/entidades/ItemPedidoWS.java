package com.msouza.coffeebreak.entidades;

/**
 * Created by Matheus on 28/09/2017.
 */

public class ItemPedidoWS {
    private Long produtoid;
    private String nome;
    private int quantidade;
    private Double valor;

    public ItemPedidoWS(Long produtoid, String nome, int quantidade, Double valor) {
        this.produtoid = produtoid;
        this.nome = nome;
        this.quantidade = quantidade;
        this.valor = valor;
    }

    public Long getProdutoid() {
        return produtoid;
    }

    public void setProdutoid(Long produtoid) {
        this.produtoid = produtoid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
