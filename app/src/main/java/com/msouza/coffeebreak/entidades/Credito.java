package com.msouza.coffeebreak.entidades;

/**
 * Created by Matheus on 27/06/2017.
 */

public class Credito {
    private Long id;
    private Double total;

    public Credito(Long id, Double total) {
        this.id = id;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
