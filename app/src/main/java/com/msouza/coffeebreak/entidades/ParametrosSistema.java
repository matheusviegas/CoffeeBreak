package com.msouza.coffeebreak.entidades;

public class ParametrosSistema {
    private Integer versaoBanco;
    private Integer versaoApp;
    private Integer desconto;

    public ParametrosSistema(Integer versaoBanco, Integer versaoApp, Integer desconto) {
        this.versaoBanco = versaoBanco;
        this.versaoApp = versaoApp;
        this.desconto = desconto;
    }

    public Integer getVersaoBanco() {
        return versaoBanco;
    }

    public void setVersaoBanco(Integer versaoBanco) {
        this.versaoBanco = versaoBanco;
    }

    public Integer getVersaoApp() {
        return versaoApp;
    }

    public void setVersaoApp(Integer versaoApp) {
        this.versaoApp = versaoApp;
    }

    public Integer getDesconto() {
        return desconto;
    }

    public void setDesconto(Integer desconto) {
        this.desconto = desconto;
    }
}
