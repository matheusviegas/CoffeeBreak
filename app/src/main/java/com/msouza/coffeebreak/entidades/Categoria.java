package com.msouza.coffeebreak.entidades;

/**
 * Created by Matheus on 03/07/2017.
 */

public class Categoria {
    private Long id;
    private String nome;
    private String foto;

    public Categoria(Long id, String nome, String foto) {
        this.id = id;
        this.nome = nome;
        this.foto = foto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
