package com.msouza.coffeebreak.entidades;

/**
 * Created by msouza on 07/10/17.
 */

public enum TipoNotificacao {
    ATUALIZACAO_PEDIDO, AVISO, CONFIRMACAO_RECEBIMENTO_SUGESTAO, CHAMADO_ENCERRADO, MENSAGEM_CHAMADO, DEPOSITO;
}
