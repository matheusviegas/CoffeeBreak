package com.msouza.coffeebreak.entidades;

/**
 * Created by Matheus on 10/09/2017.
 */

public class ItemPedidoDecorator {
    private Long id;
    private Long produtoid;
    private String nome;
    private Double valorUnitario;
    private int quantidade;
    private int tempopreparo;

    public ItemPedidoDecorator(Long id, Long produtoid, String nome, Double valorUnitario, int quantidade, int tempopreparo) {
        this.id = id;
        this.produtoid = produtoid;
        this.nome = nome;
        this.valorUnitario = valorUnitario;
        this.quantidade = quantidade;
        this.tempopreparo = tempopreparo;
    }

    public ItemPedidoDecorator(Long produtoid, String nome, Double valorUnitario, int quantidade) {
        this.produtoid = produtoid;
        this.nome = nome;
        this.valorUnitario = valorUnitario;
        this.quantidade = quantidade;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getProdutoid() {
        return produtoid;
    }

    public void setProdutoid(Long produtoid) {
        this.produtoid = produtoid;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(Double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getTempopreparo() {
        return tempopreparo;
    }

    public void setTempopreparo(int tempopreparo) {
        this.tempopreparo = tempopreparo;
    }
}
