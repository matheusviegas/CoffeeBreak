package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.chat.Mensagem;
import com.msouza.coffeebreak.chat.TipoMensagem;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class SugestaoActivity extends Activity {

    EditText txtSugestao;
    RatingBar nota;
    Button btnEnviar;
    ProgressDialog janelaCarregando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sugestao);


        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Envie uma Sugestão");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        txtSugestao = (EditText) findViewById(R.id.txtSugestao);
        nota = (RatingBar) findViewById(R.id.nota);

     //   Drawable stars = nota.getProgressDrawable(); stars.setTint(Color.RED);

        btnEnviar = (Button) findViewById(R.id.btnEnviar);

        janelaCarregando = new ProgressDialog(this);
        janelaCarregando.setMessage("Enviando sugestão..");
        janelaCarregando.setCanceledOnTouchOutside(false);


        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtSugestao.getText().toString().length() > 15){
                    janelaCarregando.show();
                    Sessao sessao = new Sessao(SugestaoActivity.this);
                    RequestParams params = new RequestParams();
                    params.put("usuarioid", Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
                    params.put("descricao", txtSugestao.getText().toString());
                    params.put("nota", (int) nota.getRating());
                    enviaSugestaoWS(params);
                }else{
                    Toast.makeText(SugestaoActivity.this, "O texto da sugestão deve ter no mínimo 15 caracteres.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private void enviaSugestaoWS(RequestParams params){
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.ENVIAR_SUGESTAO, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                janelaCarregando.dismiss();
                Toast.makeText(SugestaoActivity.this, "Sugestão enviada com sucesso!.", Toast.LENGTH_LONG).show();
                txtSugestao.setText("");
                nota.setRating(0);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
