package com.msouza.coffeebreak;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class AvaliarChamadoActivity extends Activity {

    EditText txtObservacoes;
    Button btnSalvar;
    RatingBar nota;
    RadioGroup grupo;
    ProgressDialog carregando;
    Long idChamado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avaliar_chamado);

        txtObservacoes = (EditText) findViewById(R.id.txtObservacoes);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        nota = (RatingBar) findViewById(R.id.nota);
        grupo = (RadioGroup) findViewById(R.id.radioGroup);

        carregando = new ProgressDialog(this);
        carregando.setMessage("Enviando avaliação..");
        carregando.setCanceledOnTouchOutside(false);

        Intent intent = getIntent();
        idChamado = intent.getLongExtra("id", 1l);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validar()){
                    RequestParams params = new RequestParams();
                    params.put("id", idChamado);
                    params.put("nota", (int) nota.getRating());
                    params.put("obs", txtObservacoes.getText().toString());
                    params.put("resolveu", grupo.getCheckedRadioButtonId() == R.id.rdSim ? 1 : 0);
                    enviaAvaliacaoWS(params);
                }
            }
        });
    }

    private  boolean validar(){
        if(nota.getRating() == 0){
            Toast.makeText(this, "Avalie com uma nota de 1 a 5", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void enviaAvaliacaoWS(RequestParams params){
        carregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.ENVIA_AVALIACAO_WS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
                Toast.makeText(AvaliarChamadoActivity.this, retorno.getMensagem(), Toast.LENGTH_SHORT).show();
                carregando.dismiss();
                startActivity(new Intent(AvaliarChamadoActivity.this, MainActivity.class));
            }

        });
    }
}
