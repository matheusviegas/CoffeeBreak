package com.msouza.coffeebreak;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.Categoria;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.model.Produto;

import cz.msebera.android.httpclient.Header;

public class ActivityMenuList extends Activity {
	
	ListView listMenu;
	ProgressBar prgLoading;
	//TextView txtTitle;
	EditText edtKeyword;
	ImageButton btnSearch;
	TextView txtAlert;
	
	// declare static variable to store tax and currency symbol
	static double Tax;
	static String Currency;
	
	// declare adapter object to create custom menu list
	AdapterMenuList mla;
	
	// create arraylist variables to store data from server
	static ArrayList<Long> Menu_ID = new ArrayList<Long>();
	static ArrayList<String> Menu_name = new ArrayList<String>();
	static ArrayList<Double> Menu_price = new ArrayList<Double>();
	static ArrayList<String> Menu_image = new ArrayList<String>();

	static List<Produto> listaProdutos = new ArrayList<>();
	
	int IOConnect = 0;
	Long idCategoria;
	String nomeCategoria;
	String stringBusca = null;

	DBHelper dbHelper = new DBHelper(this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_list);
        
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Produtos");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        listMenu = (ListView) findViewById(R.id.listMenu);
        edtKeyword = (EditText) findViewById(R.id.edtKeyword);
        btnSearch = (ImageButton) findViewById(R.id.btnSearch);
        txtAlert = (TextView) findViewById(R.id.txtAlert);

		// pego dados da categoria passado por intent
        Intent iGet = getIntent();
        idCategoria = iGet.getLongExtra("id_categoria", 12l);
        nomeCategoria = iGet.getStringExtra("nome_categoria");
		bar.setTitle(nomeCategoria);

		// Instancia o Adapter dos Produtos
		mla = new AdapterMenuList(ActivityMenuList.this);

		// busca por categoria
//		dbHelper.openDataBase();
//		listaProdutos.clear();
//		List<Produto> prod = dbHelper.buscaProdutos(idCategoria, DBHelper.TipoBuscaProduto.CATEGORIA);
//		if(prod.isEmpty()){
//			listaProdutosWS();
//		}else{
//			listaProdutos.addAll(prod);
//			atualizaTela();
//			System.out.println("BUSCOU PRODUTOS NO BANCO");
//		}
//		dbHelper.close();

		// SO BUSCA NO BANCO
		dbHelper.openDataBase();
		listaProdutos.clear();
		listaProdutos.addAll(dbHelper.buscaProdutos(idCategoria, DBHelper.TipoBuscaProduto.CATEGORIA));
		dbHelper.close();
		atualizaTela();




		btnSearch.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// get keyword and send it to server
				try {
					stringBusca = URLEncoder.encode(edtKeyword.getText().toString(), "utf-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				IOConnect = 0;
    			listMenu.invalidateViews();
    			clearData();
				listaProdutosWS();
			}
		});
		
		listMenu.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				// go to menu detail page
				Intent iDetail = new Intent(ActivityMenuList.this, ActivityMenuDetail.class);
				iDetail.putExtra("id_produto", listaProdutos.get(position).getId());
				startActivity(iDetail);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
			}
		});       
        
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu_category, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.cart:
			// refresh action
			Intent iMyOrder = new Intent(ActivityMenuList.this, ActivityCart.class);
			startActivity(iMyOrder);
			overridePendingTransition (R.anim.open_next, R.anim.close_next);
			return true;
			
		case R.id.refresh:
			IOConnect = 0;
			listMenu.invalidateViews();
			clearData();
			listaProdutosWS();
			return true;			
			
		case android.R.id.home:
            // app icon in action bar clicked; go home
        	this.finish();
        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// clear arraylist variables before used
    void clearData(){
		listaProdutos.clear();
    }
    

    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	//mla.imageLoader.clearCache();
    	listMenu.setAdapter(null);
    	super.onDestroy();
    }
	 
    
    @Override
	public void onConfigurationChanged(final Configuration newConfig)
	{
	    // Ignore orientation change to keep activity from restarting
	    super.onConfigurationChanged(newConfig);
	}
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }


	public void listaProdutosWS(){

		if(!prgLoading.isShown()){
			prgLoading.setVisibility(0);
			txtAlert.setVisibility(8);
		}

		clearData();

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.LISTA_TODOS_PRODUTOS, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

				if(resposta.length() > 0) {
					Type listType = new TypeToken<ArrayList<Produto>>(){}.getType();
					List<Produto> produtos = gson.fromJson(resposta.toString(), listType);

					dbHelper.openDataBase();
					for(Produto p : produtos){
						listaProdutos.add(p);
						dbHelper.adicionaProduto(p);
					}
					dbHelper.close();
				}

				atualizaTela();
			}
		});
	}


	public void listaProdutosWSANTIGO(){

		if(!prgLoading.isShown()){
			prgLoading.setVisibility(0);
			txtAlert.setVisibility(8);
		}

		clearData();

		AsyncHttpClient client = new AsyncHttpClient();
		String url = Constant.LISTA_PRODUTOS + idCategoria + ((stringBusca != null && !stringBusca.equals("")) ? "/" + stringBusca : "");

		client.get(url, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

				if(resposta.length() > 0) {
					Type listType = new TypeToken<ArrayList<Produto>>(){}.getType();
					List<Produto> produtos = gson.fromJson(resposta.toString(), listType);

					dbHelper.openDataBase();
					for(Produto p : produtos){
						listaProdutos.add(p);
						dbHelper.adicionaProduto(p);
					}
					dbHelper.close();
				}

				atualizaTela();
			}
		});
	}

	public void atualizaTela(){
		prgLoading.setVisibility(8);
		if((listaProdutos.size() > 0)){
			listMenu.setVisibility(0);
			listMenu.setAdapter(mla);
		}else {
			txtAlert.setVisibility(0);
		}
	}



}
