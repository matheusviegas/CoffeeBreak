package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmacaoRecebimentoSugestaoActivity extends Activity {

    TextView txtMensagem;
    Button fechar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacao_recebimento_sugestao);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Obrigado!");
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);

        txtMensagem = (TextView) findViewById(R.id.mensagem);
        fechar = (Button) findViewById(R.id.btnFechar);

        Intent intent = getIntent();
        txtMensagem.setText(intent.getStringExtra("mensagem") + "\n\nRecebemos sua sugestão." + "\n\nObrigado por ajudar a melhorar nosso serviço.");

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
