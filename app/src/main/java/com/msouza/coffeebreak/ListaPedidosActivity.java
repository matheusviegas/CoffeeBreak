package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.PedidoWS;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ListaPedidosActivity extends Activity {

    List<PedidoWS> pedidos = new ArrayList<>();
    ListView listaPedidos;
    private static AdapterListaPedidos adapter;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    ProgressDialog janelaCarregando;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pedidos);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Pedidos Realizados");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        janelaCarregando = new ProgressDialog(this);
        janelaCarregando.setMessage("Buscando pedidos..");
        janelaCarregando.setCanceledOnTouchOutside(false);

        listaPedidos =(ListView)findViewById(R.id.listaPedidos);

        Sessao sessao = new Sessao(this);
        listaPedidosWS(Long.parseLong(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));

        listaPedidos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PedidoWS pedido = pedidos.get(position);

                Intent detalhe = new Intent(ListaPedidosActivity.this, DetalhePedidoActivity.class);
                detalhe.putExtra("id", pedido.getId());
                detalhe.putExtra("pago", pedido.getPago());
                detalhe.putExtra("valortotal", pedido.getValortotal());
                detalhe.putExtra("tipopagamento", pedido.getTipopagamento());
                detalhe.putExtra("status", pedido.getStatus());
                detalhe.putExtra("desconto", pedido.getDesconto());
                detalhe.putExtra("data", sdf.format(pedido.getDatapedido()));
                detalhe.putExtra("obs", pedido.getObservacao());
                detalhe.putExtra("retirada", sdf.format(pedido.getHoraretirada()));
                detalhe.putExtra("motivo", pedido.getMotivo());
                startActivity(detalhe);
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
            }
        });
    }


    private void listaPedidosWS(Long codUsuario){
        janelaCarregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.LISTA_PEDIDOS_BY_USUARIO + "/" + codUsuario, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                Type listType = new TypeToken<ArrayList<PedidoWS>>(){}.getType();
                List<PedidoWS> pedidosWS = gson.fromJson(resposta.toString(), listType);
                pedidos = pedidosWS;
                adapter= new AdapterListaPedidos(pedidos, getApplicationContext());
                listaPedidos.setAdapter(adapter);
                janelaCarregando.dismiss();
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
