package com.msouza.coffeebreak;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.Categoria;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;

import cz.msebera.android.httpclient.Header;

public class ActivityCategoryList extends Activity {
	
	GridView listCategory;
	ProgressBar prgLoading;
	TextView txtAlert;

	AdapterCategoryList cla;
	static List<Categoria> listaCategorias = new ArrayList<>();

	int IOConnect = 0;

	DBHelper dbHelper;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_list);
        
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        bar.setTitle("Categorias");

		dbHelper = new DBHelper(this);
        
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        listCategory = (GridView) findViewById(R.id.listCategory);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        
        cla = new AdapterCategoryList(ActivityCategoryList.this);

        // Busca Categorias do WebService e do Banco
		/*listaCategorias.clear();
		dbHelper.openDataBase();
		if(dbHelper.temCategoriasNoBanco()){
			listaCategorias.addAll(dbHelper.buscaCategorias());
			System.out.println("BUSCOU NO BANCO");
			atualizaTela();
		}else{
			buscaCategoriasWS();
			System.out.println("BUSCOU NO WEBSERVICE");
		}
		dbHelper.close();*/

		listaCategorias.clear();
		dbHelper.openDataBase();
		listaCategorias.addAll(dbHelper.buscaCategorias());
		dbHelper.close();
		atualizaTela();

		listCategory.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				// go to menu page
				Intent iMenuList = new Intent(ActivityCategoryList.this, ActivityMenuList.class);
				iMenuList.putExtra("id_categoria", listaCategorias.get(position).getId());
				iMenuList.putExtra("nome_categoria", listaCategorias.get(position).getNome());
				startActivity(iMenuList);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
			}
		});
        
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_category, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.cart:
			// refresh action
			Intent iMyOrder = new Intent(ActivityCategoryList.this, ActivityCart.class);
			startActivity(iMyOrder);
			overridePendingTransition (R.anim.open_next, R.anim.close_next);
			return true;
			
		case R.id.refresh:
			IOConnect = 0;
			listCategory.invalidateViews();
			clearData();
			buscaCategoriasWS();
			return true;
			
		case android.R.id.home:
            // app icon in action bar clicked; go home
        	this.finish();
        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
    
    // clear arraylist variables before used
    void clearData(){
		listaCategorias.clear();
		dbHelper.openDataBase();
		dbHelper.deletaTodasCategorias();
		dbHelper.close();
		/*
    	Category_ID.clear();
    	Category_name.clear();
    	Category_image.clear();*/
    }
    

    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	//cla.imageLoader.clearCache();
    	listCategory.setAdapter(null);
    	super.onDestroy();
    }

    
    @Override
	public void onConfigurationChanged(final Configuration newConfig)
	{
	    // Ignore orientation change to keep activity from restarting
	    super.onConfigurationChanged(newConfig);
	}
    
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }



	public void buscaCategoriasWS(){
		clearData();

		AsyncHttpClient client = new AsyncHttpClient();

		client.get(Constant.LISTA_CATEGORIAS, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

				Type listType = new TypeToken<ArrayList<Categoria>>(){}.getType();
				List<Categoria> categorias = gson.fromJson(resposta.toString(), listType);

				dbHelper.openDataBase();
				for(Categoria cat : categorias){
					listaCategorias.add(cat);
					dbHelper.adicionaCategoria(cat);
				}
				dbHelper.close();


			/*	for(Categoria cat : categorias){
					Category_ID.add(cat.getId());
					Category_name.add(cat.getNome());
					Category_image.add(cat.getFoto());
				}
*/

				/*prgLoading.setVisibility(8);
				if((listaCategorias.size() > 0) && (IOConnect == 0)){
					listCategory.setVisibility(0);
					listCategory.setAdapter(cla);
				}else {
					txtAlert.setVisibility(0);
				}*/

				atualizaTela();
			}
		});
	}

	public void atualizaTela(){
		prgLoading.setVisibility(8);
		if((listaCategorias.size() > 0) && (IOConnect == 0)){
			listCategory.setVisibility(0);
			listCategory.setAdapter(cla);
		}else {
			txtAlert.setVisibility(0);
		}
	}



}
