package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.chat.Mensagem;
import com.msouza.coffeebreak.entidades.Chamado;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MensagemChamado;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ListaChamadosActivity extends Activity {

    List<Chamado> chamados = new ArrayList<>();
    ListView listaChamados;
    private static AdapterListaChamados adapter;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
    ProgressDialog janelaCarregando;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_chamados);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Chamados Realizados");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        janelaCarregando = new ProgressDialog(this);
        janelaCarregando.setMessage("Buscando chamados..");
        janelaCarregando.setCanceledOnTouchOutside(false);

        listaChamados =(ListView)findViewById(R.id.listaChamados);

        Sessao sessao = new Sessao(this);
        listaChamadosWS(Long.parseLong(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));


       /* pedidos = new ArrayList<>();
        pedidos.add(new Chamado(10l, "Chamado 1", 25l, new Date(), new Date()));
        pedidos.add(new Chamado(20l, "Chamado 2", 25l, new Date(), new Date()));
        pedidos.add(new Chamado(30l, "Chamado 3", 25l, new Date(), new Date()));
        pedidos.add(new Chamado(40l, "Chamado 4", 25l, new Date(), new Date()));
*/


        listaChamados.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Chamado chamado = chamados.get(position);
               // Toast.makeText(ListaChamadosActivity.this, String.format("Chamado Nº: %d - %s", chamado.getId(), chamado.getTitulo()), Toast.LENGTH_SHORT).show();

                Intent chat = new Intent(ListaChamadosActivity.this, ChatActivity.class);
                chat.putExtra("idChamado", chamado.getId());
                chat.putExtra("dataAbertura", sdf.format(chamado.getData_abertura()));
                if(chamado.getData_fechamento() != null){
                    chat.putExtra("dataFechamento", sdf.format(chamado.getData_fechamento()));
                }
                chat.putExtra("nomeProduto", chamado.getTitulo());
                startActivity(chat);
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
            }
        });
    }


    private void listaChamadosWS(Long codUsuario){
        janelaCarregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.LISTA_CHAMADOS_BY_USUARIO + "/" + codUsuario, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                Type listType = new TypeToken<ArrayList<Chamado>>(){}.getType();
                List<Chamado> chamadosWS = gson.fromJson(resposta.toString(), listType);
                chamados = chamadosWS;
                adapter= new AdapterListaChamados(chamados, getApplicationContext());
                listaChamados.setAdapter(adapter);
                janelaCarregando.dismiss();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
