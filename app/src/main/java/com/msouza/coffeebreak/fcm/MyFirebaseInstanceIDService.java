package com.msouza.coffeebreak.fcm;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.msouza.coffeebreak.Sessao;
import com.msouza.coffeebreak.entidades.model.Configuracoes;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static String TAG = "Registration";

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();

        FirebaseMessaging.getInstance().subscribeToTopic("avisos");
        System.out.println("GEROU NOVO TOKEN E INSCREVEU NO TOPICO AVISOS");

        // Inicializa os parâmetros de configurações default
        Sessao sessao = new Sessao(this);
        Configuracoes config = new Configuracoes(true, true);
        sessao.setConfiguracoes(config);
    }
}
