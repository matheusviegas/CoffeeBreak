package com.msouza.coffeebreak.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import com.msouza.coffeebreak.AvaliarChamadoActivity;
import com.msouza.coffeebreak.AvisoDetalhadoActivity;
import com.msouza.coffeebreak.BuildConfig;
import com.msouza.coffeebreak.ConfirmacaoRecebimentoSugestaoActivity;
import com.msouza.coffeebreak.DBHelper;
import com.msouza.coffeebreak.MainActivity;
import com.msouza.coffeebreak.R;
import com.msouza.coffeebreak.Sessao;
import com.msouza.coffeebreak.entidades.TipoNotificacao;
import com.msouza.coffeebreak.entidades.model.Configuracoes;
import com.msouza.coffeebreak.entidades.model.Notificacao;
import com.msouza.coffeebreak.timeline.TimeLineActivity;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyGcmListenerService";

    @Override
    public void onMessageReceived(RemoteMessage message) {

        Sessao sessao = new Sessao(this);
        Configuracoes config = sessao.getConfiguracoes();

        System.out.println("FROM DO FCM");
        System.out.println(message.getFrom());

        Map<String, String> dados = message.getData();
        TipoNotificacao tipoNotificacao = TipoNotificacao.values()[Integer.parseInt(dados.get("tipo"))];
        Notificacao notificacao;

        int id = 0;
        Object obj = message.getData().get("id");
        if (obj != null) {
            id = Integer.valueOf(obj.toString());
        }

        if(Integer.parseInt(dados.get("versaoapp")) != BuildConfig.VERSION_CODE){
            return;
        }

        if(tipoNotificacao.equals(TipoNotificacao.DEPOSITO)){
            notificacao = new Notificacao();
            notificacao.setId(id);
            notificacao.setMensagem(dados.get("mensagem"));
            if(config.isNotificacoes() && sessao.isLogado()) {
                enviarNotificacaoDeposito(notificacao);
            }
            return;
        }else if(tipoNotificacao.equals(TipoNotificacao.CHAMADO_ENCERRADO)){
            notificacao = new Notificacao();
            notificacao.setId(id);
            notificacao.setIdChamado(Long.valueOf(dados.get("id")));
            if(config.isNotificacoes() && sessao.isLogado()) {
                enviarNotificacaoChamadoEncerrado(notificacao);
            }
            return;
        }else  if(tipoNotificacao.equals(TipoNotificacao.MENSAGEM_CHAMADO)){
            notificacao = new Notificacao();
            notificacao.setId(id);
            notificacao.setIdChamado(Long.valueOf(dados.get("id")));
            notificacao.setMensagem(dados.get("mensagem"));

            if(config.isNotificacoes() && sessao.isLogado()) {
                enviarNotificacaoMensagemChamado(notificacao);
            }
            return;
        }else if(tipoNotificacao == TipoNotificacao.ATUALIZACAO_PEDIDO) {
            String image = dados.get("icone");
            String title = dados.get("titulo");
            String text = dados.get("mensagem");
            Long idPedido = Long.parseLong(dados.get("idpedido"));
            int status = Integer.parseInt(dados.get("status"));

            notificacao = new Notificacao(id, idPedido, null, text, status);
            notificacao.setTitulo(getString(R.string.app_name));
            DBHelper banco = new DBHelper(this);
            banco.openDataBase();
            banco.adicionaNotificacao(notificacao);
            banco.close();
        }else{
            notificacao = new Notificacao();
            notificacao.setId(id);
            notificacao.setTitulo(dados.get("titulo"));
            notificacao.setMensagem(dados.get("mensagem"));

            if(dados.containsKey("foto")){
                notificacao.setFoto(dados.get("foto"));
            }
        }

        System.out.println("RECEBEU NOTIFICACAO");
        System.out.println("Titutlo: " + notificacao.getTitulo());
        System.out.println("MSG: " + notificacao.getMensagem());


        if(config.isNotificacoes() && sessao.isLogado()) {
            this.enviarNotificacao(notificacao, tipoNotificacao);
        }
    }


    private void enviarNotificacao(Notificacao notificacao, TipoNotificacao tipo) {
        Intent intent;

        if(tipo.equals(TipoNotificacao.ATUALIZACAO_PEDIDO)) {
            intent = new Intent(this, TimeLineActivity.class);
            intent.putExtra("TEXT", notificacao.getMensagem());
            intent.putExtra("atualizaMenu", true);

            Sessao sessao = new Sessao(this);
            sessao.setPedidoAberto(!notificacao.getMensagem().equals("Seu pedido foi entregue."));
            sessao.setVisualizouNotificacao(false);
        }else if(tipo.equals(TipoNotificacao.AVISO)){
            intent = new Intent(this, AvisoDetalhadoActivity.class);
            intent.putExtra("titulo", notificacao.getTitulo());
            intent.putExtra("mensagem", notificacao.getMensagem());

            if(notificacao.getFoto() != null){
                intent.putExtra("foto", notificacao.getFoto());
            }
        }else{
            try {
                intent = new Intent(this, ConfirmacaoRecebimentoSugestaoActivity.class);
                intent.putExtra("mensagem", URLDecoder.decode(notificacao.getMensagem(), "UTF-8"));
            }catch (UnsupportedEncodingException e){
                e.printStackTrace();
                intent = new Intent();
            }
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {
            Bitmap logoGrande = BitmapFactory.decodeResource(getResources(),R.drawable.logo_big);
            notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setLargeIcon(logoGrande);
            notificationBuilder.setSmallIcon(R.drawable.notificacao);


            if(tipo.equals(TipoNotificacao.ATUALIZACAO_PEDIDO)){
                notificationBuilder.setContentTitle(getString(R.string.app_name));
                notificationBuilder.setContentText(URLDecoder.decode(notificacao.getMensagem(), "UTF-8"));
            }else if(tipo.equals(TipoNotificacao.AVISO)){
                notificationBuilder.setContentTitle("Você tem um novo aviso");
                notificationBuilder.setContentText(URLDecoder.decode(notificacao.getTitulo(), "UTF-8"));
                String mensagem = URLDecoder.decode((notificacao.getMensagem().length() >= 120 ? notificacao.getMensagem().substring(0, 119) + ".." : notificacao.getMensagem()), "UTF-8");
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(mensagem));
            }else{
                notificationBuilder.setContentTitle("Coffee Break");
                notificationBuilder.setContentText("Sua sugestão foi lida");
            }

            notificationBuilder.setAutoCancel(true);
           // notificationBuilder.setSound(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.som)); Som personalizado
            notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            notificationBuilder.setContentIntent(pendingIntent);
            // Teste adicionar vibração e piscar o led ao receber notificação
            notificationBuilder.setVibrate(new long[] {0, 100});
            notificationBuilder.setLights(Color.RED, 3000, 3000);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificacao.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }


    private void enviarNotificacaoChamadoEncerrado(Notificacao notificacao) {
        Intent intent = new Intent(this, AvaliarChamadoActivity.class);
        intent.putExtra("id", notificacao.getIdChamado());

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        Bitmap logoGrande = BitmapFactory.decodeResource(getResources(),R.drawable.logo_big);
        notificationBuilder = new NotificationCompat.Builder(this);
        notificationBuilder.setLargeIcon(logoGrande);
        notificationBuilder.setSmallIcon(R.drawable.notificacao);

        notificationBuilder.setContentTitle("Seu chamado foi encerrado");
        notificationBuilder.setContentText("Clique aqui para avaliar");

        notificationBuilder.setAutoCancel(true);
        // notificationBuilder.setSound(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.som)); Som personalizado
        notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        notificationBuilder.setContentIntent(pendingIntent);
        // Teste adicionar vibração e piscar o led ao receber notificação
        notificationBuilder.setVibrate(new long[] {0, 100});
        notificationBuilder.setLights(Color.RED, 3000, 3000);


        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificacao.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }

    private void enviarNotificacaoMensagemChamado(Notificacao notificacao) {
        Intent intent = new Intent(this, MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {
            Bitmap logoGrande = BitmapFactory.decodeResource(getResources(),R.drawable.logo_big);
            notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setLargeIcon(logoGrande);
            notificationBuilder.setSmallIcon(R.drawable.notificacao);

            notificationBuilder.setContentTitle(getString(R.string.app_name));
            notificationBuilder.setContentText(URLDecoder.decode(notificacao.getMensagem(), "UTF-8"));

            notificationBuilder.setAutoCancel(true);
            // notificationBuilder.setSound(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.som)); Som personalizado
            notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            notificationBuilder.setContentIntent(pendingIntent);
            // Teste adicionar vibração e piscar o led ao receber notificação
            notificationBuilder.setVibrate(new long[] {0, 100});
            notificationBuilder.setLights(Color.RED, 3000, 3000);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificacao.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }

    private void enviarNotificacaoDeposito(Notificacao notificacao) {
        Intent intent = new Intent(this, MainActivity.class);

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {
            Bitmap logoGrande = BitmapFactory.decodeResource(getResources(),R.drawable.logo_big);
            notificationBuilder = new NotificationCompat.Builder(this);
            notificationBuilder.setLargeIcon(logoGrande);
            notificationBuilder.setSmallIcon(R.drawable.notificacao);

            notificationBuilder.setContentTitle(getString(R.string.app_name));
            notificationBuilder.setContentText(URLDecoder.decode(notificacao.getMensagem(), "UTF-8"));

            notificationBuilder.setAutoCancel(true);
            // notificationBuilder.setSound(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.som)); Som personalizado
            notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            notificationBuilder.setContentIntent(pendingIntent);
            // Teste adicionar vibração e piscar o led ao receber notificação
            notificationBuilder.setVibrate(new long[] {0, 100});
            notificationBuilder.setLights(Color.RED, 3000, 3000);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(notificacao.getId(), notificationBuilder.build());
        } else {
            Log.d(TAG, "Não foi possível criar objeto notificationBuilder");
        }
    }

}
