package com.msouza.coffeebreak.timeline.model;

public enum StatusTarefa {
    COMPLETO,
    EM_ANDAMENTO,
    INATIVA;
}
