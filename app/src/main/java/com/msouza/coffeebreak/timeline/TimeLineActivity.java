package com.msouza.coffeebreak.timeline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.msouza.coffeebreak.DBHelper;
import com.msouza.coffeebreak.R;
import com.msouza.coffeebreak.entidades.model.Notificacao;
import com.msouza.coffeebreak.timeline.model.StatusTarefa;
import com.msouza.coffeebreak.timeline.model.TimeLineModel;

import java.util.ArrayList;
import java.util.List;


public class TimeLineActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private TimeLineAdapter mTimeLineAdapter;
    private List<TimeLineModel> mDataList = new ArrayList<>();
    private boolean mWithLinePadding;
    private int statusPedido = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar()!=null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mWithLinePadding = false;

        // Preenche notificacoes
        DBHelper banco = new DBHelper(this);
        banco.openDataBase();
        List<Notificacao> notificacoes = banco.listaNotificacoes();
        Long idPedido = this.preencheNotificacoes(notificacoes);
        banco.close();

        setTitle("Pedido Nº: " + idPedido);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setLayoutManager(getLinearLayoutManager());
        mRecyclerView.setHasFixedSize(true);

        initView();
    }

    private Long preencheNotificacoes(List<Notificacao> notificacoes) {
        Long idPedido = notificacoes.get(0).getIdPedido();
        for(int i = 0; i < notificacoes.size(); i++){
            mDataList.add(new TimeLineModel(notificacoes.get(i).getMensagem(), notificacoes.get(i).getHoraNotificacao(), (i == notificacoes.size() -1) ? StatusTarefa.EM_ANDAMENTO : StatusTarefa.COMPLETO));
        }
        return idPedido;
    }

    private LinearLayoutManager getLinearLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    }

    private void initView() {
       // setDataListItems();
        mTimeLineAdapter = new TimeLineAdapter(mDataList, mWithLinePadding);
        mRecyclerView.setAdapter(mTimeLineAdapter);
    }

    private void setDataListItems(){
        mDataList.add(new TimeLineModel("Pedido enviado para análise", "21:00", StatusTarefa.EM_ANDAMENTO));
        mDataList.add(new TimeLineModel("Pedido aprovado", "21:02", StatusTarefa.COMPLETO));
        mDataList.add(new TimeLineModel("Pedido na fila para produção", "21:05", StatusTarefa.COMPLETO));
        mDataList.add(new TimeLineModel("Pedido em produção", "21:07", StatusTarefa.COMPLETO));
        mDataList.add(new TimeLineModel("Pedido pronto para retirada", "21:15", StatusTarefa.COMPLETO));
        mDataList.add(new TimeLineModel("Pedido entregue", "21:30", StatusTarefa.EM_ANDAMENTO));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Menu
        switch (item.getItemId()) {
            //When home is clicked
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }
}
