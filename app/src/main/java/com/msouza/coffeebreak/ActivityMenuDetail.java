package com.msouza.coffeebreak;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.model.ItemPedido;
import com.msouza.coffeebreak.entidades.model.Produto;
import com.squareup.picasso.Picasso;

import cz.msebera.android.httpclient.Header;

public class ActivityMenuDetail extends Activity {
	
	ImageView imgPreview;
	TextView txtText, txtSubText, txtPrecoUnitario;
	Button btnAdd;
	ScrollView sclDetail;
	ProgressBar prgLoading;
	TextView txtAlert;
	Sessao sessao;

	static DBHelper dbhelper;
	//ImageLoader imageLoader;
	
	Long idProduto;
	int IOConnect = 0;
	Produto produtoSelecionado = null;
	ProgressDialog carregando;
	MenuItem itemMenu = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu_detail);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Detalhes do Produto");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);
        
        imgPreview = (ImageView) findViewById(R.id.imgPreview);
        txtText = (TextView) findViewById(R.id.txtText);
        txtSubText = (TextView) findViewById(R.id.txtSubText);
		txtPrecoUnitario = (TextView) findViewById(R.id.txtPrecoUnitario);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        //btnShare = (Button) findViewById(R.id.btnShare);
        sclDetail = (ScrollView) findViewById(R.id.sclDetail);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
		carregando = new ProgressDialog(this);
		carregando.setMessage("Adicionando aos favoritos..");
		carregando.setCanceledOnTouchOutside(false);
		sessao = new Sessao(ActivityMenuDetail.this);

        // Pega dimensões da tela
        DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int wPix = dm.widthPixels;
		int hPix = wPix / 2 + 50;
		
		// Redimensiona imagem
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(wPix, hPix);
        imgPreview.setLayoutParams(lp);
        
       // imageLoader = new ImageLoader(ActivityMenuDetail.this);
        dbhelper = new DBHelper(this);
		
		// pega id do produto na intent
        Intent iGet = getIntent();
        idProduto = iGet.getLongExtra("id_produto", 0);

		dbhelper.openDataBase();
		List<Produto> prod = dbhelper.buscaProdutos(idProduto, DBHelper.TipoBuscaProduto.ID);
		dbhelper.close();
		if(prod.isEmpty()) {
			buscaProdutoWS();
		}else{
			produtoSelecionado = prod.get(0);
			atualizaTela();
		}

        btnAdd.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// Abre janela pra adicionar item ao pedido
				abreJanelaSelecionarQuantidade();
			}
		});
        
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_detail, menu);

		MenuItem item = menu.findItem(R.id.favoritar);
		dbhelper.openDataBase();
		item.setVisible(!dbhelper.verificaProdutoFavoritado(produtoSelecionado.getId(), Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO))));
		dbhelper.close();
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
	//	itemMenu = menu.findItem(R.id.favoritar);
		return super.onPrepareOptionsMenu(menu);
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.cart:
			// refresh action
			Intent iMyOrder = new Intent(ActivityMenuDetail.this, ActivityCart.class);
			startActivity(iMyOrder);
			overridePendingTransition (R.anim.open_next, R.anim.close_next);
			return true;
			
		case android.R.id.home:
            // app icon in action bar clicked; go home
        	this.finish();
        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
			return true;

			case R.id.favoritar:
				RequestParams params = new RequestParams();
				params.put("produtoid", produtoSelecionado.getId());
				params.put("usuarioid", Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
				adicionarFavoritoWS(params, item);
				dbhelper.openDataBase();
				dbhelper.adicionaFavoritos(produtoSelecionado.getId(), Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
				dbhelper.close();
				return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
		
	}


    void abreJanelaSelecionarQuantidade(){
        final NumberPicker picker = new NumberPicker(ActivityMenuDetail.this);
        picker.setMinValue(1);
        picker.setMaxValue(10);

        final FrameLayout layout = new FrameLayout(ActivityMenuDetail.this);
        layout.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        new AlertDialog.Builder(ActivityMenuDetail.this)
                .setView(layout)
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbhelper.openDataBase();
                        ItemPedido item = new ItemPedido(idProduto, picker.getValue());
                        dbhelper.adicionaItemPedido(item);
                        dbhelper.close();
                        Toast.makeText(ActivityMenuDetail.this, "O item (" + txtText.getText().toString() + ") foi adicionado ao seu pedido.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }



	@Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	dbhelper.close();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
	

    @Override
    protected void onDestroy() {
    	// TODO Auto-generated method stub
    	//imageLoader.clearCache();
    	super.onDestroy();
    }
	 
    
    @Override
	public void onConfigurationChanged(final Configuration newConfig)
	{
	    // Ignore orientation change to keep activity from restarting
	    super.onConfigurationChanged(newConfig);
	}


	public void buscaProdutoWS(){

		if(!prgLoading.isShown()){
			prgLoading.setVisibility(0);
			txtAlert.setVisibility(8);
		}

		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.DETALHE_PRODUTO + idProduto, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {

				Gson gson = new GsonBuilder().create();
				Produto produto = gson.fromJson(resposta.toString(), Produto.class);
				produtoSelecionado = produto;
				atualizaTela();

//				Menu_image = menu.getString("Menu_image");
//				Menu_name = menu.getString("Menu_name");
//				Menu_price = Double.valueOf(formatData.format(menu.getDouble("Price")));
//				Menu_serve = menu.getString("Serve_for");
//				Menu_description = menu.getString("Description");
//				Menu_quantity = menu.getInt("Quantity");

			}
		});
	}

	public void atualizaTela(){
		prgLoading.setVisibility(8);

		if((produtoSelecionado != null) && IOConnect == 0){
			sclDetail.setVisibility(0);

			//imageLoader.DisplayImage(Constant.URL_BASE_FOTO_CATEGORIA + produtoSelecionado.getFoto(), imgPreview);

			Picasso.with(ActivityMenuDetail.this).load(Constant.URL_BASE_FOTO_CATEGORIA + produtoSelecionado.getFoto()).into(imgPreview);
			// .placeholder(R.drawable.carregando_imagem)
			txtText.setText(produtoSelecionado.getNome());
			txtPrecoUnitario.setText(Utilitarios.formatarValor(produtoSelecionado.getValorUnitario(), true));
			txtSubText.setText(produtoSelecionado.getDescricao() + "\n\nTempo de Preparo: " + produtoSelecionado.getTempoPreparo() + "min. \nDisponível: " + (produtoSelecionado.isDisponivel() ? "Sim" : "Não"));
		}else{
			txtAlert.setVisibility(0);
		}

		invalidateOptionsMenu();

//		RequestParams params = new RequestParams();
//		params.put("produtoid", produtoSelecionado.getId());
//		params.put("usuarioid", Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
//		verificaProdutoFavorito(params);
	}

	private void adicionarFavoritoWS(final RequestParams params, final MenuItem item){
		carregando.show();
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(Constant.ADICIONAR_FAVORITO, params, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
				RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
				Toast.makeText(ActivityMenuDetail.this, retorno.getMensagem(), Toast.LENGTH_SHORT).show();
				carregando.dismiss();
				item.setVisible(false);
			}

		});
	}



	private void verificaProdutoFavorito(RequestParams params){
		AsyncHttpClient client = new AsyncHttpClient();
		client.post(Constant.VERIFICA_FAVORITO, params, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
				RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
                itemMenu.setVisible(retorno.getStatus().equals("erro"));
			}

		});
	}

}
