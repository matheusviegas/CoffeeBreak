package com.msouza.coffeebreak;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Credito;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.model.Produto;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

@SuppressLint("NewApi")
public class MainActivity extends FragmentActivity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private AdapterNavDrawerList adapter;

	// declare dbhelper and adapter object
	static DBHelper dbhelper;
	AdapterMainMenu mma;

	String nome, credito, foto;
	Long id, idcontasocial,idCredito;

	List<Produto> produtosPromocao;

	@Override
	@SuppressWarnings("ResourceType")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.nav_drawer_main);
		produtosPromocao = new ArrayList<>();


		// SALVA INFORMACOES SESSAO
		Sessao sessao = new Sessao(MainActivity.this);
		HashMap<String, String> user = sessao.getDetalhesUsuario();
		id = Long.valueOf(user.get(Sessao.KEY_IDUSUARIO));
		nome = user.get(Sessao.KEY_NOME);
		credito = user.get(Sessao.KEY_EMAIL);
		foto = user.get(Sessao.KEY_FOTO);
		idcontasocial = Long.valueOf(user.get(Sessao.KEY_ID_CONTA_SOCIAL));


		// INICIA SERVICO PUSH
		System.out.println("MainActivity.onCreate: " + FirebaseInstanceId.getInstance().getToken());
		atualizaToken(FirebaseInstanceId.getInstance().getToken(), id);




		mTitle = mDrawerTitle = getTitle();

		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// nav drawer icons from resources
		navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		mDrawerLayout.setDrawerShadow(R.drawable.navigation_drawer_shadow, GravityCompat.START);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons.getResourceId(8, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons.getResourceId(9, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons.getResourceId(10, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[11], navMenuIcons.getResourceId(11, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[12], navMenuIcons.getResourceId(12, -1)));

		// Recycle the typed array
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new AdapterNavDrawerList(getApplicationContext(), navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		ActionBar bar = getActionBar();
		bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));

		// get screen device width and height
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);

		// checking internet connection
		if (!Constant.isNetworkAvailable(MainActivity.this)) {
			Toast.makeText(MainActivity.this, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
		}

		mma = new AdapterMainMenu(this);
		dbhelper = new DBHelper(this);

		// then, the database will be open to use
		try {
			dbhelper.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}

		// if user has already ordered food previously then show confirm dialog
		if (dbhelper.isPreviousDataExist()) {
			showAlertDialog();
			dbhelper.close();
		}

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, // nav
																								// menu
																								// toggle
																								// icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}


		buscaCredito(Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));

		Intent parametros = getIntent();
        if(parametros.hasExtra("atualizaMenu")){
			System.out.println("TEM EXTRA!");
			invalidateOptionsMenu();
        }
	}

	// show confirm dialog to ask user to delete previous order or not
	void showAlertDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.confirm);
		builder.setMessage(getString(R.string.db_exist_alert));
		builder.setCancelable(false);
		builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// delete order data when yes button clicked
				dbhelper.openDataBase();
				dbhelper.limpaPedido();
				dbhelper.close();

			}
		});

		builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// close dialog when no button clicked
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();

	}

	private Boolean sair = false;
	@Override
	public void onBackPressed() {
		if (sair) {
			finish();
			overridePendingTransition(R.anim.open_main, R.anim.close_next);
		} else {
			Toast.makeText(this, "Pression o botão 'voltar' novamente para fechar o aplicativo.",
					Toast.LENGTH_SHORT).show();
			sair = true;
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					sair = false;
				}
			}, 3000);
		}
	}

	/**
	 * Slide menu item click listener
	 */
	private class SlideMenuClickListener implements ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		switch (item.getItemId()) {
			case R.id.pedir:
				/*Sessao sessao = new Sessao(MainActivity.this);
				sessao.setVisualizouNotificacao(true);
				invalidateOptionsMenu();

				Toast.makeText(getApplicationContext(), "Visualizei", Toast.LENGTH_LONG).show();*/

				startActivity(new Intent(MainActivity.this, ActivityCategoryList.class));
				overridePendingTransition(R.anim.open_next, R.anim.close_next);

				//startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.more_apps))));
			return true;
			case R.id.atualizar:
				displayView(0);
				return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/*
	 * * Called when invalidateOptionsMenu() is triggered
	 */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		//Sessao sessao = new Sessao(MainActivity.this);

		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		MenuItem item = menu.findItem(R.id.pedir);
		MenuItem item2 = menu.findItem(R.id.atualizar);


		//if(drawerOpen || !sessao.temPedidoEmAberto()){

		item.setVisible(!drawerOpen);
		item2.setVisible(!drawerOpen);


//		int idIcone = sessao.visualizouNotificacao() ? R.drawable.ic_notifications_white_48dp : R.drawable.ic_notifications_active_white_48dp;
//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//			item.setIcon(getResources().getDrawable(idIcone, getApplicationContext().getTheme()));
//		} else {
//			item.setIcon(getResources().getDrawable(idIcone));
//		}

		return super.onPrepareOptionsMenu(menu);
	}



	private void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new PainelActivity();
			break;
		case 1:
			startActivity(new Intent(getApplicationContext(), ActivityCategoryList.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 2:
			startActivity(new Intent(getApplicationContext(), ActivityCart.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 3:
			startActivity(new Intent(getApplicationContext(), ActivityCheckout.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 4:
			startActivity(new Intent(getApplicationContext(), PerfilActivity.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
			case 5:
				Intent intent = new Intent(getApplicationContext(), MovimentacoesCreditoActivity.class);
				intent.putExtra("idcredito", idCredito);
				startActivity(intent);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
				break;
			case 6:
				startActivity(new Intent(getApplicationContext(), ListaPedidosActivity.class));
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
				break;
			case 7:
				startActivity(new Intent(getApplicationContext(), FavoritosActivity.class));
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
				break;
		case 8:
			startActivity(new Intent(getApplicationContext(), ConfiguracoesActivity.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
		case 9:
			startActivity(new Intent(getApplicationContext(), ActivityAbout.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
			break;
			case 10:
				startActivity(new Intent(getApplicationContext(), SugestaoActivity.class));
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
				break;
		case 11:
			Intent sendInt = new Intent(Intent.ACTION_SEND);
			sendInt.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
			sendInt.putExtra(Intent.EXTRA_TEXT, "Coffee Break\nAplicativo para pedidos da cantina da Faculdade Senac\n\n"
					+ "É grátis! Baixe agora em: http://app.msouzadev.com\n\nJunte-se ao lado cafeinado da força!");
			sendInt.setType("text/plain");
			startActivity(Intent.createChooser(sendInt, "Compartilhar"));
			break;
		case 12:
			dbhelper.openDataBase();
			dbhelper.limpaPedido();
			dbhelper.limpaNotificacoes();
			dbhelper.close();
			Sessao sessao = new Sessao(getApplicationContext());
			sessao.terminarSessao();
			MainActivity.this.finish();
			overridePendingTransition(R.anim.open_next, R.anim.close_next);

			break;

		default:
			break;
		}

		if (fragment != null) {
			android.app.FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction().replace(R.id.frame_container, fragment).commit();

			mDrawerList.setItemChecked(position, true);
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			Log.e("MainActivity", "erro ao criar fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	public void atualizaToken(String token, Long idUsuario){
		RequestParams parametros = new RequestParams();
		parametros.put("id", idUsuario);
		parametros.put("token", token);

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(Constant.ATUALIZA_TOKEN, parametros, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
				Gson gson = new GsonBuilder().create();
				RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
				System.out.println(retorno.toString());
			}

		});
	}

	private void buscaCredito(Long idUsuario){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.CONSULTA_SALDO_CREDITO + idUsuario, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {
				Gson gson = new GsonBuilder().create();
				Credito credito = gson.fromJson(resposta.toString(), Credito.class);
				idCredito = credito.getId();
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
		invalidateOptionsMenu();
	}
}
