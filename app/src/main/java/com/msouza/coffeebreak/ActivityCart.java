package com.msouza.coffeebreak;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.msouza.coffeebreak.entidades.ItemPedidoDecorator;

public class ActivityCart extends Activity {

	ListView listOrder;
	ProgressBar prgLoading;
	TextView txtTotalLabel, txtTotal, txtAlert;
	Button btnClear, Checkout;
	RelativeLayout lytOrder;
	
	DBHelper dbhelper;
	AdapterCart mola;
	

	static List<ItemPedidoDecorator> itensPedido = new ArrayList<>();

	final int CLEAR_ALL_ORDER = 0;
	final int CLEAR_ONE_ORDER = 1;
	int FLAG;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_order);
        
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Detalhes do Pedido");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        Checkout = (Button) findViewById(R.id.Checkout);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        listOrder = (ListView) findViewById(R.id.listOrder);
        txtTotalLabel = (TextView) findViewById(R.id.txtTotalLabel);
        txtTotal = (TextView) findViewById(R.id.txtTotal);
        txtAlert = (TextView) findViewById(R.id.txtAlert);
        btnClear = (Button) findViewById(R.id.btnClear);
        lytOrder = (RelativeLayout) findViewById(R.id.lytOrder);

        mola = new AdapterCart(this);
        dbhelper = new DBHelper(this);
        
        try{
			dbhelper.openDataBase();
            clearData();
			buscaItensPedidoDecoratorSQLITE();
			dbhelper.close();
		}catch(SQLException sqle){
			throw sqle;
		}


        // event listener to handle clear button when clicked
		btnClear.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				// show confirmation dialog
				showClearDialog(CLEAR_ALL_ORDER, 1111l);
			}
		});
		
        // event listener to handle list when clicked
		listOrder.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				showClearDialog(CLEAR_ONE_ORDER, itensPedido.get(position).getId());
			}
		});
        

        Checkout.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {
				Intent iReservation = new Intent(ActivityCart.this, ActivityCheckout.class);
				startActivity(iReservation);
				overridePendingTransition(R.anim.open_next, R.anim.close_next);
			}
		});
      
    }
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.icone_home, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {

			// se clicar no icone de voltar (android default) DEMOREI PRA KRL PRA DESCOBRIR ISSOOOOOOOOOOO
			case android.R.id.home:
				this.finish();
				overridePendingTransition(R.anim.open_main, R.anim.close_next);
				return true;
			
		case R.id.inicio:
        	//this.finish();
        	//overridePendingTransition(R.anim.open_main, R.anim.close_next);
			voltarInicio();
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}


	public void voltarInicio(){
		super.onBackPressed();
		//dbhelper.limpaPedido();
		//dbhelper.close();
		this.finish();

		Intent intent = new Intent(ActivityCart.this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}


    // method to create dialog
    void showClearDialog(int flag, final Long id){
    	FLAG = flag;
		AlertDialog.Builder builder = 	new AlertDialog.Builder(this);
		builder.setTitle(R.string.confirm);
		switch(FLAG){
		case 0:
			builder.setMessage(getString(R.string.clear_all_order));
			break;
		case 1:
			builder.setMessage(getString(R.string.clear_one_order));
			break;
		}
		builder.setCancelable(false);
		builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				dbhelper.openDataBase();
				switch(FLAG){
					case 0:
						// clear all menu in order table
						dbhelper.limpaPedido();
						listOrder.invalidateViews();
						clearData();
						break;
					case 1:
						dbhelper.removeItemPedido(id);
						listOrder.invalidateViews();
						clearData();
						break;
				}

				// Busca os Itens do Pedido Novamente pra atualizar a tela
				buscaItensPedidoDecoratorSQLITE();
				dbhelper.close();
			}
		});
		
		builder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				// close dialog
				dialog.cancel();
			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	
    }

    private void clearData(){
		itensPedido.clear();
	}


	private void buscaItensPedidoDecoratorSQLITE(){
		if(!prgLoading.isShown()){
			prgLoading.setVisibility(0);
			lytOrder.setVisibility(8);
			txtAlert.setVisibility(8);
		}

		//dbhelper.openDataBase();
		List<ItemPedidoDecorator> itens = dbhelper.buscaItensDecorator();
		Double valorTotalPedido = 0d;

		for(ItemPedidoDecorator item : itens){
			itensPedido.add(item);
			valorTotalPedido += (item.getQuantidade() * item.getValorUnitario());
		}
		//dbhelper.close();

		txtTotal.setText(Utilitarios.formatarValor(valorTotalPedido, true));
		// Total onde tinha IMPOSTOS
		txtTotalLabel.setText(getString(R.string.total_order));
		prgLoading.setVisibility(8);

		System.out.println("QUANBTIDADE ITENS NO PEDIDO: " + itens.size());

		if(itens.size() > 0){
			lytOrder.setVisibility(0);
			listOrder.setAdapter(mola);
		}else{
			txtAlert.setVisibility(0);
		}
	}




    
    @Override
    public void onBackPressed() {
    	super.onBackPressed();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
    
	 
    @Override
	public void onConfigurationChanged(final Configuration newConfig){
	    super.onConfigurationChanged(newConfig);
	}
}
