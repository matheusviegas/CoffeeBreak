package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoAtualizaFoto;
import com.msouza.coffeebreak.entidades.model.Configuracoes;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ConfiguracoesActivity extends Activity implements View.OnClickListener{

    enum TipoCancelamentoConta{
        DESATIVAR("desativar"), EXCLUIR_PERMANENTEMENTE("excluir");

        private final String descricao;

        TipoCancelamentoConta(String desc){
            this.descricao = desc;
        }

        @Override
        public String toString() {
            return this.descricao;
        }
    }

    private ToggleButton btnToggleNotificacoes;
    private ToggleButton btnTogglePromocoes;
    private Button btnSalvar, btnCancelarConta;
    private Sessao sessao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracoes);


        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Configurações");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        btnToggleNotificacoes = (ToggleButton) findViewById(R.id.btnToggleNotificacoes);
        btnTogglePromocoes = (ToggleButton) findViewById(R.id.btnTogglePromocoes);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        btnCancelarConta = (Button) findViewById(R.id.btnCancelarConta);

        btnSalvar.setOnClickListener(this);
        btnCancelarConta.setOnClickListener(this);

        sessao = new Sessao(ConfiguracoesActivity.this);
        Configuracoes config = sessao.getConfiguracoes();

        btnToggleNotificacoes.setChecked(config.isNotificacoes());
        btnTogglePromocoes.setChecked(config.isPromocoes());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnCancelarConta:
                abreJanelaCancelarConta();
                break;
            case R.id.btnSalvar:
                Configuracoes configuracoes = new Configuracoes(btnToggleNotificacoes.isChecked(), btnTogglePromocoes.isChecked());
                sessao.setConfiguracoes(configuracoes);
                Toast.makeText(ConfiguracoesActivity.this, "As alterações foram salvas", Toast.LENGTH_LONG).show();
                break;
        }
    }

    private void abreJanelaCancelarConta(){
        LayoutInflater factory = LayoutInflater.from(ConfiguracoesActivity.this);
        final View view = factory.inflate(R.layout.janela_cancelar_conta, null);
        final AlertDialog janelaCancelarConta = new AlertDialog.Builder(ConfiguracoesActivity.this).create();
        janelaCancelarConta.setCanceledOnTouchOutside(false);
        janelaCancelarConta.setView(view);

        final EditText txtSenha = (EditText) view.findViewById(R.id.txtSenha);
        final RadioGroup grupo = (RadioGroup) view.findViewById(R.id.radioGroup);

        view.findViewById(R.id.btnConfirmaCancelamento).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!txtSenha.getText().toString().trim().equals("")){
                    janelaCancelarConta.dismiss();
                    cancelarContaWS(txtSenha.getText().toString().trim(), grupo.getCheckedRadioButtonId() == R.id.rdDesativar ? TipoCancelamentoConta.DESATIVAR : TipoCancelamentoConta.EXCLUIR_PERMANENTEMENTE);
                }else{
                    Toast.makeText(ConfiguracoesActivity.this, "Informe a senha da conta", Toast.LENGTH_SHORT).show();
                }
            }
        });

        view.findViewById(R.id.btnVoltar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                janelaCancelarConta.dismiss();
            }
        });

        janelaCancelarConta.show();
    }

    private void cancelarContaWS(String senha, TipoCancelamentoConta tipo){
        final ProgressDialog carregando = new ProgressDialog(this);
        carregando.setMessage("Processando..");
        carregando.setCanceledOnTouchOutside(false);
        carregando.show();

        RequestParams params = new RequestParams();
        params.put("usuarioid", Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
        params.put("tipo", tipo.toString());
        params.put("senha", senha);


        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.CANCELAR_CONTA, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoAtualizaFoto retorno = gson.fromJson(respostaJson.toString(), RetornoAtualizaFoto.class);
                carregando.dismiss();
                Toast.makeText(ConfiguracoesActivity.this, retorno.getMensagem(), Toast.LENGTH_LONG).show();

                if(retorno.getStatus().equals("ok")){
                    DBHelper dbHelper = new DBHelper(ConfiguracoesActivity.this);
                    dbHelper.openDataBase();
                    dbHelper.limpaPedido();
                    dbHelper.limpaNotificacoes();
                    dbHelper.close();
                    sessao.terminarSessao();
                    finish();
                    Intent launchNextActivity = new Intent(ConfiguracoesActivity.this, ActivityLogin.class);
                    launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(launchNextActivity);
                    overridePendingTransition(R.anim.open_next, R.anim.close_next);
                }
            }

        });

    }
}
