package com.msouza.coffeebreak;

import java.util.HashMap;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.msouza.coffeebreak.entidades.model.Configuracoes;

public class Sessao {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "CoffeeBreakPreferences";

    private static final String IS_LOGADO = "IsLogado";
    private static final String PEDIDO_EM_ABERTO = "pedidoAberto";
    public static final String KEY_IDUSUARIO = "idUsuario";
    public static final String KEY_NOME = "nome";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_FOTO = "foto";
    public static final String KEY_ID_CONTA_SOCIAL = "idcontasocial";
    public static final String VISUALIZOU_NOTIFICACAO = "visualizou";

    public static final String KEY_CPF = "cpf";
    public static final String KEY_TELEFONE = "telefone";

    public static final String KEY_NOTIFICACOES = "notificacoes";
    public static final String KEY_PROMOCOES = "promocoes";

    public static final String KEY_VERSAO_BANCO = "versao";
    public static final String KEY_DESCONTO = "desconto";

    public Sessao(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void iniciarSessao(String nome, String email, String foto, Long id, String cpf, String telefone){
        editor.putBoolean(IS_LOGADO, true);
        editor.putBoolean(VISUALIZOU_NOTIFICACAO, true);
        editor.putString(KEY_NOME, nome);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_FOTO, foto);
        editor.putLong(KEY_IDUSUARIO, id);
        editor.putString(KEY_CPF, cpf);
        editor.putString(KEY_TELEFONE, telefone);
        editor.putLong(KEY_ID_CONTA_SOCIAL, 1l);
        editor.putBoolean(PEDIDO_EM_ABERTO, false);
        editor.commit();
    }

    public void iniciarSessao(String nome, String email, String foto, Long id, Long idContaSocial, String cpf, String telefone){
        editor.putBoolean(IS_LOGADO, true);
        editor.putBoolean(VISUALIZOU_NOTIFICACAO, true);
        editor.putString(KEY_NOME, nome);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_FOTO, foto);
        editor.putLong(KEY_IDUSUARIO, id);
        editor.putLong(KEY_ID_CONTA_SOCIAL, idContaSocial);
        editor.putBoolean(PEDIDO_EM_ABERTO, false);
        editor.putString(KEY_CPF, cpf);
        editor.putString(KEY_TELEFONE, telefone);
        editor.commit();
    }

    public void atualizaFotoSessao(String urlFoto){
        editor.remove(KEY_FOTO);
        editor.putString(KEY_FOTO, urlFoto);
        editor.commit();
    }

    public Configuracoes getConfiguracoes(){
        return new Configuracoes(pref.getBoolean(KEY_NOTIFICACOES, false), pref.getBoolean(KEY_PROMOCOES, false));
    }

    public Integer getVersaoBanco(){
        return pref.getInt(KEY_VERSAO_BANCO, 1);
    }

    public void setVersaoBanco(Integer versao){
        editor.remove(KEY_VERSAO_BANCO);

        editor.putInt(KEY_VERSAO_BANCO, versao);
        editor.commit();
    }

    public Integer getDesconto(){
        return pref.getInt(KEY_DESCONTO, 0);
    }

    public void setDesconto(Integer desconto){
        editor.remove(KEY_DESCONTO);

        editor.putInt(KEY_DESCONTO, desconto);
        editor.commit();
    }


    public void setConfiguracoes(Configuracoes configuracoes){
        editor.remove(KEY_NOTIFICACOES);
        editor.remove(KEY_PROMOCOES);

        editor.putBoolean(KEY_NOTIFICACOES, configuracoes.isNotificacoes());
        editor.putBoolean(KEY_PROMOCOES, configuracoes.isPromocoes());
        editor.commit();
    }

    public void setPedidoAberto(boolean pedidoAberto){
        editor.remove(PEDIDO_EM_ABERTO);
        editor.putBoolean(PEDIDO_EM_ABERTO, pedidoAberto);
        editor.commit();
    }


    public boolean temPedidoEmAberto(){
        return pref.getBoolean(PEDIDO_EM_ABERTO, false);
    }

    public void setVisualizouNotificacao(boolean visualizou){
        editor.remove(VISUALIZOU_NOTIFICACAO);
        editor.putBoolean(VISUALIZOU_NOTIFICACAO, visualizou);
        editor.commit();
    }

    public boolean visualizouNotificacao(){
        return pref.getBoolean(VISUALIZOU_NOTIFICACAO, false);
    }


    public void verificaLogin(){
        if(!this.isLogado()){
            Intent i = new Intent(_context, ActivityLogin.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);
        }

    }


    public HashMap<String, String> getDetalhesUsuario(){
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_NOME, pref.getString(KEY_NOME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_FOTO, pref.getString(KEY_FOTO, null));
        user.put(KEY_IDUSUARIO, pref.getLong(KEY_IDUSUARIO, 1l) + "");
        user.put(KEY_ID_CONTA_SOCIAL, pref.getLong(KEY_ID_CONTA_SOCIAL, 1l) + "");
        user.put(KEY_CPF, pref.getString(KEY_CPF, null));
        user.put(KEY_TELEFONE, pref.getString(KEY_TELEFONE, null));
        return user;
    }


    public void terminarSessao(){
        Configuracoes config = this.getConfiguracoes();
        Integer versao = this.getVersaoBanco();
        Integer desconto = this.getDesconto();
        editor.clear();
        editor.commit();
        this.setConfiguracoes(config);
        this.setVersaoBanco(versao);
        this.setDesconto(desconto);

        Intent i = new Intent(_context, ActivityLogin.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
    }

    public void limparCampos(){
        Configuracoes config = this.getConfiguracoes();
        Integer versao = this.getVersaoBanco();
        Integer desconto = this.getDesconto();
        editor.clear();
        editor.commit();
        this.setConfiguracoes(config);
        this.setVersaoBanco(versao);
        this.setDesconto(desconto);
    }

    public boolean perfilCompleto(){
        return !pref.getString(KEY_CPF, "").equals("") && !pref.getString(KEY_TELEFONE, "").equals("");
    }

    public boolean isLogado(){
        return pref.getBoolean(IS_LOGADO, false);
    }
}