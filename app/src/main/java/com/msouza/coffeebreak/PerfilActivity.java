package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.AdapterDataSemHora;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoAtualizaFoto;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.Usuario;
import com.msouza.coffeebreak.entidades.model.Configuracoes;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import de.hdodenhof.circleimageview.CircleImageView;

public class PerfilActivity extends Activity {

    private EditText nome, email, senha1, senha2, senhaAtual, cpf, telefone;

    private Button btnSalvar;
    private CircleImageView imagemPerfil;
    private boolean alteraSenha = false;
    Long codUsuario = 0l;

    ProgressDialog progressDialog, janelaAlterandoFoto;

    // UPLOAD
    private Bitmap bitmap;
    private Uri filePath;
    private int PICK_IMAGE_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Perfil");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        nome = (EditText) findViewById( R.id.txtNome);
        email = (EditText) findViewById( R.id.txtEmail);
        senha1 = (EditText) findViewById( R.id.txtSenha);
        senha2 = (EditText) findViewById( R.id.txtSenha2);
        senhaAtual = (EditText) findViewById(R.id.txtSenhaAtual);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);
        imagemPerfil = (CircleImageView) findViewById(R.id.imagemPerfil);
        telefone = (EditText) findViewById(R.id.txtTelefone);
        cpf = (EditText) findViewById(R.id.txtCPF);

        telefone.addTextChangedListener(Mask.insert("(##) #####-####", telefone));
        cpf.addTextChangedListener(Mask.insert("###.###.###-##", cpf));

        Sessao sessao = new Sessao(this);
        HashMap<String, String> user = sessao.getDetalhesUsuario();
        nome.setText(user.get(Sessao.KEY_NOME));
        email.setText(user.get(Sessao.KEY_EMAIL));
        cpf.setText(user.get(Sessao.KEY_CPF));
        telefone.setText(user.get(Sessao.KEY_TELEFONE));

        System.out.println("CPF PERFIL:> " + user.get(Sessao.KEY_CPF));

        codUsuario = Long.valueOf(user.get(Sessao.KEY_IDUSUARIO));

        // Carrega Imagem com o Picasso e salva em cache
        Picasso.with(this).load(Utilitarios.getURLFotoPerfil(user.get(Sessao.KEY_FOTO)))
                .placeholder(R.drawable.ic_profile)
                .error(R.drawable.ic_profile)
                .into(imagemPerfil);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Salvando alterações..");
        progressDialog.setCanceledOnTouchOutside(false);

        janelaAlterandoFoto = new ProgressDialog(this);
        janelaAlterandoFoto.setMessage("Alterando foto de perfil..");
        janelaAlterandoFoto.setCanceledOnTouchOutside(false);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos()){
                    progressDialog.show();
                    RequestParams parametros = new RequestParams();
                    parametros.put("id", codUsuario);
                    parametros.put("nome", nome.getText().toString());
                    parametros.put("email", email.getText().toString());
                    parametros.put("cpf", cpf.getText().toString());
                    parametros.put("telefone", telefone.getText().toString());
                    if(alteraSenha) {
                        parametros.put("senha", senha1.getText().toString().trim());
                        parametros.put("senhaatual", senhaAtual.getText().toString().trim());
                    }
                    invokeWS(parametros);
                }
            }
        });

        imagemPerfil.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                abrirJanelaEscolherFoto();
            }
        });
    }


    private boolean validarCampos(){

        if(isNull(nome)){
            exibeAlerta("Informe o nome.");
            return false;
        }

        if(isNull(email)){
            exibeAlerta("Informe o email.");
            return false;
        }

        if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()){
            exibeAlerta("Informe um endereço de email válido.");
            return false;
        }

        if(!isNull(senhaAtual) && !isNull(senha1) && !isNull(senha2)){
            if(!senha1.getText().toString().trim().equals(senha2.getText().toString().trim())){
                exibeAlerta("As senhas não conferem.");
                return false;
            }
            alteraSenha = true;
        }else{
            alteraSenha = false;
        }

        return true;
    }

    private void exibeAlerta(String mensagem){
        Toast.makeText(this,mensagem,Toast.LENGTH_LONG).show();
    }

    private boolean isNull(EditText edit){
        return edit.getText().toString().trim() == null || edit.getText().toString().trim().equals("");
    }

    private void invokeWS(RequestParams params){
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.SALVA_ALTERACOES_PERFIL, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new AdapterDataSemHora()).create();
                Usuario usuario = gson.fromJson(respostaJson.toString(), Usuario.class);

                Sessao sessao = new Sessao(PerfilActivity.this);
                sessao.limparCampos();
                sessao.iniciarSessao(usuario.getNome(), usuario.getEmail(), usuario.getFoto(), usuario.getId(), usuario.getCpf(), usuario.getTelefone());
                progressDialog.dismiss();
                senha1.setText("");
                senha2.setText("");
                senhaAtual.setText("");

                exibeAlerta("As alterações foram salvas!");
            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }



    private void atualizaFotoPerfilWS(){
        janelaAlterandoFoto.show();
        final Sessao sessao = new Sessao(PerfilActivity.this);

        RequestParams params = new RequestParams();
        params.put("foto", getFotoBase64());
        params.put("usuarioid", Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.ALTERAR_FOTO_PERFIL, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new AdapterDataSemHora()).create();
                RetornoAtualizaFoto retorno = gson.fromJson(respostaJson.toString(), RetornoAtualizaFoto.class);
                janelaAlterandoFoto.dismiss();
                Toast.makeText(PerfilActivity.this, retorno.getMensagem(), Toast.LENGTH_LONG).show();

                System.out.println("RETORNO NOVA FOTO: " + retorno.getUrlNovaFoto());

                if(retorno.getStatus().equals("ok")){
                    sessao.atualizaFotoSessao(retorno.getUrlNovaFoto());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                throwable.printStackTrace();
                System.out.println("ERRO ALTERAR FOTO: " + throwable.getMessage());
                System.out.println("RESPOSTA");
                System.out.println(responseString);
            }

            //    onFailure(int, Header[], String, Throwable) was not overriden, but callback was received

        });
    }



    // Métodos Alterar FOTO
    private void abrirJanelaEscolherFoto() {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Selecione uma foto"), PICK_IMAGE_REQUEST);
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {

                filePath = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                    imagemPerfil.setImageBitmap(bitmap);
                    atualizaFotoPerfilWS();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public String getFotoBase64(){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            return encodedImage;
        }


    // FIM Métodos Alterar FOTO
}
