package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;


public class ActivityCadastro extends Activity {

    Button botaoCadastrar;
    EditText txtEmail,txtSenha, txtSenha2, txtNome;
    ProgressDialog progressBar;


    ProgressBar prgLoading;
    TextView txtAlert, subtitulo;

    String fotofb = "";
    Long idfb = 0l;
    boolean completaFB = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        getActionBar().hide();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);
        bar.setTitle("Cadastro");

        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtAlert = (TextView) findViewById(R.id.txtAlert);



        botaoCadastrar = (Button) findViewById(R.id.botaoCadastrar);
        txtNome = (EditText) findViewById(R.id.txtNome);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtSenha = (EditText) findViewById(R.id.txtSenha);
        txtSenha2 = (EditText) findViewById(R.id.txtSenha2);
        subtitulo = (TextView) findViewById(R.id.subtitulo);

        // Clique no botão cadastrar
        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos()){
                    RequestParams parametros = new RequestParams();
                    parametros.put("email", txtEmail.getText().toString().trim());
                    parametros.put("senha", txtSenha.getText().toString().trim());
                    parametros.put("nome", txtNome.getText().toString().trim());
                    if(completaFB){
                        parametros.put("idcontasocial", idfb);
                        parametros.put("foto", fotofb);
                    }
                    cadastroWS(parametros);
                }
            }
        });


        progressBar = new ProgressDialog(botaoCadastrar.getContext());
        progressBar.setCancelable(false);
        progressBar.setMessage("Cadastrando...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);



        // Verifica se é pra completar cadastro do facebook
        Intent param = getIntent();
        if(param.getBooleanExtra("cadastrofb", false)){
            preencheCampos(param);
        }
    }

    private void preencheCampos(Intent intent){
        txtNome.setText(intent.getStringExtra("nomecompleto"));
        txtEmail.setText(intent.getStringExtra("email"));
        fotofb = intent.getStringExtra("foto");
        idfb = intent.getLongExtra("idcontasocial", 0l);
        completaFB = true;
        subtitulo.setText("Complete seu cadastro");
    }

    private boolean validarCampos(){

        if(txtNome.getText().toString().trim() == null || txtSenha.getText().toString().trim().equals("")){
            exibeAlerta("Informe o nome");
            return false;
        }

        if(txtEmail.getText().toString().trim() == null || txtEmail.getText().toString().trim().equals("")){
            exibeAlerta("Preencha o email");
            return false;
        }

        if(!this.validaEmail(txtEmail.getText().toString().trim())){
            exibeAlerta("Email inválido");
            return false;
        }

        if(txtSenha.getText().toString().trim() == null || txtSenha.getText().toString().trim().equals("")){
            exibeAlerta("Preencha a senha");
            return false;
        }

        if(txtSenha2.getText().toString().trim() == null || txtSenha2.getText().toString().trim().equals("")){
            exibeAlerta("Preencha a confirmação de senha");
            return false;
        }

        if(!txtSenha2.getText().toString().trim().equals(txtSenha.getText().toString().trim())){
            exibeAlerta("As senhas não conferem");
            return false;
        }

        return true;
    }


    private void exibeAlerta(String mensagem){
        Toast.makeText(getApplicationContext(),mensagem,Toast.LENGTH_LONG).show();
    }

    private boolean validaEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }



     private void cadastroWS(RequestParams params){
        progressBar.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.CADASTRO_API,params ,new JsonHttpResponseHandler() {


            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                progressBar.hide();

                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
                exibeAlerta(retorno.getMensagem());

                if(retorno.getStatus().equals("ok")){
                    Intent intent = new Intent(ActivityCadastro.this, ActivityLogin.class);
                    startActivity(intent);
                }

            }

        });
    }

}
