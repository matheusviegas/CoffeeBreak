package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class AvisoDetalhadoActivity extends Activity {

    TextView txtTitulo, txtMensagem;
    ImageView foto;
    Button fechar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aviso_detalhado);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Detalhes do Aviso");
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);

        foto = (ImageView) findViewById(R.id.foto);
        txtTitulo = (TextView) findViewById(R.id.titulo);
        txtMensagem = (TextView) findViewById(R.id.mensagem);
        fechar = (Button) findViewById(R.id.btnFechar);

        Intent intent = getIntent();
        txtTitulo.setText(intent.getStringExtra("titulo"));
        txtMensagem.setText(intent.getStringExtra("mensagem"));
        if(intent.hasExtra("foto") && !intent.getStringExtra("foto").equals("sem")) {
            Picasso.with(this).load(Constant.URL_BASE_FOTO_CATEGORIA + Utilitarios.getURLFoto(intent.getStringExtra("foto"))).into(foto);
        }else{
            foto.setVisibility(View.GONE);
        }

        fechar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.btnFechar){
                    finish();
                }
            }
        });
    }
}
