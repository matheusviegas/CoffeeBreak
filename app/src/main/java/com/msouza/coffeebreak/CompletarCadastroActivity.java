package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;


public class CompletarCadastroActivity extends Activity {

    Button botaoCadastrar;
    EditText txtEmail,txtSenhaCredito;
    ProgressDialog progressBar;
    TextView txtApresentacao;
    Long idusuario;
    String nome, email;


    ProgressBar prgLoading;
    TextView txtAlert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completar_cadastro);

        getActionBar().hide();

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setDisplayHomeAsUpEnabled(false);
        bar.setHomeButtonEnabled(false);
        bar.setTitle("Completar Cadastro");

        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtAlert = (TextView) findViewById(R.id.txtAlert);


        botaoCadastrar = (Button) findViewById(R.id.botaoCadastrar);
        txtSenhaCredito = (EditText) findViewById(R.id.txtSenhaCredito);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtApresentacao = (TextView) findViewById(R.id.txtApresentacao);

        Intent intent = getIntent();
        idusuario = intent.getLongExtra("id", 0l);
        nome = intent.getStringExtra("nome");
        email = intent.getStringExtra("email");


        txtApresentacao.setText("Olá " + nome + "! Para começar a usar nossos serviços, é necessário definir uma senha de crédito e confirmar seu endereço de email.");
        txtEmail.setText(email);

        // Clique no botão cadastrar
        botaoCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validarCampos()){
                    RequestParams parametros = new RequestParams();
                    parametros.put("id", idusuario);
                    parametros.put("email", txtEmail.getText().toString().trim());
                    parametros.put("senhacredito", txtSenhaCredito.getText().toString().trim());
                    invokeWS(parametros);

                    Intent intent = new Intent(CompletarCadastroActivity.this, ActivityLogin.class);
                    startActivity(intent);
                }
            }
        });


        progressBar = new ProgressDialog(botaoCadastrar.getContext());
        progressBar.setCancelable(false);
        progressBar.setMessage("Cadastrando...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private boolean validarCampos(){

        if(txtEmail.getText().toString().trim() == null || txtEmail.getText().toString().trim().equals("")){
            exibeAlerta("Preencha o email");
            return false;
        }

        if(!this.validaEmail(txtEmail.getText().toString().trim())){
            exibeAlerta("Email inválido");
            return false;
        }


        if(txtSenhaCredito.getText().toString().trim() == null || txtSenhaCredito.getText().toString().trim().equals("")){
            exibeAlerta("Preencha a senha de crédito");
            return false;
        }else{
            if(txtSenhaCredito.getText().length() != 4){
                exibeAlerta("A senha de ccrédito deve conter exatamente 4 caracteres");
                return false;
            }
        }

        return true;
    }


    private void exibeAlerta(String mensagem){
        Toast.makeText(getApplicationContext(),mensagem,Toast.LENGTH_LONG).show();
    }

    private boolean validaEmail(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onConfigurationChanged(final Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }



    /**
     * Método que faz o login pelo webservice
     *
     * @param params
     */
    private void invokeWS(RequestParams params){
        // Show Progress Dialog
        progressBar.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.COMPLETAR_CADASTRO_API,params ,new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                progressBar.hide();
                exibeAlerta("Cadastro efetuado com sucesso!");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                // Hide Progress Dialog
                progressBar.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }


        });
    }

}
