package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.Credito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;
import com.msouza.coffeebreak.entidades.PedidoAberto;
import com.msouza.coffeebreak.entidades.model.Configuracoes;
import com.msouza.coffeebreak.entidades.model.ItemPedido;
import com.msouza.coffeebreak.entidades.model.Produto;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class PainelActivity extends Fragment implements View.OnClickListener {

    TextView precoAntigo, novoPreco, tituloProduto, txtBoasVindas, txtSaldo, txtDataAtualizacao, txtStatusPedido, txtNumeroPedido;
    ImageView fotoProduto;

    Button verMovimentacoes, verPedidos, btnOcultarPromo, btnAdicionarCarrinho;

    DBHelper dbHelper;
    List<Produto> produtosPromocao;
    LinearLayout boxPromocao, statusPedidoAberto, layoutCompletarPerfil;
    ProgressDialog carregando;
    View defaultView;
    Long idCredito = 0l;
    Sessao sessao;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.activity_painel, container, false);
        defaultView = v;

        dbHelper = new DBHelper(v.getContext());
        produtosPromocao = new ArrayList<>();
        sessao = new Sessao(v.getContext());

        verMovimentacoes = (Button) v.findViewById(R.id.verMovimentacoes);
        verPedidos = (Button) v.findViewById(R.id.verPedidos);

        txtBoasVindas = (TextView) v.findViewById(R.id.txtBoasVindas);
        txtSaldo = (TextView) v.findViewById(R.id.txtSaldo);

        txtDataAtualizacao = (TextView) v.findViewById(R.id.txtDataAtualizacao);
        txtStatusPedido = (TextView) v.findViewById(R.id.txtStatusPedido);
        txtNumeroPedido = (TextView) v.findViewById(R.id.txtNumeroPedido);

        verMovimentacoes.setOnClickListener(this);
        verPedidos.setOnClickListener(this);

        carregando = new ProgressDialog(v.getContext());
        carregando.setMessage("Carregando..");
        carregando.setCanceledOnTouchOutside(false);
        carregando.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        Sessao sessao = new Sessao(v.getContext());
        txtBoasVindas.setText("Olá, " + sessao.getDetalhesUsuario().get(Sessao.KEY_NOME) + "!");
        txtSaldo.setText("...");
        Configuracoes config = sessao.getConfiguracoes();
        // Busca promoções apenas se o usuário quer ver
        if(config.isPromocoes()){
            // BUSCA PRODUTOS EM PROMOCAO
            dbHelper.openDataBase();
            produtosPromocao.clear();
            List<Produto> prod = dbHelper.buscaProdutosEmPromocao();
            if(prod.isEmpty()){
                buscaPromocoesWS(v);
            }else{
                produtosPromocao.addAll(prod);
            }
            dbHelper.close();
        }else{
            produtosPromocao.clear();
        }

        boxPromocao = (LinearLayout) v.findViewById(R.id.boxPromocao);
        statusPedidoAberto = (LinearLayout) v.findViewById(R.id.statusPedidoAberto);
        layoutCompletarPerfil = (LinearLayout) v.findViewById(R.id.layoutCompletarPerfil);

        layoutCompletarPerfil.setVisibility(sessao.perfilCompleto() ? View.GONE : View.VISIBLE);
        layoutCompletarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(), PerfilActivity.class));
            }
        });

        atualizaTela(v, false);

        buscaCredito(Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));

        // Atualiza cores da box do txtNomeProduto do pedido se houver

        // FIM txtNomeProduto pedido
        return v;
    }

    private int getBGBoxStatus(int status){
        switch(status){
            case 1:
                return R.drawable.box_pedido_aguardando;
            case 2:
                return R.drawable.box_pedido_aceito;
            case 3:
                return R.drawable.box_pedido_pronto;
            default:
                return R.drawable.box_pedido_aguardando;
        }

    }

    private void buscaCredito(Long idUsuario){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.CONSULTA_SALDO_CREDITO + idUsuario, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {
                Gson gson = new GsonBuilder().create();
                Credito credito = gson.fromJson(resposta.toString(), Credito.class);
                txtSaldo.setText(Utilitarios.formatarValor(credito.getTotal(), true));
                idCredito = credito.getId();
            }
        });
    }

    @Override
    public void onClick(View v) {
        Sessao sessao = new Sessao(v.getContext());

        switch (v.getId()){
            case R.id.verMovimentacoes:
                Intent intent = new Intent(getActivity(), MovimentacoesCreditoActivity.class);
                intent.putExtra("idcredito", idCredito);
                startActivity(intent);
                break;
            case R.id.verPedidos:
                // abre lista pedidos
                startActivity(new Intent(v.getContext(), ListaPedidosActivity.class));
                break;
            case R.id.btnAdicionarCarrinho:
                abreJanelaAdicionarCarrinho(v);
                break;
            case R.id.btnOcultarPromo:
                Configuracoes config = sessao.getConfiguracoes();
                config.setPromocoes(false);
                sessao.setConfiguracoes(config);
                boxPromocao.setVisibility(View.GONE);
                Toast.makeText(v.getContext(), "A promoção foi ocultada da sua tela inicial. Para ver novamente as promoções, altere a opção na tela de configurações.", Toast.LENGTH_LONG).show();
                break;
        }
    }




    private void atualizaTela(View v, boolean veioWS){
        Configuracoes config = sessao.getConfiguracoes();
        if(config.isPromocoes() && boxPromocao.getVisibility() == View.GONE && !veioWS){
            System.out.println("BUSCOU DE NOVO WS");
            buscaPromocoesWS(v);
        }else if(!config.isPromocoes()){
            produtosPromocao.clear();
        }

        if(produtosPromocao.isEmpty()){
            boxPromocao.setVisibility(View.GONE);
        }else{
            boxPromocao.setVisibility(View.VISIBLE);
            Produto promo = produtosPromocao.get(0);

            precoAntigo = (TextView) v.findViewById(R.id.precoAntigo);
            novoPreco = (TextView) v.findViewById(R.id.novoPreco);
            tituloProduto = (TextView) v.findViewById(R.id.tituloProduto);
            fotoProduto = (ImageView) v.findViewById(R.id.fotoProduto);

            precoAntigo.setText(Utilitarios.formatarValor(promo.getValorAntigo(), true));
            Utilitarios.riscaPrecoAntigo(precoAntigo);

            novoPreco.setText(Utilitarios.formatarValor(promo.getValorUnitario(), true));
            tituloProduto.setText(promo.getNome());

            Picasso.with(v.getContext()).load(Constant.URL_BASE_FOTO_CATEGORIA + promo.getFoto()).into(fotoProduto);

            btnOcultarPromo = (Button) v.findViewById(R.id.btnOcultarPromo);
            btnAdicionarCarrinho = (Button) v.findViewById(R.id.btnAdicionarCarrinho);
            btnOcultarPromo.setOnClickListener(this);
            btnAdicionarCarrinho.setOnClickListener(this);
        }

        txtBoasVindas.setText("Olá, " + sessao.getDetalhesUsuario().get(Sessao.KEY_NOME) + "!");
        txtSaldo.setText("...");
        buscaCredito(Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));

        verificaPedidoAberto();
    }

    public void buscaPromocoesWS(final View v){
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(Constant.BUSCA_PROMOCOES, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

                if(resposta.length() > 0) {
                    Type listType = new TypeToken<ArrayList<Produto>>(){}.getType();
                    List<Produto> produtos = gson.fromJson(resposta.toString(), listType);
                    System.out.println("EXIBINDO PRODUTOS");
                    for(Produto p : produtos){
                        System.out.println(p.getNome());
                        System.out.println("Promo: " + p.getPromocao());
                        System.out.println("Antigo: " + p.getValorAntigo());
                    }

                    if(produtos.size() > 0) {
                        produtosPromocao.addAll(produtos);
                        dbHelper.openDataBase();
                        dbHelper.limpaPromocoes();
                        dbHelper.salvaPromocoesDB(produtos);
                        //dbHelper.adicionaProduto(produtos.get(0));
                        dbHelper.close();
                    }

                    atualizaTela(v, true);
                }
            }
        });
    }



    private void abreJanelaAdicionarCarrinho(final View v){
        final NumberPicker picker = new NumberPicker(v.getContext());
        picker.setMinValue(1);
        picker.setMaxValue(10);

        final FrameLayout layout = new FrameLayout(v.getContext());
        layout.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        new AlertDialog.Builder(v.getContext())
                .setView(layout)
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbHelper.openDataBase();
                        ItemPedido item = new ItemPedido(produtosPromocao.get(0).getId(), picker.getValue());
                        dbHelper.adicionaItemPedido(item);
                        dbHelper.close();
                        Toast.makeText(v.getContext(), "O item (" + produtosPromocao.get(0).getNome() + ") foi adicionado ao seu pedido.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("CHAMOU ON RESUME");
        atualizaTela(defaultView, false);
    }

    private void verificaPedidoAberto(){

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.VERIFICA_PEDIDOS_ABERTOS + Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)), new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {

                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                PedidoAberto pedidoAberto = gson.fromJson(resposta.toString(), PedidoAberto.class);

                if(pedidoAberto != null){
                    System.out.println("TEM PEDIDO ABERTO");

                    statusPedidoAberto.setBackground(getResources().getDrawable(getBGBoxStatus(pedidoAberto.getStatus())));
                    statusPedidoAberto.setVisibility(View.VISIBLE);
                    txtNumeroPedido.setText(pedidoAberto.getNumero().toString());
                    txtStatusPedido.setText(Utilitarios.getStatusTexto(pedidoAberto.getStatus()));
                    txtDataAtualizacao.setText("Feito em " + new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm").format(pedidoAberto.getDataPedido()));
                }else{
                    System.out.println("NAO TEM PEDIDO ABERTO");
                    statusPedidoAberto.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                statusPedidoAberto.setVisibility(View.GONE);
            }
        });
    }
}
