package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class MovimentacoesCreditoActivity extends Activity {

    ListView listaMov;
    List<MovimentacaoCredito> movimentacoesCredito;
    ProgressDialog carregando;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimentacoes_credito);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Histórico de Crédito");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        listaMov = (ListView) findViewById(R.id.listaMovimentacoesCredito);
        movimentacoesCredito = new ArrayList<>();

        carregando = new ProgressDialog(this);
        carregando.setMessage("Carregando..");
        carregando.setCanceledOnTouchOutside(false);
        carregando.show();


        Intent intent = getIntent();
        listaMovimentacoes(intent.getLongExtra("idcredito", 0l));

        listaMov.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0) {
                    abreJanelaDetalhesMovimentacao(movimentacoesCredito.get(position - 1));
                }
            }
        });
    }

    public void listaMovimentacoes(Long idCredito){
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(Constant.LISTA_MOVIMENTACOES + idCredito, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

                Type listType = new TypeToken<ArrayList<MovimentacaoCredito>>(){}.getType();
                List<MovimentacaoCredito> movimentacoes = gson.fromJson(resposta.toString(), listType);
                movimentacoesCredito.addAll(movimentacoes);

                View header = getLayoutInflater().inflate(R.layout.cabecalho_credito,null);
                listaMov.addHeaderView(header);
                AdapterMovimentacoesCredito1 customAdapter = new AdapterMovimentacoesCredito1(movimentacoes, getApplicationContext());
                listaMov.setAdapter(customAdapter);

                carregando.dismiss();
            }
        });
    }

    private void abreJanelaDetalhesMovimentacao(MovimentacaoCredito mov){
        LayoutInflater factory = LayoutInflater.from(MovimentacoesCreditoActivity.this);
        final View view = factory.inflate(R.layout.popup_detalhe_movimentacao, null);
        final AlertDialog detalheMovimentacao = new AlertDialog.Builder(MovimentacoesCreditoActivity.this).create();
        detalheMovimentacao.setCanceledOnTouchOutside(false);
        detalheMovimentacao.setView(view);

        TextView txtDataHora = (TextView) view.findViewById(R.id.txtDataHora);
        TextView txtDescricao = (TextView) view.findViewById(R.id.txtDescricao);
        TextView txtValor = (TextView) view.findViewById(R.id.txtValor);

        String cor;
        switch(mov.getTipomovimentacao()){
            case 1:
                cor = "#dd4b39";
                break;
            case 2:
                cor = "#00a65a";
                break;
            default:
                cor = "#424242";
                break;
        }


        txtDataHora.setText((new SimpleDateFormat("dd MMM yyyy, HH:mm", new Locale("pt", "BR")).format(mov.getDatamovimentacao())).toUpperCase());
        txtDescricao.setText(mov.getDescricao());
        txtValor.setText(Utilitarios.formatarValor(mov.getValormovimentado(), true));
        txtValor.setTextColor(Color.parseColor(cor));

        view.findViewById(R.id.btnFechar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                detalheMovimentacao.dismiss();
            }
        });

        detalheMovimentacao.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }

}
