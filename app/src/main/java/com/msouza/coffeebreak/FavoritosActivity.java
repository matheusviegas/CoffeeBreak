package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.PedidoWS;
import com.msouza.coffeebreak.entidades.Usuario;
import com.msouza.coffeebreak.entidades.model.ItemPedido;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class FavoritosActivity extends Activity {

    ListView listaFavoritos;
    ProgressDialog janelaCarregando;
    public static ProgressDialog carregando;
    Sessao sessao;
    static List<Favorito> favoritos;
    private static AdapterListaFavoritos adapter;
    DBHelper dbHelper;

    ProgressBar iconeCarregando;
    TextView txtMensagem;
    LinearLayout layoutFavoritos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoritos);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Favoritos");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        txtMensagem = (TextView) findViewById(R.id.txtMensagem);
        iconeCarregando = (ProgressBar) findViewById(R.id.iconeCarregando);
        layoutFavoritos = (LinearLayout) findViewById(R.id.layoutFavoritos);

        listaFavoritos =(ListView)findViewById(R.id.listaFavoritos);
        janelaCarregando = new ProgressDialog(this);
        janelaCarregando.setMessage("Carregando..");
        janelaCarregando.setCanceledOnTouchOutside(false);

        this.carregando = new ProgressDialog(this);
        this.carregando.setMessage("Removendo..");
        this.carregando.setCanceledOnTouchOutside(false);

        favoritos = new ArrayList<>();
        sessao = new Sessao(this);
        dbHelper = new DBHelper(this);

        listaFavoritos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                abreJanelaAdicionarCarrinho(position);
            }
        });

        // Busca favoritos do banco e atualiza tela
        dbHelper.openDataBase();
        List<Favorito> favoritosBD = dbHelper.listaFavoritos(Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
        dbHelper.close();

        if(!favoritosBD.isEmpty()){
            iconeCarregando.setVisibility(View.GONE);
            txtMensagem.setVisibility(View.GONE);
            layoutFavoritos.setVisibility(View.VISIBLE);

            favoritos = favoritosBD;
            adapter = new AdapterListaFavoritos(favoritos, getApplicationContext());
            listaFavoritos.setAdapter(adapter);
        }else{
            layoutFavoritos.setVisibility(View.GONE);
            iconeCarregando.setVisibility(View.GONE);
            txtMensagem.setVisibility(View.VISIBLE);
        }

    }

    private void abreJanelaAdicionarCarrinho(final int posicao){
        final NumberPicker picker = new NumberPicker(FavoritosActivity.this);
        picker.setMinValue(1);
        picker.setMaxValue(10);

        final FrameLayout layout = new FrameLayout(FavoritosActivity.this);
        layout.addView(picker, new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.WRAP_CONTENT,
                FrameLayout.LayoutParams.WRAP_CONTENT,
                Gravity.CENTER));

        new AlertDialog.Builder(FavoritosActivity.this)
                .setView(layout)
                .setPositiveButton("Adicionar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dbHelper.openDataBase();
                        ItemPedido item = new ItemPedido(favoritos.get(posicao).getProdutoid(), picker.getValue());
                        dbHelper.adicionaItemPedido(item);
                        dbHelper.close();
                        Toast.makeText(FavoritosActivity.this, "O item (" + favoritos.get(posicao).getNome() + ") foi adicionado ao seu pedido.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancelar", null)
                .show();
    }


    public static void removeFavoritoLista(int posicao){
        adapter.remove(favoritos.get(posicao));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
