package com.msouza.coffeebreak;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.msouza.coffeebreak.chat.Mensagem;
import com.msouza.coffeebreak.chat.TipoMensagem;
import com.msouza.coffeebreak.entidades.MensagemChamado;
import com.msouza.coffeebreak.entidades.PedidoWS;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Utilitarios {

    public static String formatarValor(Double valor){
        return formatarValor(valor, false);
    }

    public static String formatarValor(Double valor, boolean simbolo) {
        String valorFormatado = "";
        DecimalFormat df = new DecimalFormat("#,##0.00", new DecimalFormatSymbols(new Locale("pt", "BR")));
        if (simbolo) {
            valorFormatado = "R$ ";
        }
        if (valor == null || valor == 0) {
            valorFormatado += "0,00";
        } else {
            valorFormatado += df.format(valor);
        }
        return valorFormatado;
    }

    public static String getURLFoto(String url){
        url = url.replaceAll("\\\\", "");
        url = url.replaceAll("amp;", "");
        return url;
    }

    public static String getFotoPerfil(String url, Long idFB){
        if(idFB != null){
            return "https://graph.facebook.com/" + idFB + "/picture?type=large";
        }else{
            return getURLFoto(url);
        }
    }

    public static String getURLFotoPerfil(String url){
        return url.contains("https") ? getURLFoto(url) : (Constant.URL_BASE_FOTO_CATEGORIA + url);
    }

    public static ProgressDialog abrirJanela(Context context, String titulo){
        ProgressDialog janela = new ProgressDialog(context);
        janela.setMessage(titulo);
        janela.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        janela.setCancelable(false);
        return janela;
    }

    public static List<Mensagem> converteMensagemChamado(List<MensagemChamado> mensagensChamado){
        List<Mensagem> mensagens = new ArrayList<>();
        for(MensagemChamado mc : mensagensChamado) {
            mensagens.add(new Mensagem(mc.getMensagem(), mc.getAdminenviou() == 1 ? TipoMensagem.RECEBIDA : TipoMensagem.ENVIADA, mc.getDataenvio()));
        }
        return mensagens;
    }

    public static String getStatusTexto(int status){
        switch (status){
            case 0:
                return "Rascunho";
            case 1:
                return "Aguardando Aprovação";
            case 2:
                return "Em Produção";
            case 3:
                return "Pronto";
            case 4:
                return "Cancelado";
            case 5:
            case 6:
                return "Entregue";
            default:
                return "Desconhecido";
        }
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            System.out.println("ALTURA: " + listItem.getMeasuredHeight());
            totalHeight += listItem.getMeasuredHeight();
        }

        int sub = (listAdapter.getCount() > 2) ? (listAdapter.getCount()-1) * 10 : 0;
        System.out.println("SUB: " + sub);
        System.out.println("DIVIDER HEIGHT: " + listView.getDividerHeight());
        System.out.println("COUNT: " + listAdapter.getCount());

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1) - sub);
        listView.setLayoutParams(params);
    }

    public static void setListViewHeightBasedOnChildren2(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }


        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static void riscaPrecoAntigo(TextView textView){
        textView.setPaintFlags(textView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }


    public static boolean isNull(EditText edit){
        return edit.getText().toString().trim() == null || edit.getText().toString().trim().equals("");
    }

    public static Double calculaValorDescontado(PedidoWS pedido){
        Double porcentagem = (0d + pedido.getDesconto()) / 100;
        return pedido.getDesconto() > 0 ? (pedido.getValortotal() * porcentagem) : 0d;
    }

}
