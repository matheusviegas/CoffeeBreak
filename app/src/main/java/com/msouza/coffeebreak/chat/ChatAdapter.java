package com.msouza.coffeebreak.chat;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.msouza.coffeebreak.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ChatAdapter extends ArrayAdapter<Mensagem> {

    private TextView txtMensagem, txtDataHora;
    private List<Mensagem> listaMensagens = new ArrayList<>();
    private Context context;
    private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");

    @Override
    public void add(Mensagem mensagem) {
        listaMensagens.add(mensagem);
        super.add(mensagem);
    }

    @Override
    public void addAll(@NonNull Collection<? extends Mensagem> lista) {
        listaMensagens.addAll(lista);
        super.addAll(lista);
    }

    public ChatAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
    }

    public int getCount() {
        return this.listaMensagens.size();
    }

    public Mensagem getItem(int index) {
        return this.listaMensagens.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Mensagem mensagem = getItem(position);
        View row;
        LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        boolean mensagemInicial = false;

        switch(mensagem.getTipoMensagem()){
            case ENVIADA:
                row = inflater.inflate(R.layout.chat_mensagem_enviada, parent, false);
                break;
            case RECEBIDA:
                row = inflater.inflate(R.layout.chat_mensagem_recebida, parent, false);
                break;
            default:
                row = inflater.inflate(R.layout.chat_mensagem_inicial, parent, false);
                mensagemInicial = true;
                break;
        }


        txtMensagem = (TextView) row.findViewById(R.id.campoMensagem);
        txtMensagem.setText(mensagem.getTexto());
        if(!mensagemInicial){
            txtDataHora = (TextView) row.findViewById(R.id.txtDataHora);
            txtDataHora.setText(sdf.format(mensagem.getDataMensagem()));
        }
        return row;
    }
}