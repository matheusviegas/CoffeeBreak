package com.msouza.coffeebreak.chat;

import java.util.Date;

public class Mensagem{
    private String texto;
    private TipoMensagem tipoMensagem;
    private Date dataMensagem;

    public Mensagem(String texto, TipoMensagem tipoMensagem, Date dataMensagem) {
        this.texto = texto;
        this.tipoMensagem = tipoMensagem;
        this.dataMensagem = dataMensagem;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public TipoMensagem getTipoMensagem() {
        return tipoMensagem;
    }

    public void setTipoMensagem(TipoMensagem tipoMensagem) {
        this.tipoMensagem = tipoMensagem;
    }

    public Date getDataMensagem() {
        return dataMensagem;
    }

    public void setDataMensagem(Date dataMensagem) {
        this.dataMensagem = dataMensagem;
    }
}