package com.msouza.coffeebreak.chat;

public enum TipoMensagem {
    ENVIADA, RECEBIDA, INICIAL;
}
