package com.msouza.coffeebreak;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.msouza.coffeebreak.entidades.PedidoWS;

import java.text.SimpleDateFormat;
import java.util.List;

public class AdapterListaPedidos extends ArrayAdapter<PedidoWS>{

    private List<PedidoWS> dataSet;
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    private static class ViewHolder {
        TextView titulo, status, data, valorTotal;
    }

    public AdapterListaPedidos(List<PedidoWS> data, Context context) {
        super(context, R.layout.pedido_item_lista, data);
        this.dataSet = data;
        this.mContext=context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PedidoWS pedido = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.pedido_item_lista, parent, false);
            viewHolder.titulo = (TextView) convertView.findViewById(R.id.titulo);
            viewHolder.status = (TextView) convertView.findViewById(R.id.status);
            viewHolder.data = (TextView) convertView.findViewById(R.id.data);
            viewHolder.valorTotal = (TextView) convertView.findViewById(R.id.valorTotal);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.titulo.setText("Pedido Nº: " + pedido.getId());
        viewHolder.status.setText(Utilitarios.getStatusTexto(pedido.getStatus()));
        viewHolder.data.setText(sdf.format(pedido.getDatapedido()));
        viewHolder.valorTotal.setText(Utilitarios.formatarValor(pedido.getValortotal() - Utilitarios.calculaValorDescontado(pedido), true));

        return convertView;
    }
}