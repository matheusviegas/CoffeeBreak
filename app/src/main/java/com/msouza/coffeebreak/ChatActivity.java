package com.msouza.coffeebreak;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.chat.ChatAdapter;
import com.msouza.coffeebreak.chat.Mensagem;
import com.msouza.coffeebreak.chat.TipoMensagem;
import com.msouza.coffeebreak.entidades.Chamado;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MensagemChamado;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;
import com.msouza.coffeebreak.entidades.Usuario;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;

public class ChatActivity extends Activity {

    private ChatAdapter chatAdapter;
    private ListView listaMensagens;
    private EditText txtMensagem;
    private Button btnEnviarMensagem;
    private boolean side = false;

    ProgressDialog progressDialog;
    String dataAbertura = "", tituloChamado = "";
    Long idChamado = 0l;
    Date dataFechamento = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Chamado");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

        // Inicia Janela Carregando
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Abrindo chamado..");

        //CHAT
        btnEnviarMensagem = (Button) findViewById(R.id.btnEnviar);
        txtMensagem = (EditText) findViewById(R.id.txtMensagem);

        listaMensagens = (ListView) findViewById(R.id.listaMensagens);
        chatAdapter = new ChatAdapter(getApplicationContext(), R.layout.chat_mensagem_enviada);
        listaMensagens.setAdapter(chatAdapter);

        // Enviar mensagem no ENTER no celular
        txtMensagem.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER) &&  validaMensagem()) {
                    return enviarMensagem();
                }
                return false;
            }
        });


        // Enviar mensagem ao clicar no botão
        btnEnviarMensagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(validaMensagem()) {
                    enviarMensagem();
                }
            }
        });

        listaMensagens.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listaMensagens.setAdapter(chatAdapter);

        // Rola o scroll pra baixo quando detectar que os dados mudaram (mensagem enviada)
        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listaMensagens.setSelection(chatAdapter.getCount() - 1);
            }
        });


        // CÓDIGO PARA INICIALIZAR A CONVERSA
        Intent intent = getIntent();
        progressDialog.show();
        tituloChamado = intent.getStringExtra("titulo");
        if(intent.hasExtra("idChamado") && intent.hasExtra("dataAbertura")){
            bar.setTitle("Chamado Nº: " + intent.getLongExtra("idChamado", 1l));
            dataAbertura = intent.getStringExtra("dataAbertura");
            idChamado = intent.getLongExtra("idChamado", 1l);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
                //sdf.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));

             //   sdf.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
                dataFechamento = (intent.hasExtra("dataFechamento") ? sdf.parse(intent.getStringExtra("dataFechamento")) : null);
            }catch (Exception e){
                Log.d("Erro: ", "Erro ao converter data");
            }
            carregaChamadoWS(intent.getLongExtra("idChamado", 1l));
            // carrega chamado antigo - busca no WS
        }else{
            Sessao sessao = new Sessao(this);
            RequestParams parametros = new RequestParams();
            parametros.put("usuarioid", Long.parseLong(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
            parametros.put("titulo", intent.getStringExtra("titulo"));
            iniciaChamadoWS(parametros);
        }

        // Adiciona mensagem apresentação
       // SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy às HH:mm");
        //chatAdapter.add(new Mensagem("Chat iniciado em " + sdf.format(new Date()), TipoMensagem.INICIAL, null));
       // chatAdapter.add(new Mensagem("Olá! Como posso te ajudar?", TipoMensagem.RECEBIDA, new Date()));
    }

    private boolean enviarMensagem() {
        enviaMensagemWS(txtMensagem.getText().toString());
        return true;
    }

    private boolean validaMensagem(){
        if(txtMensagem.getText().toString().equals("")){
            Toast.makeText(ChatActivity.this, "Digite algo para enviar..", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    private void atualizaTela(List<Mensagem> mensagens){
        chatAdapter.add(new Mensagem("Chamado (" + tituloChamado + ") iniciado em " + dataAbertura + ".", TipoMensagem.INICIAL, null));
        chatAdapter.addAll(mensagens);
        txtMensagem.setText("");
        if(dataFechamento != null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
            //sdf.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
            chatAdapter.add(new Mensagem("Chamado encerrado em " + sdf.format(dataFechamento) + ".", TipoMensagem.INICIAL, null));
            txtMensagem.setEnabled(false);
            btnEnviarMensagem.setEnabled(false);
            txtMensagem.setFocusable(false);
        }
    }


    private void iniciaChamadoWS(RequestParams params){
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.INICIA_CHAMADO, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                Chamado chamado = gson.fromJson(respostaJson.toString(), Chamado.class);
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");
              //  sdf.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
                dataAbertura = sdf.format(chamado.getData_abertura());
                idChamado = chamado.getId();
                carregaChamadoWS(chamado.getId());
            }
        });
    }

    private void carregaChamadoWS(Long id){
        getActionBar().setTitle("Chamado Nº: " + id);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.CARREGA_CHAMADO + "/" + id, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                Type listType = new TypeToken<ArrayList<MensagemChamado>>(){}.getType();

                List<MensagemChamado> mensagensChamado = gson.fromJson(resposta.toString(), listType);
                List<Mensagem> mensagens = Utilitarios.converteMensagemChamado(mensagensChamado);
                progressDialog.dismiss();
                atualizaTela(mensagens);
            }
        });
    }

    private void enviaMensagemWS(final String mensagem){
        chatAdapter.add(new Mensagem(txtMensagem.getText().toString(), TipoMensagem.ENVIADA, new Date()));
        txtMensagem.setText("");

        RequestParams params = new RequestParams();
        params.put("chamadoid", idChamado);
        params.put("mensagem", mensagem);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.ENVIA_MENSAGEM_WS, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                System.out.println("MENSAGEM ENVIADA [SUCESSO]: " + mensagem);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                overridePendingTransition(R.anim.open_main, R.anim.close_next);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
}
