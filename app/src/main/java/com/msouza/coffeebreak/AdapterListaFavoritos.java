package com.msouza.coffeebreak;

import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.Usuario;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class AdapterListaFavoritos extends ArrayAdapter<Favorito> {

    private List<Favorito> dataSet;
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    DBHelper dbHelper;
    Sessao sessao;

    private static class ViewHolder {
        TextView txtPrecoUnitario, txtNomeProduto;
        ImageView btnRemoverFavoritos;
    }

    public AdapterListaFavoritos(List<Favorito> data, Context context) {
        super(context, R.layout.favoritos_item_lista, data);
        this.dataSet = data;
        this.mContext=context;
        this.dbHelper = new DBHelper(context);
        this.sessao = new Sessao(context);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Favorito favorito = getItem(position);
        final ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.favoritos_item_lista, parent, false);
            viewHolder.txtPrecoUnitario = (TextView) convertView.findViewById(R.id.txtPrecoUnitario);
            viewHolder.txtNomeProduto = (TextView) convertView.findViewById(R.id.txtNomeProduto);
            viewHolder.btnRemoverFavoritos = (ImageView) convertView.findViewById(R.id.btnRemoverFavoritos);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtPrecoUnitario.setText(Utilitarios.formatarValor(favorito.getValorUnitario(), true));
        viewHolder.txtNomeProduto.setText(favorito.getNome());
        viewHolder.btnRemoverFavoritos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                removeFavoritoWS(favorito, position);
            }
        });


        return convertView;
    }

    private void removeFavoritoWS(final Favorito fav, final int posicao){
        FavoritosActivity.carregando.show();
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.REMOVER_FAVORITO, new RequestParams("id", fav.getId()), new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);
                Toast.makeText(mContext, retorno.getMensagem(), Toast.LENGTH_SHORT).show();
                FavoritosActivity.carregando.dismiss();
                FavoritosActivity.removeFavoritoLista(posicao);

                dbHelper.openDataBase();
                dbHelper.removeFavorito(fav.getProdutoid(), Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
                dbHelper.close();
            }

        });
    }

}