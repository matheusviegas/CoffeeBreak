package com.msouza.coffeebreak;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.Credito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;
import com.msouza.coffeebreak.entidades.ViaCEP;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import cz.msebera.android.httpclient.Header;

/**
 * Created by Matheus on 27/06/2017.
 */

public class TabCredito extends Fragment {

   // TextView txt;
    ListView listaMov;
    private ArrayAdapter<String> listAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.tab_credito1, container, false);


       // txt = (TextView) rootView.findViewById(R.id.txtCredito);
        listaMov = (ListView) rootView.findViewById( R.id.listaMovimentacoesCredito );



        Sessao sessao = new Sessao(getActivity().getApplicationContext());
        HashMap<String, String> user = sessao.getDetalhesUsuario();
        Long idUsuario = Long.valueOf(user.get(Sessao.KEY_IDUSUARIO));
        //idUsuario = 12l;


        AsyncHttpClient client = new AsyncHttpClient();
        client.get(Constant.CONSULTA_SALDO_CREDITO + idUsuario, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {

                Gson gson = new GsonBuilder().create();
                Credito credito = gson.fromJson(resposta.toString(), Credito.class);

               // txt.setText("Seu saldo de crédito é de " + credito.getTotal() + " CreditoID: " + credito.getId() + " ToString: " + resposta.toString());

                listaMovimentacoes(credito.getId());

            }
        });



/*
        Type collectionType = new TypeToken<Collection<Integer>>(){}.getType();
        Collection<Integer> ints2 = gson.fromJson(json, collectionType);*/


        return rootView;
    }

    public void listaMovimentacoes(Long idCredito){
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(Constant.LISTA_MOVIMENTACOES + idCredito, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

                Type listType = new TypeToken<ArrayList<MovimentacaoCredito>>(){}.getType();
                List<MovimentacaoCredito> movimentacoes = gson.fromJson(resposta.toString(), listType);


                for(MovimentacaoCredito mov : movimentacoes){
                    System.out.println(mov.toString());
                }


                View header = getActivity().getLayoutInflater().inflate(R.layout.cabecalho_credito,null);
                listaMov.addHeaderView(header);
               // ListView yourListView = (ListView) findViewById(R.id.itemListView);

// get data from the table by the ListAdapter
                AdapterMovimentacoesCredito customAdapter = new AdapterMovimentacoesCredito(getActivity().getApplicationContext(), R.layout.movimentacao_credito_item, movimentacoes);
                listaMov .setAdapter(customAdapter);







               /* String[] planets = new String[] { "Mercury", "Venus", "Earth", "Mars",
                        "Jupiter", "Saturn", "Uranus", "Neptune"};
                ArrayList<String> planetList = new ArrayList<String>();
                planetList.addAll(Arrays.asList(planets));

                listAdapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.movimentacao_credito_item, planetList);

                listAdapter.add( "Ceres" );
                listAdapter.add( "Pluto" );
                listAdapter.add( "Haumea" );
                listAdapter.add( "Makemake" );
                listAdapter.add( "Eris" );

                listaMov.setAdapter(listAdapter);*/

            }
        });
    }



}
