package com.msouza.coffeebreak;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.msouza.coffeebreak.entidades.Chamado;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class AdapterListaChamados extends ArrayAdapter<Chamado>{

    private List<Chamado> dataSet;
    Context mContext;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy 'às' HH:mm");

    private static class ViewHolder {
        TextView titulo;
        TextView status;
        TextView dataHora;
    }

    public AdapterListaChamados(List<Chamado> data, Context context) {
        super(context, R.layout.chamado_item_lista, data);
        this.dataSet = data;
        this.mContext=context;

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Chamado chamado = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.chamado_item_lista, parent, false);
            viewHolder.titulo = (TextView) convertView.findViewById(R.id.titulo);
            viewHolder.status = (TextView) convertView.findViewById(R.id.status);
            viewHolder.dataHora = (TextView) convertView.findViewById(R.id.dataHora);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.titulo.setText(chamado.getTitulo());
        viewHolder.status.setText(chamado.getData_fechamento() == null ? "Em Aberto" : "Fechado");
        viewHolder.status.setBackgroundColor(chamado.getData_fechamento() == null ? mContext.getResources().getColor(R.color.header) : mContext.getResources().getColor(R.color.chat_fundo_enviado));
        viewHolder.dataHora.setText(sdf.format(chamado.getData_abertura()));

        return convertView;
    }
}