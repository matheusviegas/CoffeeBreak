package com.msouza.coffeebreak;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.msouza.coffeebreak.entidades.Categoria;
import com.msouza.coffeebreak.entidades.Favorito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.ParametrosSistema;
import com.msouza.coffeebreak.entidades.model.Produto;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class ActivitySplash extends Activity {

	DBHelper dbHelper;
	Sessao sessao;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash);

		sessao = new Sessao(this);
		dbHelper = new DBHelper(this);

		// Verifica se o banco existe, se não, cria
		try {
			dbHelper.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Erro ao criar banco");
		}

		buscaFavoritosWS(Long.valueOf(sessao.getDetalhesUsuario().get(Sessao.KEY_IDUSUARIO)));
		verificaVersao();
    }

	private void buscaCategoriasWS(){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.LISTA_CATEGORIAS, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

				Type listType = new TypeToken<ArrayList<Categoria>>(){}.getType();
				List<Categoria> categorias = gson.fromJson(resposta.toString(), listType);

				dbHelper.openDataBase();
				dbHelper.deletaTodasCategorias();
				for(Categoria cat : categorias){
					dbHelper.adicionaCategoria(cat);
				}
				dbHelper.close();

				buscaProdutosWS();
			}
		});
	}

	private void buscaProdutosWS(){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.LISTA_TODOS_PRODUTOS, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();

				if(resposta.length() > 0) {
					Type listType = new TypeToken<ArrayList<Produto>>(){}.getType();
					List<Produto> produtos = gson.fromJson(resposta.toString(), listType);

					dbHelper.openDataBase();
					dbHelper.deletaTodosProtudos();
					for(Produto p : produtos){
						dbHelper.adicionaProduto(p);
					}
					dbHelper.close();

					redirecionaPainel();
				}
			}
		});
	}

	private void redirecionaPainel(){
		if(sessao.isLogado()){
			Intent painel = new Intent(ActivitySplash.this, MainActivity.class);
			painel.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			painel.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			painel.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			startActivity(painel);
		}else {
			startActivity(new Intent(getApplicationContext(), ActivityLogin.class));
			overridePendingTransition(R.anim.open_next, R.anim.close_next);
		}

		finish();
	}

	private void verificaVersao(){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.BUSCA_VERSAO_BANCO, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {

				Gson gson = new GsonBuilder().create();
				ParametrosSistema parametrosSistema = gson.fromJson(resposta.toString(), ParametrosSistema.class);
				sessao.setDesconto(parametrosSistema.getDesconto());

				if(parametrosSistema.getVersaoApp() == BuildConfig.VERSION_CODE) {
					dbHelper.openDataBase();
					boolean temCategoriasBanco = dbHelper.temCategoriasNoBanco();
					dbHelper.close();

					if (temCategoriasBanco && parametrosSistema.getVersaoBanco().equals(sessao.getVersaoBanco())) {
						redirecionaPainel();
						System.out.println("VERSAO IGUAL");
					} else {
						sessao.setVersaoBanco(parametrosSistema.getVersaoBanco());
						buscaCategoriasWS();
						System.out.println("ATUALIZOU DADOS");
					}
				}else{
					Intent atualizarApp = new Intent(ActivitySplash.this, AppDesatualizadoActivity.class);
					atualizarApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					atualizarApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
					atualizarApp.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					startActivity(atualizarApp);
				}
			}
		});
	}

	private void buscaFavoritosWS(final Long idUsuario){
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(Constant.LISTA_FAVORITOS_BY_USUARIO + idUsuario, new JsonHttpResponseHandler() {
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONArray resposta) {
				Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
				Type listType = new TypeToken<ArrayList<Favorito>>(){}.getType();
				List<Favorito> favoritosWS = gson.fromJson(resposta.toString(), listType);
				dbHelper.openDataBase();
				dbHelper.limpaFavoritos();
				for(Favorito f : favoritosWS){
					dbHelper.adicionaFavoritos(f.getProdutoid(), idUsuario);
				}
				dbHelper.close();
			}
		});
	}


}