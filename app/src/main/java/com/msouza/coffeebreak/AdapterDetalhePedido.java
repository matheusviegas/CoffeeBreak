package com.msouza.coffeebreak;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.msouza.coffeebreak.entidades.ItemPedidoWS;
import java.text.SimpleDateFormat;
import java.util.List;

public class AdapterDetalhePedido extends ArrayAdapter<ItemPedidoWS>{
    private List<ItemPedidoWS> dataSet;
    Context mContext;

    private static class ViewHolder {
        TextView nomeProduto, quantidade, valor;
    }

    public AdapterDetalhePedido(List<ItemPedidoWS> data, Context context) {
        super(context, R.layout.detalhe_pedido_item_lista, data);
        this.dataSet = data;
        this.mContext=context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemPedidoWS item = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.detalhe_pedido_item_lista, parent, false);
            viewHolder.nomeProduto = (TextView) convertView.findViewById(R.id.nomeProduto);
            viewHolder.quantidade = (TextView) convertView.findViewById(R.id.quantidade);
            viewHolder.valor = (TextView) convertView.findViewById(R.id.valor);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nomeProduto.setText(item.getNome());
        viewHolder.quantidade.setText(item.getQuantidade() + "x");
        viewHolder.valor.setText(Utilitarios.formatarValor(item.getValor(), true));

        return convertView;
    }
}