package com.msouza.coffeebreak;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.RetornoAtualizaFoto;

import org.json.JSONObject;

import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class ReativarContaActivity extends Activity implements View.OnClickListener{

    ProgressDialog carregando;
    Long idUsuario;
    TextView mensagem, nome;
    Button reativar, voltar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reativar_conta);

        mensagem = (TextView) findViewById(R.id.mensagem);
        nome = (TextView) findViewById(R.id.nome);
        voltar = (Button) findViewById(R.id.btnVoltar);
        reativar = (Button) findViewById(R.id.btnReativar);
        voltar.setOnClickListener(this);
        reativar.setOnClickListener(this);

        carregando = new ProgressDialog(this);
        carregando.setMessage("Reativando sua conta..");
        carregando.setCanceledOnTouchOutside(false);
        carregando.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        Intent intent = getIntent();
        idUsuario = intent.getLongExtra("id", 0l);
        nome.setText("Olá, " + intent.getStringExtra("nome") + "!");
        mensagem.setText("Você tem até o dia " + intent.getStringExtra("datalimite") + " para reativar sua conta.\n\nApós esta data, não será mais possível recuperar sua conta e a mesma será desativada permanentemente.");

    }

    private void reativarContaWS(){
        carregando.show();

        RequestParams params = new RequestParams();
        params.put("id", idUsuario);

        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Constant.REATIVAR_CONTA, params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new GsonUTCDateAdapter()).create();
                RetornoAtualizaFoto retorno = gson.fromJson(respostaJson.toString(), RetornoAtualizaFoto.class);
                carregando.dismiss();
                Toast.makeText(ReativarContaActivity.this, retorno.getMensagem(), Toast.LENGTH_LONG).show();
                voltarLogin();
            }

        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnReativar:
                reativarContaWS();
                break;
            case R.id.btnVoltar:
                voltarLogin();
                break;
        }
    }

    private void voltarLogin(){
        Intent voltarLogin = new Intent(ReativarContaActivity.this, ActivityLogin.class);
        voltarLogin.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        voltarLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        voltarLogin.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(voltarLogin);
        overridePendingTransition(R.anim.open_next, R.anim.close_next);
    }
}
