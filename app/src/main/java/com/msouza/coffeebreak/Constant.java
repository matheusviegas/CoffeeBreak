package com.msouza.coffeebreak;

import java.io.InputStream;
import java.io.OutputStream;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

enum Ambiente{
	LOCALHOST, TESTE, PRODUCAO;
}

public class Constant {

	// Define o Ambiente de Implantação
	private static final Ambiente ambiente = Ambiente.PRODUCAO;

    //static String BASE_URL_MAIN = "http://10.112.224.19";
    //static String BASE_URL = "http://10.112.224.19/restful";

	// ULTIMA OK LOCALHOST
	//static String BASE_URL_MAIN = "http://192.168.15.3";
	//static String BASE_URL = BASE_URL_MAIN + "/restful";

	// TESTE HOSTGATOR
	static String BASE_URL_MAIN = "http://msouzadev.com";
	static String BASE_URL = BASE_URL_MAIN;

	//static String BASE_URL_MAIN = montaURLBase()[0];
	//static String BASE_URL = montaURLBase()[1];


	static String CONSULTA_SALDO_CREDITO = BASE_URL + "/credito/";
	static String LISTA_MOVIMENTACOES = BASE_URL + "/credito/movimentacoes/";

	static String AdminPageURL = BASE_URL_MAIN + "/ecommerce/";
	static String CategoryAPI = BASE_URL_MAIN + "/ecommerce/api/get-all-category-data.php";
	static String MenuAPI = BASE_URL_MAIN + "/ecommerce/api/get-menu-data-by-category-id.php";
	static String TaxCurrencyAPI  =  BASE_URL_MAIN + "/ecommerce/api/get-tax-and-currency.php";
	static String MenuDetailAPI = BASE_URL_MAIN + "/ecommerce/api/get-menu-detail.php";
	static String SendDataAPI = BASE_URL_MAIN + "/ecommerce/api/add-reservation.php";

	static String LOGIN_API = BASE_URL + "/usuarios/logar";
	static String CADASTRO_API = BASE_URL + "/usuarios/cadastrar";
	static String COMPLETAR_CADASTRO_API = BASE_URL + "/usuarios/completar";
	static String LISTA_CATEGORIAS = BASE_URL + "/categoria/lista";
	static String LISTA_PRODUTOS = BASE_URL + "/produtos/"; // idCategoria/stringBusca
	static String DETALHE_PRODUTO = BASE_URL + "/produto/";
	static String PERFIL_USUARIO_BY_ID = BASE_URL + "/usuarios/";
	static String SALVA_ALTERACOES_PERFIL = BASE_URL + "/usuarios/salvarAlteracoes";
	static String FINALIZA_PEDIDO = BASE_URL + "/pedido/finalizar";
	public static String ATUALIZA_TOKEN = BASE_URL + "/usuarios/atualizaToken";
	public static String REATIVAR_CONTA = BASE_URL + "/usuarios/reativarConta";

	public static String RECUPERAR_SENHA = "http://msouzadev.com/cbgerencial/ContaUsuario/redefinirSenha";

	public static String ALTERAR_FOTO_PERFIL = BASE_URL + "/usuarios/alterarFoto";
	public static String CANCELAR_CONTA = BASE_URL + "/usuarios/cancelarConta";
	public static String CANCELAR_CONTA_DEFINITIVO = BASE_URL + "/usuarios/cancelarDefinitivo";



	// CHAMADO
	static String INICIA_CHAMADO = BASE_URL + "/chamado/iniciar";
	static String CARREGA_CHAMADO = BASE_URL + "/chamado/listaMensagens";
	static String BUSCA_CHAMADO_ID = BASE_URL + "/chamado";
	static String ENVIA_MENSAGEM_WS = BASE_URL + "/chamado/enviar_mensagem";
	static String LISTA_CHAMADOS_BY_USUARIO = BASE_URL + "/chamado/listar";
	static String ENVIA_AVALIACAO_WS = BASE_URL + "/chamado/avaliar";

	// Sugestão
	static String ENVIAR_SUGESTAO = BASE_URL + "/sugestao/enviar";

	// PEDIDOS
	static String LISTA_PEDIDOS_BY_USUARIO = BASE_URL + "/pedido/listar";
	static String LISTA_ITENS_PEDIDO_FEITO = BASE_URL + "/pedido/itens";
	static String VERIFICA_PEDIDOS_ABERTOS_USUARIO = BASE_URL + "/pedido/verificapedidoaberto";

	static String VERIFICA_PEDIDOS_ABERTOS = BASE_URL + "/pedido/verificapedidoabertonotificacao/";

	// FAVORITOS
	static String LISTA_FAVORITOS_BY_USUARIO = BASE_URL + "/produtos/favoritos/";
	static String REMOVER_FAVORITO = BASE_URL + "/produtos/removefavorito";
	static String ADICIONAR_FAVORITO = BASE_URL + "/produtos/adicionafavorito";
	static String VERIFICA_FAVORITO = BASE_URL + "/produtos/favoritado";


	// PRODUTOS E CATEGORIAS AO INIICAR APP
	static String LISTA_TODOS_PRODUTOS = BASE_URL + "/produtos/todos";

	// VERSAO DO BANCO
	static String BUSCA_VERSAO_BANCO = BASE_URL + "/versao";

	// PROMOCOES
	static String BUSCA_PROMOCOES = BASE_URL + "/produto/promocoes";

	static String URL_BASE_FOTO_CATEGORIA = BASE_URL_MAIN + "/cbgerencial/uploads/";
	
	static String AccessKey = "12345";
	
	static String DBPath = "/data/data/com.msouza.coffeebreak/databases/";
	
	// verifica se tem conexão com a internet
	public static boolean isNetworkAvailable(Activity activity) {
		ConnectivityManager connectivity = (ConnectivityManager) activity
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			return false;
		} else {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	// Salva as imagens no dispositivo
	public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }


    private static String[] montaURLBase(){
		String base, base_main;

		if(ambiente.equals(Ambiente.LOCALHOST)){
			base_main = "http://192.168.15.3";
			base = base_main + "/restful";
		}else{
			base_main = "https://msouzadev.com";
			base = base_main;
		}

		return new String[]{base_main, base};

	}

    public static String getBaseURL(){
		switch(ambiente){
			case LOCALHOST:
				return "http://192.168.15.3";
			case PRODUCAO:
			case TESTE:
				return "http://msouzadev.com";
			default:
				return "http://msouzadev.com";
		}
	}

	public static String getBaseURL(String contexto){
		switch(ambiente){
			case LOCALHOST:
				return "http://192.168.15.3" + contexto;
			case PRODUCAO:
			case TESTE:
				return "http://msouzadev.com" + contexto;
			default:
				return "http://msouzadev.com" + contexto;
		}
	}

}
