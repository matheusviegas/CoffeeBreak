package com.msouza.coffeebreak;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.msouza.coffeebreak.entidades.Credito;
import com.msouza.coffeebreak.entidades.GsonUTCDateAdapter;
import com.msouza.coffeebreak.entidades.ItemPedidoDecorator;
import com.msouza.coffeebreak.entidades.RetornoCount;
import com.msouza.coffeebreak.entidades.RetornoRequisicao;
import com.msouza.coffeebreak.entidades.model.Notificacao;
import com.msouza.coffeebreak.entidades.model.PedidoDecorator;
import com.msouza.coffeebreak.entidades.model.PedidoStatus;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ActivityCheckout extends FragmentActivity {
	
	Button btnSend;
	static Button btnDate, btnTime;
	EditText edtOrderList, edtComment;
	ScrollView sclDetail;
	ProgressBar prgLoading;
	TextView txtAlert;
    Spinner formaPagamento;

	// Helper do Banco de Dados
	static DBHelper dbhelper;
	ArrayList<ArrayList<Object>> data;

	String OrderList = "";

	// Variáveis pra armazenar a data e a hora de retirada
	private static int mYear;
	private static int mMonth;
	private static int mDay;
	private static int mHour;
	private static int mMinute;
	
	// declare static variables to store tax and currency data
	static double Tax;

	
	static final String TIME_DIALOG_ID = "timePicker";
	static final String DATE_DIALOG_ID = "datePicker";

	DecimalFormat formatData = new DecimalFormat("#.##");

	List<ItemPedidoDecorator> itensPedido = new ArrayList<>();
	Double totalPedido = 0d;
	Long idCliente = 0l;
	ProgressDialog progressDialog;
	Sessao sessao;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.checkout);
        
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.header)));
        bar.setTitle("Finalizar Pedido");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.setHomeButtonEnabled(true);

		sessao = new Sessao(this);

        btnDate = (Button) findViewById(R.id.btnDate);
        btnTime = (Button) findViewById(R.id.btnTime);
        edtOrderList = (EditText) findViewById(R.id.edtOrderList);
        edtComment = (EditText) findViewById(R.id.edtComment);
        btnSend = (Button) findViewById(R.id.btnSend);
        sclDetail = (ScrollView) findViewById(R.id.sclDetail);
        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
        txtAlert = (TextView) findViewById(R.id.txtAlert);

        formaPagamento = (Spinner) findViewById(R.id.formaPagamento);

		// Pega ID Usuario da session e armazena
		Sessao sessao = new Sessao(ActivityCheckout.this);
		HashMap<String, String> user = sessao.getDetalhesUsuario();
		idCliente = Long.valueOf(user.get(Sessao.KEY_IDUSUARIO));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.formasPagamento, R.layout.spinner_item_pagamento);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        formaPagamento.setAdapter(adapter);

        dbhelper = new DBHelper(this);
		try{
			dbhelper.openDataBase();
		}catch(SQLException sqle){
			throw sqle;
		}
		

        // Abre o Picker da DATA
        btnDate.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				DialogFragment newFragment = new DatePickerFragment();
			    newFragment.show(getSupportFragmentManager(), DATE_DIALOG_ID);
			}
		});
        
        // Abre o Picker da HORA
        btnTime.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				DialogFragment newFragment = new TimePickerFragment();
			    newFragment.show(getSupportFragmentManager(), TIME_DIALOG_ID);
			}
		});

        // FINALIZAR O PEDIDO - ENVIAR PRO WEBSERVICE
        btnSend.setOnClickListener(new OnClickListener() {
			
			public void onClick(View arg0) {

				if(validaPedido()) {
					String retirada = btnDate.getText() + " " + btnTime.getText() + ":00";

					Date dataRetirada = null;
					try {
						dataRetirada = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(retirada);
					} catch (ParseException e) {
						e.printStackTrace();
					}

					PedidoDecorator pedido = new PedidoDecorator();
					pedido.setItens(itensPedido);
					pedido.setTipopagamento(formaPagamento.getSelectedItemPosition());
					pedido.setDesconto(calculaDesconto());
					pedido.setTotal(totalPedido);
					pedido.setObservacao(edtComment.getText().toString());
					pedido.setRetirada(dataRetirada);
					pedido.setCliente(idCliente);

					System.out.println("TOSTRING JSON PEDIDO");
					System.out.println(pedido.toJsonString());

					RequestParams parametros = new RequestParams();
					parametros.put("pedido", pedido.toJsonString());

					validaPedidoAberto(parametros, idCliente);
					//finalzarPedidoWS(parametros);

				}





				//Toast.makeText(ActivityCheckout.this, "Forma Pagamento: " + formaPagamento.getSelectedItem().toString() +  " / Itens: " + edtOrderList.getText().toString() + " / Data: " + dataRetirada + " / OBS: " + edtComment.getText().toString(), Toast.LENGTH_SHORT).show();


				// Chamar método que manda pro webservice
			}
		});

		// busca itens do pedido no banco
		//buscaItensPedidoSQLite();
        buscaItensPedidoDecoratorSQLITE();
    }

    private Integer calculaDesconto(){
		if(sessao.getDetalhesUsuario().get(Sessao.KEY_EMAIL).contains("@senacrs.com.br")){
			return sessao.getDesconto();
		}

		return 0;
	}

	private void abreJanelaPedidoOK(int idIcone, String status, final Long idPedido){
		LayoutInflater factory = LayoutInflater.from(ActivityCheckout.this);
		final View deleteDialogView = factory.inflate(R.layout.alertapedido, null);
		final AlertDialog deleteDialog = new AlertDialog.Builder(ActivityCheckout.this).create();
		deleteDialog.setCanceledOnTouchOutside(false);
		deleteDialog.setView(deleteDialogView);
		ImageView icone = (ImageView) deleteDialogView.findViewById(R.id.icone);
		TextView txtStatus = (TextView) deleteDialogView.findViewById(R.id.status);
		txtStatus.setText(status);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			icone.setImageDrawable(getResources().getDrawable(idIcone, getApplicationContext().getTheme()));
		} else {
			icone.setImageDrawable(getResources().getDrawable(idIcone));
		}

		deleteDialogView.findViewById(R.id.ok).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deleteDialog.dismiss();
				adicionaNotificacaoInicial(idPedido);
				voltarInicio();
			}
		});

		deleteDialog.show();
	}

	private void adicionaNotificacaoInicial(Long idPedido) {
		dbhelper.limpaNotificacoes();
		dbhelper.limpaPedido();
		dbhelper.adicionaNotificacao(new Notificacao(1, idPedido, null, "Pedido enviado para análise.", 1));
		dbhelper.close();
	}

	public boolean validaPedido(){
		if(itensPedido.size() == 0){
			Toast.makeText(ActivityCheckout.this, "Você não tem nenhum item no seu pedido.", Toast.LENGTH_SHORT).show();
			return false;
		}

		if(formaPagamento.getSelectedItemPosition() == 0){
			Toast.makeText(ActivityCheckout.this, "Selecione a forma de pagamento.", Toast.LENGTH_SHORT).show();
			return false;
		}

		if(btnDate.getText().toString().equals("Data Retirada") || btnTime.getText().toString().equals("Hora Retirada")){
			Toast.makeText(ActivityCheckout.this, "Informe a data e a hora de retirada do pedido.", Toast.LENGTH_SHORT).show();
			return false;
		}

		String retirada = btnDate.getText() + " " + btnTime.getText() + ":00";

		Date dataRetirada = null;
		try {
			dataRetirada = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(retirada);

			Calendar ret = Calendar.getInstance(new Locale("pt", "BR"));
			ret.setTime(dataRetirada);

			Calendar hoje = Calendar.getInstance(new Locale("pt", "BR"));

			if(ret.get(Calendar.HOUR_OF_DAY) < 10 || (ret.get(Calendar.HOUR_OF_DAY) >= 12 && ret.get(Calendar.HOUR_OF_DAY) < 20) || ret.get(Calendar.HOUR_OF_DAY) >= 22
					|| ret.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || ret.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY){
				Toast.makeText(ActivityCheckout.this, "Fora do horário de atendimento.", Toast.LENGTH_SHORT).show();
				return false;
			}

			if(ret.before(hoje)){
				Toast.makeText(ActivityCheckout.this, "Data inválida. Não é permitido informar uma data passada.", Toast.LENGTH_LONG).show();
				return false;
			}

			hoje.add(Calendar.MINUTE, maiorTempoPreparo());

			if(ret.before(hoje)){
				Toast.makeText(ActivityCheckout.this, "Impossível produzir algum item do seu pedido no intervalo de tempo entre agora e a hora de retirada. Favor conferir e alterar a hora de retirada.", Toast.LENGTH_LONG).show();
				return false;
			}


		} catch (ParseException e) {
			e.printStackTrace();
		}

		return true;
	}

	private int maiorTempoPreparo(){
		int maior = 0;

		for(ItemPedidoDecorator i : itensPedido){
			if(i.getTempopreparo() > maior){
				maior = i.getTempopreparo();
			}
		}

		return maior;
	}

	public void voltarInicio(){
		//super.onBackPressed();
//        dbhelper.limpaPedido();
//		dbhelper.close();
		finish();

		Intent intent = new Intent(ActivityCheckout.this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
		startActivity(intent);
		overridePendingTransition(R.anim.open_main, R.anim.close_next);
	}


    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.icone_home, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				this.finish();
        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
				return true;


			case R.id.inicio:
//        	this.finish();
//        	overridePendingTransition(R.anim.open_main, R.anim.close_next);
				voltarInicio();
			return true;
			
		default:
			return super.onOptionsItemSelected(item);
		}
	}
    
    // Cria o Picker pra DATA
    public static class DatePickerFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			return new DatePickerDialog(getActivity(), this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
		}
		
		public void onDateSet(DatePicker view, int year, int month, int day) {
			mYear = year;
			mMonth = month;
			mDay = day;

			btnDate.setText(pad(mDay) + "/" + pad(mMonth + 1) + "/" + mYear);
		}
    }
    
    // Cria o Picker pra HORA
    public static class TimePickerFragment extends DialogFragment
    implements TimePickerDialog.OnTimeSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar c = Calendar.getInstance();
			return new TimePickerDialog(getActivity(), this, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), DateFormat.is24HourFormat(getActivity()));
		}
		
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			mHour = hourOfDay;
			mMinute = minute;

			btnTime.setText(pad(mHour) + ":" + pad(mMinute));
		}
    }



	

    public void buscaItensPedidoSQLite(){
    	
    	data = dbhelper.getAllData();

    	double Order_price = 0;
    	double Total_price = 0;
    	double tax = 0;
    	
    	// store all data to variables
    	for(int i=0;i<data.size();i++){
    		ArrayList<Object> row = data.get(i);
    		
    		String Menu_name = row.get(1).toString();
    		String Quantity = row.get(2).toString();
    		double Sub_total_price = Double.parseDouble(formatData.format(Double.parseDouble(row.get(3).toString())));
    		Order_price += Sub_total_price;
    		
    		// calculate order price
    		OrderList += (Quantity+"x "+Menu_name+" - "+Utilitarios.formatarValor(Sub_total_price, true)+"\n");
    	}
    	
    	if(OrderList.equalsIgnoreCase("")){
    		OrderList += getString(R.string.no_order_menu);
    	}
    	
    	tax = Double.parseDouble(formatData.format(Order_price *(Tax /100)));
    	Total_price = Double.parseDouble(formatData.format(Order_price - tax));
    	OrderList += "\nTotal: "+ Utilitarios.formatarValor(Order_price, true)+
    			"\nDesconto: R$ 0,00"+
    			"\nTotal a Pagar: "+ Utilitarios.formatarValor(Total_price, true);
    	edtOrderList.setText(OrderList);
    }

    private static String pad(int c) {
        if (c >= 10){
             return String.valueOf(c);
        }else{
        	return "0" + String.valueOf(c);
        }
    }

    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    	dbhelper.close();
    	finish();
    	overridePendingTransition(R.anim.open_main, R.anim.close_next);
    }
    
    @Override
	public void onConfigurationChanged(final Configuration newConfig){
		super.onConfigurationChanged(newConfig);
	}


	private void buscaItensPedidoDecoratorSQLITE(){
		this.itensPedido.clear();

		List<ItemPedidoDecorator> itens = dbhelper.buscaItensDecorator();
		this.itensPedido.addAll(itens);
		Double valorTotalPedido = 0d;
		Double subTotalItem = 0d;

		for(ItemPedidoDecorator item : itens){
			subTotalItem = item.getQuantidade() * item.getValorUnitario();
			valorTotalPedido += subTotalItem;

			OrderList += (item.getQuantidade() + "x " + item.getNome() + " - " + Utilitarios.formatarValor(subTotalItem, true)+"\n");
		}

		totalPedido = valorTotalPedido;


		if(OrderList.equalsIgnoreCase("")){
			OrderList += getString(R.string.no_order_menu);
		}

		Integer porcentagemDesconto = calculaDesconto();
		Double porcentagem = (0d + porcentagemDesconto) / 100;

		Double valorDescontado = 0d;
		if(porcentagemDesconto > 0) {
			System.out.println("PORCENTAGEM POSITIVA");
			valorDescontado = (valorTotalPedido * porcentagem);
			System.out.println("VALORDESOCNTADO: " + valorDescontado);
			System.out.println("TOTAL: " + valorTotalPedido);
			System.out.println("PORCENT: " + porcentagemDesconto);
			System.out.println("PR: " + (porcentagemDesconto / 100));
		}

		OrderList += "\nTotal: "+ Utilitarios.formatarValor(valorTotalPedido, true)+
				"\nDesconto: "+ Utilitarios.formatarValor(valorDescontado, true) + " (" + porcentagemDesconto + "%)" +
				"\nTotal a Pagar: "+ Utilitarios.formatarValor(valorTotalPedido - valorDescontado, true);
		edtOrderList.setText(OrderList);
	}


	private void validaPedidoAberto(final RequestParams params, Long idUsuario){
		progressDialog = new ProgressDialog(this);
		progressDialog.setMessage("Validando pedido..");
		progressDialog.setCanceledOnTouchOutside(false);
		progressDialog.show();
		AsyncHttpClient client = new AsyncHttpClient();

		RequestParams parametros = new RequestParams();
		parametros.put("usuarioid", idUsuario);
		parametros.put("tipoPagamento", formaPagamento.getSelectedItemPosition());
		parametros.put("total", totalPedido);

		client.post(Constant.VERIFICA_PEDIDOS_ABERTOS_USUARIO, parametros, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
                System.out.println("REPOSTA JSON: " + respostaJson.toString());

				Gson gson = new GsonBuilder().create();
				RetornoRequisicao retorno = gson.fromJson(respostaJson.toString(), RetornoRequisicao.class);

				if(retorno.getStatus().equalsIgnoreCase("ok")){
					finalzarPedidoWS(params);
				}else{
					progressDialog.dismiss();

					LayoutInflater factory = LayoutInflater.from(ActivityCheckout.this);
					final View viewJanelinha = factory.inflate(R.layout.alertapedido, null);
					final AlertDialog janela = new AlertDialog.Builder(ActivityCheckout.this).create();
					janela.setCanceledOnTouchOutside(false);
					janela.setView(viewJanelinha);
					ImageView icone = (ImageView) viewJanelinha.findViewById(R.id.icone);
					TextView txtStatus = (TextView) viewJanelinha.findViewById(R.id.status);
                    try {
                        txtStatus.setText(("\n" + URLDecoder.decode(retorno.getMensagem(), "UTF-8")).replaceAll("nl", "\n\n"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
						icone.setImageDrawable(getResources().getDrawable(R.drawable.button_delete, getApplicationContext().getTheme()));
					} else {
						icone.setImageDrawable(getResources().getDrawable(R.drawable.button_delete));
					}

					viewJanelinha.findViewById(R.id.ok).setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							janela.dismiss();
						}
					});

					janela.show();
				}
			}


		});

//		client.get(Constant.VERIFICA_PEDIDOS_ABERTOS_USUARIO +  "/" + idUsuario, new JsonHttpResponseHandler() {
//			@Override
//			public void onSuccess(int statusCode, Header[] headers, JSONObject resposta) {
//				Gson gson = new GsonBuilder().create();
//				RetornoCount total = gson.fromJson(resposta.toString(), RetornoCount.class);
//
//				if(total.getTotal() == 0){
//					finalzarPedidoWS(params);
//				}else{
//					progressDialog.dismiss();
//
//					LayoutInflater factory = LayoutInflater.from(ActivityCheckout.this);
//					final View viewJanelinha = factory.inflate(R.layout.alertapedido, null);
//					final AlertDialog janela = new AlertDialog.Builder(ActivityCheckout.this).create();
//					janela.setCanceledOnTouchOutside(false);
//					janela.setView(viewJanelinha);
//					ImageView icone = (ImageView) viewJanelinha.findViewById(R.id.icone);
//					TextView txtStatus = (TextView) viewJanelinha.findViewById(R.id.status);
//					txtStatus.setText("\nVocê possui um pedido em aberto e não poderá efetuar outro pedido até que este seja completado.");
//
//					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//						icone.setImageDrawable(getResources().getDrawable(R.drawable.button_delete, getApplicationContext().getTheme()));
//					} else {
//						icone.setImageDrawable(getResources().getDrawable(R.drawable.button_delete));
//					}
//
//					viewJanelinha.findViewById(R.id.ok).setOnClickListener(new OnClickListener() {
//						@Override
//						public void onClick(View v) {
//							janela.dismiss();
//						}
//					});
//
//					janela.show();
//				}
//			}
//		});
	}

	private void finalzarPedidoWS(RequestParams params){
        System.out.println("CHEGOU FINALIZAR");
        progressDialog.setMessage("Finalizando Pedido..");

		AsyncHttpClient client = new AsyncHttpClient();
		client.post(Constant.FINALIZA_PEDIDO, params, new JsonHttpResponseHandler(){
			@Override
			public void onSuccess(int statusCode, Header[] headers, JSONObject respostaJson) {
				Gson gson = new GsonBuilder().create();
				PedidoStatus pedidoStatus = gson.fromJson(respostaJson.toString(), PedidoStatus.class);
				progressDialog.dismiss();
				abreJanelaPedidoOK(R.drawable.iconeok, "Seu pedido foi enviado para análise. \n\nVocê receberá notificações toda vez que seu pedido sofrer mudanças de quantidade.", pedidoStatus.getId());
			}

			@Override
			public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
				super.onFailure(statusCode, headers, responseString, throwable);
				System.out.println("ERRO DE REQUISICAO");
			}

		});
	}
}
