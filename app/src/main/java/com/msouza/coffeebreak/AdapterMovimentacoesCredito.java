package com.msouza.coffeebreak;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bumptech.glide.util.Util;
import com.msouza.coffeebreak.entidades.MovimentacaoCredito;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Matheus on 27/06/2017.
 */

public class AdapterMovimentacoesCredito extends ArrayAdapter<MovimentacaoCredito> {

    public AdapterMovimentacoesCredito(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    public AdapterMovimentacoesCredito(Context context, int resource, List<MovimentacaoCredito> items) {
        super(context, resource, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.movimentacao_credito_item, null);
        }

        MovimentacaoCredito p = getItem(position);

        if (p != null) {
            TextView valor = (TextView) v.findViewById(R.id.valor);
           // TextView tipo = (TextView) v.findViewById(R.id.tipo);
            TextView data = (TextView) v.findViewById(R.id.data);

            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy, HH:mm", new Locale("pt", "BR"));

            valor.setText(Utilitarios.formatarValor(p.getValormovimentado(), true));
            valor.setTextColor(Color.parseColor(getCor(p.getTipomovimentacao())));
           // tipo.setText(getTipoMovimentacao(p.getTipomovimentacao()));
            data.setText(p.getDatamovimentacao() != null ? sdf.format(p.getDatamovimentacao()).toUpperCase() : "-");

          /*  if (tt1 != null) {
                tt1.setText(p.getId());
            }

            if (tt2 != null) {
                tt2.setText(p.getCategory().getId());
            }

            if (tt3 != null) {
                tt3.setText(p.getDescription());
            }*/
        }

        return v;
    }

    private String getTipoMovimentacao(int tipo){
        switch(tipo){
            case 1:
                return "Saque";
            case 2:
                return "Depósito";
            default:
                return "Abertura";
        }
    }

    private String getCor(int tipo){
        switch(tipo){
            case 1:
                return "#dd4b39";
            case 2:
                return "#00a65a";
            default:
                return "#424242";
        }
    }

    @Nullable
    @Override
    public MovimentacaoCredito getItem(int position) {
        return super.getItem(position);
    }

}