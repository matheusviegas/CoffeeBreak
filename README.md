## Coffee Break
#### Um sistema de gerenciamento de cantinas


Este é um sistema que permite aos alunos da faculdade efetuarem pedidos para a cantina, agilizando assim a produção 
de pedidos mais demorados e ajudando a evitar que os alunos passem o intervalo inteiro na fila para pedir.

<a href='https://play.google.com/store/apps/details?id=com.msouza.coffeebreak'><img alt='Disponível no Google Play' src='https://gitlab.com/matheusviegas/CoffeeBreak/uploads/2c4def67205f41dc7024f78208d5659b/google_play.png'/></a>


## Tecnologias Utilizadas
##### Sistema Web
* PHP
* MySQL (MariaDB)
* HTML5, CSS3 & Javascript
* Codeigniter
* Bootstrap
* Admin LTE (Identidade Visual Open Source)
* Pace JS
* FPDF

##### Webservice
* PHP
* Lumen Framework

 
##### Sistema Android
* Java
* OpenFacebook API
* SQLite


## Rodando o Sistema

Para rodar este sistema, você precisará dar suporte as tecnologias citadas acima. Para isto, você vai precisar de:
* Servidor HTTP (Apache) para rodar a parte Web e o WebService.
* Servidor de Banco de Dados (MySQL)
* IDE de Desenvolvimento AndroidStudio para compilar e testar a parte mobile.

1. Faça o download dos projetos cbgerencial (painel administrativo) e restful (Web Service) através [deste link](https://www.dropbox.com/s/ufvjhhq7ytm276q/sistemas.zip?dl=1 "Fazer download dos projetos") e baixe o SQL de criação do banco de dados [neste link](https://www.dropbox.com/s/pjlb9bldjwyvxqg/coffeebreak.sql?dl=1 "Fazer download do SQL"); 
2. Crie o banco de dados chamado **coffeebreak** e importe o arquivo coffeebreak.sql;
3. Coloque as pastas **cbgerencial** e **restful** dentro da raiz do seu servidor HTTP;
4. Neste ponto, você já tem acesso a parte Web e ao *WebService*. Acesse localhost/cbgerencial (troque pelo seu caminho se estiver rodando em um servidor na web) e use o email: **admin@cantina.com** senha: **admin** para acessar o painel gerencial;
5. Agora abra o arquivo **Constant.java** que é onde as URL's de acesso ao *WebService* ficam. Atualize a variável estática **BASE_URL** para a URL raiz do seu servidor HTTP;
6. Agora basta compilar e rodar a parte android.